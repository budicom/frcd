<?php

require_once('config.inc.php');
require_once(FR_LIB_PATH.'/presenter.php');
require_once(FR_BASE_PATH."/dao/dao.php");

//error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
error_reporting(E_ALL);
function getmicrotime()
{
   list($usec, $sec) = explode(" ",microtime());
   return ((float)$usec + (float)$sec);
}

//$start = getmicrotime();

  if (isset($_GET['module'])) {
      $module = $_GET['module'];}
  else $module = "home";
  
  if (isset($_GET['event'])) {
	  $event = $_GET['event'];
  } else {
	  $event = 'execute';
  }
  
  if (isset($_GET['action'])) {
	$action = $_GET['action'];
  } else {
	$action = $module;
  }

  $classFile = FR_BASE_PATH.'/modules/'.$module.'/'.$action.'.php';
      if (file_exists($classFile)) {
          require_once($classFile);
          if (class_exists($action)) {
                  $instance = new $action();
                  if (!FR_Module::isValid($instance)) {
                      die(MSG_INVALID_MODULE);
                  }

                  $instance->moduleName = $module;
                  if ($instance->authenticate()) {
                          $result = $instance->$event();
                          if (!$result) {
                              $presenter = FR_Presenter::factory($instance->presenter,$instance);
                              if ($presenter) {
                                  $presenter->display();
                              } else {
                                  die("Erreur presenter".$presenter->getMessage());
                              }
                          }
                  } else {
                      die(MSG_AUTH_ERROR);
                  }
          } else {
              die(MSG_MODULE_NOT_FOUND);
          }
      } else {
          die(MSG_ACTION_NOT_FOUND);
      }

//echo "Temps d'ex&eacute;cution : ".round((getmicrotime()-$start),2)."s";

?>
