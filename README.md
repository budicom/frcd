# Le jeu FRCD

## Avertissement

Ce jeu en ligne sur le cinéma est basé sur des versions obsolètes de PHP et MySQL.
- Version du mySQL: 5.1.35
- Version de PHP: 5.2.9


Je ne suis pas l'auteur de ce code. J'ai participé à ce jeu à une lointaine époque et été un co-organisateur d'une session, ce qui fait que je suis entré en possession du code source, que j'ai retrouvé dans mes archives.

Il est encore possible d'en trouver des traces sur Internet, notamment à cette adresse http://jeu.frcd.free.fr/
