<?php
// parametres du jeu 
define ("FRCD_SESSION","29");
define ("FRCD_TITRE","Jeu FRCD session ".FRCD_SESSION);
define ("FRCD_EMAIL","email@quelquechose.com");
define ("FRCD_NB_ELEMENTS",3);
define ("FRCD_BETISIER_NB_VOTES",5);
define ("FRCD_MINIATURES_LARGEUR",80);
define ("FRCD_HEURE_DEFAUT",22);
define ("FRCD_FICHIERS_CHEMIN","./fichiers/");
define ("FRCD_FICHIERS_CODAGE",32);
define ("FRCD_ARCHIVES","http://jeu.frcd.free.fr/");
define ("FRCD_TEMPS_CONNEXION",15); // delai en minutes pour compter le nombre de connectes en fonction de leur derniere consultation du site
define ("FRCD_TEMPS_CORRECTION",15); // delai en minutes du verrouillage d'une correction

// constantes de connexion a la base de donnees 
define ("DB_DRIVER","mysql");
define ("DB_SERVER","localhost");
//define ("DB_SERVER","sql.free.fr");
define ("DB_USER","frcd");
define ("DB_PASSWORD","frcd");
define ("DB_DATABASE","frcd");

// chemins d'acces
$full_path =".";//dirname(__FILE__);
define ("FR_INCLUDE_PATH",$full_path."/include");
define ("FR_BASE_PATH",FR_INCLUDE_PATH."/app");
define ("FR_LIB_PATH",FR_INCLUDE_PATH."/lib");
define ("FR_TEMPLATE_PATH",FR_INCLUDE_PATH."/templates");

// messages d'erreur generaux
define ("MSG_DB_CONNECTION_ERROR","La base de donn&eacute;es est actuellement indisponible. Revenez plus tard.");
define ("MSG_AUTH_ERROR","Vous n'avez pas le droit d'acc&eacute;der &agrave; cette page.");
define ("MSG_MODULE_NOT_FOUND","Le module sp&eacute;cifi&eacute; n'existe pas.");
define ("MSG_INVALID_MODULE","Le module sp&eacute;cifi&eacute; n'est pas valide.");
define ("MSG_ACTION_NOT_FOUND","Action introuvable.");
define ("MSG_PRESENTER_ERROR","Erreur du presenter.");

setlocale(LC_TIME, "fr", "french", "fra", "fr_FR", "fr_FR@euro");
