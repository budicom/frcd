<?php

require_once(FR_LIB_PATH."/session.php");
	require_once(FR_LIB_PATH."/object.php");

class FR_Object_Web extends FR_Object
{
	var $session;

	function __construct()
	{
		parent::__construct();
		static $session=null;
		if ($session === null)
		{
			$session=new FR_Session();
		}
	$this->session = $session;

	//$user=$this->session->get('user');
	//$this->user=$user;
	}

	function FR_Object_Web()
	{
		$this->__construct();
	}

	function redirect($url)
	{
		if (count($this->errors)===0) 
		{
			header("Location: $url");
			exit();
		}
	}

	function __destruct()
	{
		$this->session->__destruct();
		//$this->user->__destruct();
		parent::__destruct();
	}

}

?>
