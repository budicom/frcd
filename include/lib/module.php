<?php

require_once(FR_LIB_PATH."/object/web.php");
// require_once(FR_LIB_PATH."/object/db.php");

class FR_Module extends FR_Object_Web
{
	var $presenter = 'phptemplate';

	var $data = array();

	var $name;

	var $tplFile;

	var $moduleName = null;

	var $pageTemplateFile = null;
      
	var $dao = null;
      
	function __construct()
	{
		parent::__construct();
		$this->name = get_class($this);
		$this->tplFile = $this->name.'.php';
	}

	function FR_Module()
	{
		$this->__construct();
	}

	function noevent()
	{
	}

	function set($var,$val)
	{
		$this->data[$var] = $val; 
	}

	function get($var)
	{
		if (isset($this->data[$var]))
			return $this->data[$var]; 
		else return false;
	}

	function addFrom(&$data)
	{
		if (is_array($data) && count($data))
		{
			$this->data=$data;
		}
	}

	function getData()
	{
		return $this->data;
	}

	function isValid($module)
	{
		return ((is_object($module)) && ( is_a ($module,'FR_Module')) && (is_a($module, 'FR_Auth')));
	}

	/* function checkDaoResult($res)
	{
		if ($res===false)
		{
			$this->setErrorMsg($this->dao->errorMsg());
			//die($this->errorMsg());
		}
	}*/

	function __destruct()
	{
		parent::__destruct();
		if (isset($this->dao)) $this->dao->__destruct();
	}

}

?>
