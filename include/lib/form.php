<?php
require_once(FR_LIB_PATH."/validate.php");

class FR_Form
{
	var $data;
	function __construct()
	{
		$this->data=array();
	}

	function FR_Form()
	{
		$this->__construct();
	}

	function add($var,$type,$obligatoire=false)
	{
		$this->data[$var]=array('type'=>$type,'val'=>false,'obligatoire'=>$obligatoire);
	}

	function validate()
	{
		$erreurs=array();
		foreach ($this->data as $var=>$value)
		{
			$validate=Validate::$value['type']($value['val']);
			$this->data[$var]['val']=$validate;
			if ($validate===false&&$value['obligatoire']===true) $erreurs[$var]="Valeur invalide ou absente";
		}
		return $erreurs;
	}

	function get()
	{
		$res=array();
		foreach ($this->data as $var=>$value)
		{
			$res[$var]=$value['val'];
		}
		return $res;
	}

	function set($data)
	{
		foreach ($data as $key=>$value)
		{
			if (isset($this->data[$key])) $this->data[$key]['val']=$value;
		}
	}

	function __destruct()
	{
	}
}

?>
