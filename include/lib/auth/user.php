<?php

require_once(FR_LIB_PATH."/auth.php");

class FR_Auth_User extends FR_Auth
{
	function __construct()
	{
		parent::__construct();
	}

	function FR_Auth_User()
	{
		$this->__construct();
	}

	function authenticate()
	{
		$user=$this->session->get('user');
		if ($user)
		{
			if ($user['status'] == 'active')
			{
				return true;
			}
		}
		return false;
	}

	function __destruct()
	{
		parent::__destruct();
	}
}

?>
