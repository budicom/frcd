<?php

class FR_Presenter
{
	function factory($type, $module)
	{
		$file = FR_LIB_PATH.'/presenter/'.$type.'.php';
		if (require_once($file))
		{
			$class = 'FR_Presenter_'.$type;
			if (class_exists($class))
			{
				$presenter = new $class($module);
				if (is_a($presenter,'FR_Presenter_common'))
				{
					return $presenter;
				}
			}
		}

		return false;
	}
}

?>
