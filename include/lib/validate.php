<?php

class Validate
{
	//function email($email)
	function html($html)
	{
		$return=false;
		if (isset($html))
		{
			$return=$html;
			if (get_magic_quotes_gpc()) $return=stripslashes($return);
				$return=html_entity_decode($return);
				$return=trim($return);
				if ($return=="") return false;
		}
		return $return;
	}

	function text($text)
	{
		$return=false;
		if (isset($text))
		{
			$return=strip_tags($text);
			//$return=htmlentities($return,ENT_NOQUOTES);
			if (get_magic_quotes_gpc()) $return=stripslashes($return);
				$return=trim($return);
				if ($return=="") return false;
		}
		return $return;
	}

	function alphanum($text)
	{
		$return=false;
		if (isset($text))
		{
			if (ereg('[^A-Za-z0-9]', $text)) $return=false;
			else
			{
				$return=strip_tags($text);
				$return=htmlentities($return);
				$return=trim($return);
				if ($return=="") return false;
			}
		}
		return $return;
	}

	function alpha($text)
	{
		$return=false;
		if (isset($text))
		{
			if (ereg('[^A-Za-z]', $text)) $return=false;
			else
			{
				$return=strip_tags($text);
				$return=htmlentities($return);
				$return=trim($return);
				if ($return=="") return false;
			}
		}
		return $return;
	}

	function num($text)
	{
		$return=false;
		if (isset($text))
		{
			if (ereg('[^0-9]', $text)) $return=false;
			else
			{
				$return=strip_tags($text);
				$return=htmlentities($return);
				$return=trim($return);
				if ($return=="") return false;
			}
		}	      
		return $return;
	}

}

?>
