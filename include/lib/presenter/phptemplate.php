<?php

require_once(FR_LIB_PATH.'/thirdparty/template/template.php');
require_once(FR_LIB_PATH."/presenter/common.php");

class FR_Presenter_PHPtemplate extends FR_Presenter_common
{
	var $template = null;
	var $path = null;
	var $file = null;

	function FR_Presenter_PHPtemplate($module)
	{
		FR_Presenter_common::FR_Presenter_common($module);
		$this->path = FR_TEMPLATE_PATH.'/'.$this->module->moduleName;
		$this->file = $this->module->tplFile;
		$this->template = new Template();
	}

	function display()
	{
		$tplPath = FR_TEMPLATE_PATH;
		$this->template->set('modulePath',$this->path);
		$this->template->set('tplPath',$tplPath);
		$this->template->set('tplFile',$this->file);

		$this->template->set('user',$this->module->session->get('user'));
		//$this->template->assign('session',$this->module->session);
		$this->template->set('errors',$this->module->errors);

		foreach ($this->module->getData() as $var => $val)
		{
			if (!in_array($var,array('tplPath','tplFile')))
			{
				$this->template->set($var,$val);
			}
		}

		if ($this->module->pageTemplateFile != null)
		{
			$pageTemplateFile = $this->module->pageTemplateFile;
		} else {
			$pageTemplateFile = 'default.php';
		}

		//$this->module->session->__destruct();
		//$this->module->__destruct();
		//return $this->template->display($pageTemplateFile);
		echo $this->template->fetch($tplPath.'/'.$pageTemplateFile);
	}

	function __destruct()
	{
		parent::__destruct();
	}
}

?>

