<?php

class FR_Object
{
	var $errors=array();

	function setErrorMsg($msg)
	{
		if (is_array($msg)) $this->errors+=$msg;
		else $this->errors[]=$msg;
	}

	function errorMsg()
	{
		return $this->errors;
	}

	function __construct()
	{
		//$this->log = Log::factory('file',FR_LOG_FILE);
		//$this->me = new ReflectionClass($this);
		//$this->data=null;
	}

	function FR_Object()
	{
		$this->__construct();
	}

	function __destruct()
	{
	}
}

?>
