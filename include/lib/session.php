<?php

class FR_Session 
{
	var $sessionID;

	function FR_Session()
	{
		@session_cache_limiter('private_no_cache');
		@session_start();
		$this->sessionID = session_id();
	}

	function destroy()
	{
		foreach ($_SESSION as $var => $val)
		{
			$_SESSION[$var] = null;
		}
		@session_destroy();
	}

	function delete($var)
	{
		return @$_SESSION[$var] = null;
	}

	function __clone()
	{
		trigger_error('Clone is not allowed for '.__CLASS__,E_USER_ERROR);
	}

	function get($var)
	{
		return @$_SESSION[$var];
	}

	function set($var,$val)
	{
		return ($_SESSION[$var] = $val);
	}

	function __destruct()
	{
		@session_write_close();
	}

}

?>
