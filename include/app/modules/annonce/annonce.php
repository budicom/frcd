<?php
require_once(FR_BASE_PATH."/modules/frcd_auth_no.php");
require_once(FR_LIB_PATH."/validate.php");

class annonce extends FRCD_Auth_No
{
	function annonce()
	{
		parent::FRCD_Auth_No();
	}

	function execute()
	{
		$liste_des_annonces=$this->dao->get_annonces();
		//$derniere_annonce=$this->dao->get_derniere_annonce();

		if (!$liste_des_annonces) $this->setErrorMsg($this->dao->errorMsg());
		else
		{
			setcookie('derniere_annonce_lue', $liste_des_annonces[0]['id'], time()+ 60*60*24*365);

			foreach($liste_des_annonces as $annonce_id=>$annonce)
			{
				$liste_des_annonces [$annonce_id]['texte']=nl2br($annonce['texte']);
			}
			$this->set("liste_des_annonces",$liste_des_annonces);
		}
		$this->set('annonces_non_lues',0);

	}

	function destruct()
	{
		parent::destruct();
	}
}

?>
