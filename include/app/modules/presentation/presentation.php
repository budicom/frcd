<?php
require_once(FR_BASE_PATH."/modules/frcd_auth_no.php");

class presentation extends FRCD_Auth_No
{
	function presentation ()
	{
		parent::FRCD_Auth_No();
	}

	function execute()
	{
		$organisateurs=$this->dao->liste_des_organisateurs();
		$this->set('organisateurs',$organisateurs);
	}

	function destruct()
	{
		parent::destruct();
	}
}

?>
