<?php
require_once(FR_BASE_PATH."/modules/frcd_auth_no.php");

class equipes extends FRCD_Auth_No
{
	function equipes ()
	{
		parent::FRCD_Auth_No();
	}

	function execute()
	{
		$equipes=$this->dao->liste_des_equipes("active");
		$this->set('equipes',$equipes);
	}

	function destruct()
	{
		parent::destruct();
	}
}

?>
