<?php
require_once(FR_BASE_PATH."/modules/frcd_admin.php");
require_once(FR_LIB_PATH."/validate.php");

class ajouter_question extends FRCD_Admin
{
	function ajouter_question()
	{
		parent::FRCD_Admin();
		$this->tplFile="ajouter_question.php";
	}

	function execute()
	{
		$semaine_id=Validate::num($_GET['semaine_id']);
			if ($semaine_id)
			{
				$this->set("semaine_id",$semaine_id);
				$libelles=$this->dao->liste_des_libelles();
				$this->set('libelles',$libelles);
			}
			else $this->setErrorMsg("Identificateur de semaine invalide");
	}

	function destruct()
	{
		parent::destruct();
	}
}

?>
