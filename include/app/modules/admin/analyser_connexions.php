<?php
require_once(FR_BASE_PATH."/modules/frcd_admin.php");
require_once(FR_LIB_PATH."/validate.php");

class analyser_connexions extends FRCD_Admin
{

	function analyser_connexions()
	{
		parent::FRCD_Admin();
		$this->tplFile="analyser_connexions.php";
	}

	function execute()
	{
		$ip_communes=$this->dao->liste_des_connexions_problematiques();
		$nombre_ip_equipes=$this->dao->liste_nombre_ip_par_equipe();
		$nombre_conn_equipes=$this->dao->liste_nombre_connexions_par_equipe();

		$equipes_connectees=$this->dao->liste_equipes_connectees();

		if ($ip_communes === false || $ip_communes === false || $nombre_conn_equipes === false)
		{
			$this->setErrorMsg($this->dao->errorMsg());
			return false;
		}

		$this->set('ip_communes', $ip_communes);
		$this->set('nombre_ip_equipes', $nombre_ip_equipes);
		$this->set('nombre_conn_equipes', $nombre_conn_equipes);
		$this->set('equipes_connectees', $equipes_connectees);
	}

	function detail_ip()
	{

	}
}
?>
