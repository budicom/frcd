<?php
require_once(FR_BASE_PATH."/modules/frcd_admin.php");

class admin extends FRCD_Admin
{
	function admin()
	{
		parent::FRCD_Admin();
		$this->tplFile="admin.php";
	}

	function execute()
	{
		$semaines= $this->dao->liste_des_semaines();

		/*$n = 1;
		foreach($liste_semaines as $semaine)
		{
			$temp=$semaine;
			$temp['num']=$n;
			$semaines[]=$temp;
			$n=$n+1;
		}*/
		$this->set('semaines',$semaines);

		/*$semaines_en_cours= $this->dao->liste_des_semaines("en_cours");
		$this->set('semaines_ouvertes',$semaines_en_cours);*/
		$semaines_en_cours=array();
		foreach ($semaines as $semaine)
		{
			if ($semaine['etat']=="en_cours") $semaines_en_cours[]=$semaine;
		}
		$this->set('semaines_ouvertes',$semaines_en_cours);

		$annonces= $this->dao->liste_des_annonces();
		$this->set('annonces',$annonces);

		$equipes_actives= $this->dao->liste_des_equipes("active");
		$this->set('equipes_actives',$equipes_actives);
		$equipes_suspendues= $this->dao->liste_des_equipes("disabled");
		$this->set('equipes_suspendues',$equipes_suspendues);
	}

	function destruct()
	{
		parent::destruct();
	}
}
