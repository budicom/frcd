<?php
require_once(FR_BASE_PATH."/modules/frcd_admin.php");
require_once(FR_LIB_PATH."/validate.php");

class editer_bonus extends FRCD_Admin
{
	function editer_bonus()
	{
		parent::FRCD_Admin();
	}

	function execute()
	{
		$user_id=false;
		if (isset($_GET['equipe_id'])) $user_id=Validate::num($_GET['equipe_id']);

			if ($user_id)
			{
				$equipe=$this->dao->get_user_profile($user_id);
				if ($equipe)
				{
					$this->set('equipe',$equipe);
				}
				else $this->setErrorMsg("Compte invalide");
			}
		else $this->setErrorMsg("Identificateur invalide");
	}

	function destruct()
	{
		parent::destruct();
	}
}

?>
