<?php
require_once(FR_BASE_PATH."/modules/frcd_admin.php");
require_once(FR_LIB_PATH."/validate.php");

class creer_semaine extends FRCD_Admin
{
	function creer_semaine()
	{
		parent::FRCD_Admin();
		$this->tplFile="creer_semaine.php";
	}

	function execute()
	{
		$jours=array();
		$mois=array();
		$annees=array();

		for ($i=1;$i<=31;$i+=1) $jours[]=$i;
		for ($i=1;$i<=12;$i+=1) $mois[]=$i;

		for ($i=0;$i<=23;$i+=1) $heures[]=$i;

		$date=getdate();
		$cette_annee=$date['year'];
		for ($i=$cette_annee;$i<=$cette_annee+1;$i+=1) $annees[]=$i;

		$types = $this->dao->types_de_semaines();

		$this->set('jours',$jours);
		$this->set('mois',$mois);
		$this->set('annees',$annees);
		$this->set('heures',$heures);
		$this->set('types',$types);

	}

	function destruct()
	{
		parent::destruct();
	}
}

?>
