<?php
require_once(FR_BASE_PATH."/modules/frcd_admin.php");
require_once(FR_LIB_PATH."/validate.php");

class editer_annonce extends FRCD_Admin
{
	function editer_annonce()
	{
		parent::FRCD_Admin();
		$this->tplFile="editer_annonce.php";
	}

	function execute()
	{
		if (isset($_POST['annonce_id']))
		{
			$annonce_id=Validate::num($_POST['annonce_id']);
			if (($annonce_id)&&($annonce_id!=""))
			{
				$this->set('annonce_id',$annonce_id);
				$annonce=$this->dao->get_annonce($annonce_id);
				//if (!$annonce) $this->setErrorMsg($this->dao->errorMsg());
				$this->set('annonce',$annonce);              
			}
			else $this->settErrorMsg("Identificateur d'annonce invalide");
		} else {
			$this->set('annonce_id',"");
			$annonce=array();
			$this->set('annonce',$annonce);              
		}
	}

	function destruct()
	{
		parent::destruct();
	}
}

?>
