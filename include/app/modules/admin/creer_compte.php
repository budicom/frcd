<?php
require_once(FR_BASE_PATH."/modules/frcd_admin.php");
require_once(FR_LIB_PATH."/validate.php");

class creer_compte extends FRCD_Admin
{
	function creer_compte()
	{
		parent::FRCD_Admin();
		$this->tplFile="creer_compte.php";
	}

	function execute()
	{
	}

	function creer_equipe()
	{
		$this->set("admin",0);
	}

	function creer_admin()
	{
		$this->set("admin",1);
	}

	function destruct()
	{
		parent::destruct();
	}
}

?>
