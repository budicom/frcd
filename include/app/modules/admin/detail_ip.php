<?php
require_once(FR_BASE_PATH."/modules/frcd_admin.php");
require_once(FR_LIB_PATH."/validate.php");

class detail_ip extends FRCD_Admin
{

	function detail_ip()
	{
		parent::FRCD_Admin();
		$this->tplFile="detail_ip.php";
	}
    
	function execute()
	{
		$adresse_ip=Validate::text($_POST['adresse_ip']);

		if (!$adresse_ip)
		{
			$this->setErrorMsg("Pas d'adresse IP ou adresse IP invalide.");
			return false;
		}

		$connexions = $this->dao->liste_des_connexions_par_ip($adresse_ip);

		if ($connexions === false)
		{
			$this->setErrorMsg($this->dao->errorMsg());
			return false;
		}
		$this->set('connexions', $connexions);
	}
}
?>
