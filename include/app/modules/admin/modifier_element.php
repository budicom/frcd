<?php
require_once(FR_BASE_PATH."/modules/frcd_admin.php");
require_once(FR_LIB_PATH."/validate.php");

class modifier_element extends FRCD_Admin
{
	function modifier_element()
	{
		parent::FRCD_Admin();
		$this->tplFile="modifier_element.php";
	}

	function execute()
	{
		$question_id=Validate::num($_GET['question_id']);
		if (($question_id)&&($question_id!=""))
		{
			$this->set('question_id',$question_id);
			$ordre=Validate::num($_GET['ordre']);
			if (($ordre)&&($ordre!=""))
			{
				$this->set('ordre',$ordre);

				$element=$this->dao->get_element($question_id,$ordre);

				if (isset($element['points'])) $points=$element['points'];
				else 
				{
					$bareme=$this->dao->bareme();
					/*if ($ordre==1) $points=3;
					elseif ($ordre==2) $points=2;
					else $points=1;*/
					if (isset($bareme[$ordre-1]['points']))
						$points=$bareme[$ordre-1]['points'];
					else $points=1;
				}

				$this->set('points',$points);

				/*if ($element)*/ $this->set('element',$element);
				//else $this->setErrorMsg("Erreur lors du chargement des �l�ments");

				/*$types=$this->dao->liste_types_elements();
				if ($types) $this->set('types',$types);
				else $this->setErrorMsg("Erreur lors du chargement des types d'�l�ments");*/
			}
			else $this->setErrorMsg("Num�ro d'indice invalide.");
		}
		else $this->setErrorMsg("Identificateur de question invalide.");
	}

	function destruct()
	{
		parent::destruct();
	}
}

?>
