<?php
require_once(FR_BASE_PATH."/modules/frcd_admin.php");
require_once(FR_LIB_PATH."/validate.php");

class classement_provisoire extends FRCD_Admin
{

	function classement_provisoire()
	{
		parent::FRCD_Admin();
		$this->tplFile="classement_provisoire.php";
	}

	function execute()
	{
		$classement_provisoire=$this->dao->classement_provisoire();
		if ($classement_provisoire===false)
		{
			$this->setErrorMsg($this->dao->errorMsg());
			return false;
		}

		$this->set('classement_provisoire', $classement_provisoire);
	}

	function detail_ip()
	{

	}
}
?>
