<?php
require_once(FR_BASE_PATH."/modules/frcd_admin.php");
require_once(FR_LIB_PATH."/validate.php");

class modifier_element_points extends FRCD_Admin
{
	function modifier_element_points()
	{
		parent::FRCD_Admin();
		$this->tplFile="modifier_element_points.php";
	}

	function execute()
	{
		$question_id=Validate::num($_GET['question_id']);
		if (($question_id)&&($question_id!=""))
		{
			$this->set('question_id',$question_id);
			$ordre=Validate::num($_GET['ordre']);
			if (($ordre)&&($ordre!=""))
			{
				$this->set('ordre',$ordre);

				$element=$this->dao->get_element($question_id,$ordre);
				$this->set('element',$element);
				//else $this->setErrorMsg("Erreur lors du chargement des �l�ments");

			}
			else $this->setErrorMsg("Num�ro d'indice invalide.");
		}
		else $this->setErrorMsg("Identificateur de question invalide.");
	}

	function destruct()
	{
		parent::destruct();
	}
}

?>
