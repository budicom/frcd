<?php
require_once(FR_BASE_PATH."/modules/frcd_admin.php");
require_once(FR_LIB_PATH."/validate.php");

class validation extends FRCD_Admin
{
	function validation()
	{
		parent::FRCD_Admin();
		$this->tplFile="validation.php";
	}

	function execute()
	{
	}

	function nouvelle_question()
	{
		$this->set('titre',"Enregistrement d'une question");

		$reponse=Validate::text($_POST['reponse']);
		if ($reponse)
		{
			$semaine_id=$_GET['semaine_id'];
			$libelle_id=$_POST['libelle_id'];
			$user=$this->session->get('user');

			$organisateur_id=$user['id'];
			$reponse=addslashes($reponse);

			$res=$this->dao->nouvelle_question($semaine_id,$libelle_id,$organisateur_id,$reponse);
			$this->set('message',"La question a bien &eacute;t&eacute; enregistr&eacute;e.");
		}
		else $this->setErrorMsg("La r&eacute;ponse est invalide");

		$this->set('retour',"index.php?module=admin&action=editer_semaine&semaine_id=".$semaine_id);
	}

	function suspendre_compte()
	{
		$this->set('titre',"Suspension d'un compte");

		$equipe_id=Validate::num($_POST['equipe_id']);
		if ($equipe_id)
		{
			$res=$this->dao->update_compte($equipe_id,"disabled");
			$this->set('message',"Le compte a &eacute;t&eacute; suspendu.");
		}
		else $this->setErrorMsg("Le compte est invalide");
	}

	function reactiver_compte()
	{
		$this->set('titre',"R&eacute;activation d'un compte");

		$equipe_id=Validate::num($_POST['equipe_id']);
		if ($equipe_id)
		{
			$res=$this->dao->update_compte($equipe_id,"active");
			$this->set('message',"Le compte a &eacute;t&eacute; r&eacute;activ&eacute;.");
		}
		else $this->setErrorMsg("Le compte est invalide");
	}

	function nouvelle_equipe()
	{
		$this->set('titre',"Enregistrement d'une &eacute;quipe");

		$nom=Validate::text($_POST['nom']);

		if (($nom)&&($nom!=""))
		{
			$code = $this->dao->generate_password(8);
			if (isset($_POST['admin'])) $admin = $_POST['admin'];
				$res=$this->dao->nouvelle_equipe(addslashes($nom),$code,$admin);
				$this->set('message',"Le compte $nom a &eacute;t&eacute; cr&eacute;&eacute;e.
							<br>Le code d'identification de ce compte est le <strong>{$code}</strong>.");
		}
		else $this->setErrorMsg("Le nom d'&eacute;quipe est invalide");
	}

	function sauver_profil()
	{
		$this->set('titre',"Sauvegarde du profil utilisateur");

		if (isset($_POST['equipe_id'])) $equipe_id=Validate::num($_POST['equipe_id']);
		else $equipe_id=false;

		if (isset($_POST['description'])) $description=Validate::html($_POST['description']);
		else $description=false;

		if ($equipe_id)
		{
			$res=$this->dao->update_description($equipe_id,$description);
			$this->set('message',"Le profil a &eacute;t&eacute; mis &agrave; jour.");
		}
		else $this->setErrorMsg("Le compte est invalide");
	}

	function sauver_bonus()
	{
		$this->set('titre',"Sauvegarde du compte utilisateur");

		if (isset($_POST['equipe_id'])) $equipe_id=Validate::num($_POST['equipe_id']);
		else $equipe_id=false;

		if (isset($_POST['bonus'])) $bonus=$_POST['bonus'];
		else $bonus=false;

		if ($equipe_id)
		{
			$res=$this->dao->update_bonus($equipe_id,$bonus);
			$this->set('message',"Le profil a &eacute;t&eacute; mis &agrave; jour.");
		}
		else $this->setErrorMsg("Le compte est invalide");
	}

	function nouvelle_semaine()
	{
		/*$this->set('titre',"Cr&eacute;ation d'une semaine");
		$titre_semaine=Validate::text($_POST['titre_semaine']);
		if (($titre_semaine)&&($titre_semaine!=""))
		{*/
			$titre_semaine = NULL;
			// TODO : v�rifier les POST
			$annee_debut=$_POST['annee_debut'];
			$annee_fin=$_POST['annee_fin'];
			$mois_debut=$_POST['mois_debut'];
			$mois_fin=$_POST['mois_fin'];
			$jour_debut=$_POST['jour_debut'];
			$jour_fin=$_POST['jour_fin'];
			$heure_debut=$_POST['heure_debut'];
			$heure_fin=$_POST['heure_fin'];

			$type=$_POST['deroulement'];

			$theme_semaine=Validate::text($_POST['theme_semaine']);
			$description_semaine=Validate::html($_POST['description_semaine']);

			$this->set('titre',"Enregistrement d'un &eacute;l&eacute;ment.");

			$fichier_nom=$_FILES['image']['name'];
			$fichier_temporaire = $_FILES['image']['tmp_name'];
			$image=$this->dao->upload($fichier_nom,$fichier_temporaire);

			$date_debut=date("Y-m-d H:i:s", mktime($heure_debut, 0, 0, $mois_debut, $jour_debut, $annee_debut));
			$date_fin=date("Y-m-d H:i:s", mktime($heure_fin, 0, 0, $mois_fin, $jour_fin, $annee_fin));

			$res=$this->dao->nouvelle_semaine($titre_semaine,$theme_semaine,$description_semaine,$image,$date_debut,$date_fin,$type);
			$this->set('message',"La semaine a bien &eacute;t&eacute; enregistr&eacute;e.");
			/*}
		else $this->setErrorMsg("Le titre de la semaine est invalide");*/
	}

	function update_semaine()
	{
		$this->set('titre',"Mise &agrave; jour de la semaine");
		$semaine_id=Validate::num($_POST['semaine_id']);
		if ($semaine_id!==false)
		{
			/*$titre_semaine=Validate::text($_POST['titre_semaine']);
			if (($titre_semaine)&&($titre_semaine!=""))
			{*/
				// TODO : v�rifier les POST
				$titre_semaine = NULL;
				$annee_debut=$_POST['annee_debut'];
				$annee_fin=$_POST['annee_fin'];
				$mois_debut=$_POST['mois_debut'];
				$mois_fin=$_POST['mois_fin'];
				$jour_debut=$_POST['jour_debut'];
				$jour_fin=$_POST['jour_fin'];
				$heure_debut=$_POST['heure_debut'];
				$heure_fin=$_POST['heure_fin'];

				$image=$_POST['semaine_image'];
				$type=$_POST['deroulement'];

				if (isset($_POST['modifier_image']))
				{
					if ($_POST['modifier_image']==0) 
						$image="";
					elseif ($_POST['modifier_image']==1)
					{
						$fichier_nom=$_FILES['nouvelle_image']['name'];
						$fichier_temporaire = $_FILES['nouvelle_image']['tmp_name'];
						if ($fichier_nom)
							$image=$this->dao->upload($fichier_nom,$fichier_temporaire);
					}
				}

				$theme_semaine=Validate::text($_POST['theme_semaine']);
				$description_semaine=Validate::html($_POST['description_semaine']);

				$date_debut=date("Y-m-d H:i:s", mktime($heure_debut, 0, 0, $mois_debut, $jour_debut, $annee_debut));
				$date_fin=date("Y-m-d H:i:s", mktime($heure_fin, 0, 0, $mois_fin, $jour_fin, $annee_fin));

				$res=$this->dao->update_semaine($semaine_id,$titre_semaine,$theme_semaine,$description_semaine,$image,$date_debut,$date_fin,$type);           
				$this->set('message',"La semaine a bien &eacute;t&eacute; enregistr&eacute;e.");
				$this->set('retour',"index.php?module=admin&action=editer_semaine&semaine_id=".$semaine_id);
			/*}
			else $this->setErrorMsg("Le titre de la semaine est invalide");*/
		}
		else $this->setErrorMsg("Id de semaine invalide");
	}

	function clore_semaine()
	{
		$this->set('titre',"Cl�ture d'une semaine");
		$semaine_id=Validate::text($_GET['semaine_id']);
		if (($semaine_id)&&($semaine_id!=""))
		{
			$res=$this->dao->set_etat_semaine($semaine_id,'finie');
			$this->set('message',"La semaine a bien &eacute;t&eacute; cl�tur&eacute;e.");
		}
		else $this->setErrorMsg("Identificateur de semaine invalide.");
	}

	function melanger_semaine()
	{
		$this->set('titre',"M�lange d'une semaine");
		$semaine_id=Validate::num($_GET['semaine_id']);
		if (($semaine_id)&&($semaine_id!=""))
		{
			$res=$this->dao->melanger_les_questions($semaine_id);
			$this->set('message',"Les questions de la semaine ont bien &eacute;t&eacute; m&eacute;lang&eacute;es (bien v�rifier que le r&eacute;sultat n'est pas incoh&eacute;rent !).");
		}
		else $this->setErrorMsg("Identificateur de semaine invalide.");
			$this->set('retour',"index.php?module=admin&action=editer_semaine&semaine_id=".$semaine_id);
	}

	function rouvrir_semaine()
	{
		$this->set('titre',"R&eacute;ouverture d'une semaine");
		$semaine_id=Validate::text($_GET['semaine_id']);
		if (($semaine_id)&&($semaine_id!=""))
		{
			$res=$this->dao->set_etat_semaine($semaine_id,'en_cours');
			$this->set('message',"La semaine a bien &eacute;t&eacute; r&eacute;ouverte.");
		}
		else $this->setErrorMsg("Identificateur de semaine invalide.");
	}

	function ouvrir_semaine()
	{
		$this->set('titre',"Ouverture d'une semaine");
		$semaine_id=Validate::text($_GET['semaine_id']);
		if (($semaine_id)&&($semaine_id!=""))
		{
			$res=$this->dao->set_etat_semaine($semaine_id,'en_cours');
			$this->set('message',"La semaine a bien &eacute;t&eacute; ouverte.");
		}
		else $this->setErrorMsg("Identificateur de semaine invalide.");
	}

	function ordre_semaine_synchronisee()
	{
		$this->set('titre',"Ordre");
		$semaine_id=Validate::text($_GET['semaine_id']);
		$ordre=Validate::num($_GET['ordre']);
		if (($semaine_id)&&($semaine_id!=""))
		{
			if ($ordre!="")
			{
				$res=$this->dao->set_ordre_semaine_synchrone($semaine_id,$ordre);
				$this->set('message',"La semaine a bien &eacute;t&eacute; actualis&eacute;e.");
			}
			else $this->setErrorMsg("Ordre invalide.");
		}
		else $this->setErrorMsg("Identificateur de semaine invalide.");
		$this->set('retour',"index.php?module=admin&action=editer_semaine&semaine_id=".$semaine_id);
	}

	function questions_semaine_progressive()
	{
		$this->set('titre',"Questions");
		$semaine_id=Validate::text($_GET['semaine_id']);
		$questions_a_afficher=Validate::num($_GET['questions_a_afficher']);
		if (($semaine_id)&&($semaine_id!=""))
		{
			if ($questions_a_afficher!="")
			{
				$res=$this->dao->set_questions_semaine_progressive($semaine_id,$questions_a_afficher);
				$this->set('message',"La semaine a bien &eacute;t&eacute; actualis&eacute;e.");
			}
			else $this->setErrorMsg("Nombre invalide.");
		}
		else $this->setErrorMsg("Identificateur de semaine invalide.");
		$this->set('retour',"index.php?module=admin&action=editer_semaine&semaine_id=".$semaine_id);
	}

	function nouvel_element()
	{
		$this->set('titre',"Enregistrement d'un &eacute;l&eacute;ment.");

		$question_id=Validate::num($_POST['question_id']);
		$ordre=Validate::num($_POST['ordre']);
		$points=Validate::num($_POST['points']);

		if ($question_id && $ordre && $points)
		{
			//TODO verifier les donnees entrees
			$nouveau_fichier=false;

			$fichier_uploade=Validate::text($_POST['fichier_uploade']);
			if ($fichier_uploade)
			{
				if (file_exists(FRCD_FICHIERS_CHEMIN.$fichier_uploade))
					$nouveau_fichier=$fichier_uploade;
				else $this->setErrorMsg("Le fichier n'a pas ete trouve sur le serveur. Verifiez le nom.");
			} else {
				$fichier_nom=$_FILES['fichier']['name'];
				$fichier_temporaire = $_FILES['fichier']['tmp_name'];
				$fichier_existant=$_POST['fichier_existant'];
				$miniature_existante=$_POST['miniature_existante'];
				switch ($_FILES['fichier'] ['error'])
				{
					case 1:
						$this->setErrorMsg("Le fichier est trop volumineux (maximum : ".ini_get('upload_max_filesize').")");
						return false;
						break;
					case 2:
						$this->setErrorMsg("Le fichier est trop volumineux.");
						return false;
						break;
					case 3:
						$this->setErrorMsg("Une partie seulement du fichier a ete transferee.");
						return false;
						break;
					case 4:
						$this->setErrorMsg("Aucun fichier n'a ete transfere.");
						return false;
						break;
				}
				if ($fichier_nom)
				{
					$nouveau_fichier=$this->dao->upload($fichier_nom,$fichier_temporaire);
				} else $this->setErrorMsg("Fichier introuvable.");
			}

			if ($nouveau_fichier)
			{
				$icone=$this->dao->generer_miniature($nouveau_fichier);
				if ($icone)
				{
					// enregistrement dans la bdd
					$res=$this->dao->enregistrer_element($question_id,$ordre,$points,$nouveau_fichier,$icone,$fichier_existant);
					if ($res)
						$this->set('message',"L'&eacute;l&eacute;ment a bien &eacute;t&eacute; enregistr&eacute;.");
					else 
					{
						$this->setErrorMsg("Il y a eu un probl&egrave;me lors de l'enregistrement du fichier.");
						print_r($this->dao->errorMsg());
					}
				}
				else $this->setErrorMsg("Il y a eu un probl&egrave;me lors de la creation de la miniature.");
			}

			$semaine=$this->dao->question_quelle_semaine($question_id);
			$semaine_id=$semaine['semaine_id'];    

			$this->set('retour',"index.php?module=admin&action=editer_semaine&semaine_id=".$semaine_id."#".$question_id);
		} else {
			$this->setErrorMsg("Une erreur s'est produite. Veuillez recommencer.");
			$this->set('retour',"index.php?module=admin");
		}
	}

	function enregistrer_indication()
	{
		$this->set('titre',"Enregistrement d'une indication.");

		$question_id=Validate::num($_POST['question_id']);
		if (($question_id)&&($question_id!=""))
		{
			$ordre=Validate::num($_POST['ordre']);
			if (($ordre)&&($ordre!=""))
			{
				$indication=Validate::html($_POST['indication']);
				if (!$indication) $indication="";
					$res=$this->dao->enregistrer_indication($question_id,$ordre,addslashes($indication));
					$this->set('message',"L'indication a bien &eacute;t&eacute; enregistr&eacute;e.");
			} else $this->setErrorMsg("L'ordre est invalide");
		}
		else $this->setErrorMsg("Identificateur de question invalide.");

		$semaine=$this->dao->question_quelle_semaine($question_id);
		$semaine_id=$semaine['semaine_id'];
		$this->set('retour',"index.php?module=admin&action=editer_semaine&semaine_id=".$semaine_id."#".$question_id);
	}

	function enregistrer_points()
	{
		$this->set('titre',"Enregistrement d'une indication.");

		$question_id=Validate::num($_POST['question_id']);
		if (($question_id)&&($question_id!=""))
		{
			$ordre=Validate::num($_POST['ordre']);
			if (($ordre)&&($ordre!=""))
			{
				$points=Validate::num($_POST['points']);
				if (!$points) $points="0";
					$res=$this->dao->enregistrer_points($question_id,$ordre,$points);
					$this->set('message',"Les points ont bien &eacute;t&eacute; enregistr&eacute;s.");
			}
			else $this->setErrorMsg("L'ordre est invalide");
		}
		else $this->setErrorMsg("Identificateur de question invalide.");
		$semaine=$this->dao->question_quelle_semaine($question_id);
		$semaine_id=$semaine['semaine_id'];                    
		$this->set('retour',"index.php?module=admin&action=editer_semaine&semaine_id=".$semaine_id."#".$question_id);
	}

	function update_question()
	{
		$this->set('titre',"Enregistrement d'un commentaire");
		$question_id=Validate::num($_POST['question_id']);
		if (($question_id)&&($question_id!=""))
		{
			$semaine=$this->dao->question_quelle_semaine($question_id);
			$semaine_id=$semaine['semaine_id'];      
			$this->set('retour',"index.php?module=admin&action=editer_semaine&semaine_id=".$semaine_id."#".$question_id);

			$reponse=Validate::text($_POST['reponse']);
			if ($reponse)
			{
				$commentaire=Validate::html($_POST['commentaire']);
				if (!$commentaire) $commentaire="";
					$titre_original=Validate::text($_POST['titre_original']);
					if (!$titre_original) $titre_original="";
						$realisateur=Validate::text($_POST['realisateur']);
						if (!$realisateur) $realisateur="";
							$annee=Validate::text($_POST['annee']);
							$lien_imdb=Validate::text($_POST['lien_imdb']);
							if (!$lien_imdb) $lien_imdb="";
								if (isset($_POST['cacher_les_points']))
									$cacher_les_points=1;
								else
								$cacher_les_points=0;
							$libelle_id=$_POST['libelle_id'];
						$res=$this->dao->update_question(
						$question_id,
						$libelle_id,
						addslashes($reponse),
						addslashes($commentaire),
						addslashes($titre_original),
						addslashes($realisateur),
						addslashes($annee),
						addslashes($lien_imdb),
							$cacher_les_points
						);
						$this->set('message',"La question a bien &eacute;t&eacute; enregistr&eacute;e.");
			}
			else $this->setErrorMsg("La r&eacute;ponse est invalide");
		}
		else $this->setErrorMsg("Identificateur de question invalide.");
	}

	function annuler_question()
	{
		$this->set('titre',"Annulation d'une question");
		$question_id=Validate::num($_POST['question_id']);

		if (($question_id)&&($question_id!=""))
		{
			$semaine=$this->dao->question_quelle_semaine($question_id);
			$semaine_id=$semaine['semaine_id'];      
			$this->set('retour',"index.php?module=admin&action=editer_semaine&semaine_id=".$semaine_id."#".$question_id);

			$res=$this->dao->annuler_question($question_id);
			$this->set('message',"La question a bien &eacute;t&eacute; annul&eacute;e.");
		}
		else $this->setErrorMsg("Identificateur de question invalide.");
		$this->set('retour',"index.php?module=admin&action=editer_semaine&semaine_id=".$semaine_id."#".$question_id);
	}        

	function retablir_question()
	{
		$this->set('titre',"R&eacute;tablissement d'une question");
		$question_id=Validate::num($_POST['question_id']);

		if (($question_id)&&($question_id!=""))
		{
			$semaine=$this->dao->question_quelle_semaine($question_id);
			$semaine_id=$semaine['semaine_id'];
			$this->set('retour',"index.php?module=admin&action=editer_semaine&semaine_id=".$semaine_id."#".$question_id);

			$res=$this->dao->retablir_question($question_id);
			$this->set('message',"La question a bien &eacute;t&eacute; r&eacute;tabliee.");
		}
		else $this->setErrorMsg("Identificateur de question invalide.");
	}

	function nouvelle_proposition()
	{
		$this->set('titre',"Enregistrement des propositions");

		$user=$this->session->get('user');
		$correcteur_id = $user['id'];
		$equipe_id=Validate::num($_POST['equipe_id']);
		if ($equipe_id)
		{
			$semaine_id=Validate::num($_POST['semaine_id']);
			if ($semaine_id)
			{
				// v�rifie le verrou
				$res=$this->dao->nettoyer_verrous();

				$infos_verrou=$this->dao->infos_verrou($semaine_id,$equipe_id);
				if ($infos_verrou)
				{
					if ($correcteur_id!=$infos_verrou['id'])
					{
						$this->setErrorMsg("L'organisateur '".$infos_verrou["nom"]."' est en train de corriger cette �quipe.");
						return false;
					}
				} else {
					$res=$this->dao->nouvelle_edition($correcteur_id,$semaine_id,$equipe_id);
				}

				//enregistre les propositions

				if (isset($_POST['juste'])) $propositions=$_POST['juste'];
				else  $propositions=array();
				if (isset($_POST['betise'])) $betises=$_POST['betise'];
				else  $betises=array();

				foreach ($propositions as $id=>$juste)
				{
					foreach($juste as $ordre=>$valeur)
					{
						if ($valeur!="") $this->dao->ajouter_proposition($id,$ordre,$equipe_id,$valeur,$correcteur_id);
					}
				}

				foreach ($betises as $id=>$betise)
				{
					if (Validate::text($betise)!="") $this->dao->ajouter_betise($id,$equipe_id,addslashes($betise));
				}

				// d�v�rouille l'�dition
				$user=$this->session->get('user');

				$res=$this->dao->deverrouille_edition($user['id'],$semaine_id,$equipe_id);

				$this->set('message',"Les propositions ont bien �t� enregistr�es");
			}
			else $this->setErrorMsg("Identificateur de semaine invalide.");
		}
		else $this->setErrorMsg("Identificateur d'�quipe invalide.");
	}

	function annuler_proposition()
	{
		$user=$this->session->get('user');
		$correcteur_id = $user['id'];
		$equipe_id=Validate::num($_GET['eq_id']);
		$semaine_id=Validate::num($_GET['s_id']);

		$this->set('titre',"Annulation d'une proposition");
		// v�rifie le verrou
		$res=$this->dao->nettoyer_verrous();

		$infos_verrou=$this->dao->infos_verrou($semaine_id,$equipe_id);
		if ($infos_verrou)
		{
			if ($correcteur_id!=$infos_verrou['id'])
			{
				$this->setErrorMsg("L'organisateur '".$infos_verrou["nom"]."' est en train de corriger cette �quipe.");
				return false;
			}
		} else {
			$res=$this->dao->nouvelle_edition($correcteur_id,$semaine_id,$equipe_id);
		}
		//
		$question_id=Validate::num($_GET['q_id']);
		$ordre=Validate::num($_GET['o']);
		$correct=Validate::num($_GET['c']);
		$res=$this->dao->annuler_proposition($question_id,$ordre,$equipe_id,$correct,$correcteur_id);

		$user=$this->session->get('user');

	$res=$this->dao->deverrouille_edition($user['id'],$semaine_id,$equipe_id);

		$this->set('message',"La proposition a bien �t� annul�e.");
	}

	function supprimer_semaine()
	{
		$this->set('titre',"Suppression d'une semaine");

		$semaine_id=Validate::num($_GET['semaine_id']);
		if ($semaine_id)
		{
			$res=$this->dao->supprimer_semaine($semaine_id);
			$this->set('message',"La semaine a bien �t� supprim�e.");
		}
		else $this->setErrorMsg("Identificateur de semaine invalide.");
	}

	function supprimer_question()
	{
		$this->set('titre',"Suppression d'une question");

		$question_id=Validate::num($_POST['question_id']);
		if ($question_id)
		{
			$res=$this->dao->supprimer_question($question_id);
			$this->set('message',"La question a bien �t� supprim�e.");
		}
		else $this->setErrorMsg("Identificateur de question invalide.");
	}

	function supprimer_annonce()
	{
		$this->set('titre',"Suppression d'une annonce");

		$annonce_id=Validate::num($_POST['annonce_id']);
		if ($annonce_id)
		{
			$res=$this->dao->supprimer_annonce($annonce_id);
			$this->set('message',"L'annonce a bien �t� supprim�e.");
		}
		else $this->setErrorMsg("Identificateur d'annonce invalide.");
	}

	function sauver_annonce()
	{
		$this->set('titre',"Enregistrement d'une annonce");

		$id=$_POST['annonce_id'];
		$texte=Validate::html($_POST['texte']);
		$titre=Validate::text($_POST['titre']);
		$user=$this->session->get('user');
		$user_id=$user['id'];

		if ($id!="")
		{
			$res=$this->dao->update_annonce($id,addslashes($titre),addslashes($texte));
			$this->set('message',"L'annonce a bien �t� enregistr�e.");
		} else {
			$res=$this->dao->ajouter_annonce($user_id,addslashes($titre),addslashes($texte));
			$this->set('message',"L'annonce a bien �t� enregistr�e.");	        	
		}
	}

	function destruct()
	{
		parent::destruct();
	}
}
