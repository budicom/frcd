<?php
require_once(FR_BASE_PATH."/modules/frcd_admin.php");
require_once(FR_LIB_PATH."/validate.php");

class editer_semaine extends FRCD_Admin
{
	function editer_semaine()
	{
		parent::FRCD_Admin();
		$this->tplFile="editer_semaine.php";
	}

	function execute()
	{  
		$semaine_id=Validate::num($_GET['semaine_id']);

		if ($semaine_id)
		{
			$this->set("semaine_id",$semaine_id);

			$infos_semaine=$this->dao->infos_semaine($semaine_id);
			$this->set('semaine',$infos_semaine);

			$questions=$this->dao->liste_des_reponses($semaine_id);


			if ($questions)
			{
				foreach ($questions as $key => $value)
				{
					$elements=array_fill(1, FRCD_NB_ELEMENTS, "");
					$questions[$key]['elements']=$elements;
				}

				$assoc=array();
				foreach($questions as $num=>$question)
				{
					$assoc[$question['id']]=$num;
				}

				$elements=$this->dao->liste_des_elements($semaine_id);
				if ($elements)
					foreach ($elements as $element)
					{
						/* if (isset($element['question_id']))
						if (isset($assoc[$element['question_id']]))
						if (isset($questions[$assoc[$element['question_id']]]))*/
						{
							$questions[$assoc[$element['question_id']]]['elements'][$element['ordre']]['fichier']=FRCD_FICHIERS_CHEMIN.$element['fichier'];
							$questions[$assoc[$element['question_id']]]['elements'][$element['ordre']]['miniature']=FRCD_FICHIERS_CHEMIN.$element['miniature'];
							$questions[$assoc[$element['question_id']]]['elements'][$element['ordre']]['id']=$element['id'];
							$questions[$assoc[$element['question_id']]]['elements'][$element['ordre']]['points']=$element['points'];
							$questions[$assoc[$element['question_id']]]['elements'][$element['ordre']]['indication']=$element['indication'];
						}
					}
			}
			else $questions = array();

			$this->set('questions',$questions);

			$organisateurs=$this->dao->liste_des_organisateurs();
			$this->set('organisateurs',$organisateurs);

			$libelles=$this->dao->liste_des_libelles();
			$this->set('libelles',$libelles);

			$jours=array();
			$mois=array();
			$annees=array();

			for ($i=1;$i<=31;$i+=1) $jours[]=$i;
			for ($i=1;$i<=12;$i+=1) $mois[]=$i;

			for ($i=0;$i<=23;$i+=1) $heures[]=$i;

			$date=getdate();
			$cette_annee=$date['year'];
			for ($i=$cette_annee;$i<=$cette_annee+1;$i+=1) $annees[]=$i;

			$this->set('jours',$jours);
			$this->set('mois',$mois);
			$this->set('annees',$annees);
			$this->set('heures',$heures);

			$debut_ts=$infos_semaine['debut_ts'];
			@list($date, $heure) = explode(" ", $debut_ts);           
			@list($annee_debut, $mois_debut, $jour_debut) = explode("-", $date); 
			@list($heure_debut, $minute_debut, $seconde_debut) = explode(":", $heure);

			$fin_ts=$infos_semaine['fin_ts'];
			@list($date, $heure_fin) = explode(" ", $fin_ts);           
			@list($annee_fin, $mois_fin, $jour_fin) = explode("-", $date); 

			$types = $this->dao->types_de_semaines();
			$this->set('types_de_semaines',$types);

			$this->set('jour_debut',$jour_debut);
			$this->set('mois_debut',$mois_debut);
			$this->set('annee_debut',$annee_debut);
			$this->set('heure_debut',$heure_debut);
			$this->set('jour_fin',$jour_fin);
			$this->set('mois_fin',$mois_fin);
			$this->set('annee_fin',$annee_fin);
			$this->set('heure_fin',$heure_fin);

			$equipes_actives= $this->dao->liste_des_equipes("active");
			$this->set('equipes_actives',$equipes_actives);

		}
		else $this->setErrorMsg("Identificateur de semaine invalide");
	}

	function destruct()
	{
		parent::destruct();
	}
}

?>
