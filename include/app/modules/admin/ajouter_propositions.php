<?php
require_once(FR_BASE_PATH."/modules/frcd_admin.php");
require_once(FR_LIB_PATH."/validate.php");

class ajouter_propositions extends FRCD_Admin
{
	function ajouter_propositions()
	{
		parent::FRCD_Admin();
		$this->tplFile="ajouter_propositions.php";
	}

	function execute()
	{  
		$semaine_id=Validate::num($_GET['semaine_id']);
		if ($semaine_id)
		{
			$equipe_id=Validate::num($_GET['equipe_id']);
			if ($equipe_id)
			{

				// v�rifie le verrou //
				$res=$this->dao->nettoyer_verrous();

				$user=$this->session->get('user');

				$infos_verrou=$this->dao->infos_verrou($semaine_id,$equipe_id);
				if ($infos_verrou)
				{
					if ($user['id']!=$infos_verrou['id'])
					{
						$this->setErrorMsg("L'organisateur '".$infos_verrou["nom"]."' est en train de corriger cette �quipe.");
						return false;
					}
				}
				else 
				{
					$res=$this->dao->nouvelle_edition($user['id'],$semaine_id,$equipe_id);
				}
				/////////////////////////

				$questions=$this->dao->liste_des_reponses($semaine_id);

				$propositions=$this->dao->liste_des_propositions($equipe_id,$semaine_id);

				$elements=$this->dao->liste_des_elements_visibles($semaine_id,$equipe_id);

				$points=$this->dao->points_par_question($semaine_id,$equipe_id);

				$assoc=array();
				foreach ($questions as $num=>$question)
				{
					$assoc[$question['id']]=$num;
				}

				foreach ($points as $point)
				{
					$questions[$assoc[$point['question_id']]]['points']=$point['points'];
				}

				foreach ($elements as $element)
				{
					$questions[$assoc[$element['question_id']]]['elements'][$element['ordre']]['id']=$element['id'];
					$questions[$assoc[$element['question_id']]]['elements'][$element['ordre']]['correct']="";
					$questions[$assoc[$element['question_id']]]['elements'][$element['ordre']]['miniature']=FRCD_FICHIERS_CHEMIN.$element['miniature'];
					$questions[$assoc[$element['question_id']]]['elements'][$element['ordre']]['fichier']=FRCD_FICHIERS_CHEMIN.$element['fichier'];
				}

				$assoc_p=array();

				foreach ($propositions as $num=>$proposition)
				{
					$assoc_p[$proposition['question_id']]=$num;
				}

				$assoc_q=array();

				foreach ($questions as $num=>$question)
				{
					$assoc_q[$question['id']]=$num;
				}

				foreach ($questions as $question)
				{
					if (isset($question['elements'])) 
						foreach ($question['elements'] as $ordre => $element)
						{

							if ($ordre < count($question['elements'])) 
								$questions[$assoc_q[$question['id']]]['elements'][$ordre]['correct']="0";
							else
							{

								if (isset($assoc_p[$question['id']]) && isset($propositions[$assoc_p[$question['id']]]))
								{
									if (($propositions[$assoc_p[$question['id']]]['correct']))
										$questions[$assoc_q[$question['id']]]['elements'][$ordre]['correct']="1";
									else
									{
										if ($propositions[$assoc_p[$question['id']]]['ordre'] >= count($question['elements'])) 
											$questions[$assoc_q[$question['id']]]['elements'][$ordre]['correct']="0";
									}
								}
							}
						}
				}

				// date de derni�re correction
				$derniere_correction = $this->dao->derniere_correction($equipe_id, $semaine_id);

				/*if ($derniere_correction === false)
				{
					$this->setErrorMsg($this->dao->errorMsg());
					return false;
				}*/
				if ($derniere_correction)
				{
					$this->set('derniere_correction',$derniere_correction);
				}

				$this->set('questions',$questions);
				$this->set('equipe_id',$equipe_id);
				$this->set('semaine_id',$semaine_id);
			}
			else $this->setErrorMsg("Identificateur d'�quipe invalide.");

		}
		else $this->setErrorMsg("Identificateur de semaine invalide.");
	}

	function destruct()
	{
		parent::destruct();
	}
}

?>
