<?php
require_once(FR_BASE_PATH."/modules/frcd_admin.php");
require_once(FR_LIB_PATH."/validate.php");

class editer_profil extends FRCD_Admin
{
	function editer_profil()
	{
		parent::FRCD_Admin();
	}

	function execute()
	{
		if (isset($_POST['equipe_id'])) $user_id=Validate::num($_POST['equipe_id']);
		else {
			$user=$this->session->get('user');
			$user_id=$user['id'];
		}

		if ($user_id)
		{
			$profil=$this->dao->get_user_profile($user_id);
			if ($profil)
				{
					$this->set('equipe',$profil);
				}
			else $this->setErrorMsg("Compte invalide");
		}
		else $this->setErrorMsg("Identificateur invalide");

	}

	function destruct()
	{
		parent::destruct();
	}
}

?>
