<?php 
$dao=new frcd_Dao();
$this->dao = $dao;

$user=$this->session->get('user');

// identification : s'il y a un cookie, on récupère l'équipe, et on trace une connexion
if ((!$user) &&(isset($_COOKIE['qp'])))
{
	$code_crypte = $_COOKIE['qp'];
	$user = $this->dao->get_user_crypte($code_crypte);
	if ($user)
	{
		if ($user['status'] == 'active')
		{
			$this->session->set('user',$user);
			// on logue la connexion
			$navigateur = getenv('HTTP_USER_AGENT');
			$ip_client = getenv('REMOTE_ADDR');
			// TODO gestion erreur
			$this->dao->insert_connexion($user['id'], $ip_client, $navigateur, 0);
		} else {
			// l'équipe est desactivée ; on peut se connecter au jeu, mais on reset le cookie
			$user = null;
			setcookie('qp', '', mktime(12,0,0,1, 1, 1990));
		}
	} else {
		// code équipe non valide ; on reset le cookie
		$user = null;
		setcookie('qp', '', mktime(12,0,0,1, 1, 1990));
		// et on logue un avertissement à destination des admins
		// (sauf bug, qq'un ne doit pas pouvoir avoir un code non valide dans son cookie)
		// TODO
	}
}

/*// si admin on maj la date de dernière activité
if ($user['admin'] == 1)
{
	// TODO gestion erreur
	$this->dao->update_derniere_activite($user['id']);
}*/

//  maj la date de dernière activité
$this->dao->update_derniere_activite($user['id']);

// récupération du cookie pour la notification d'annonces
if (isset($_COOKIE['derniere_annonce_lue']))
{
	$derniere_annonce_lue=$_COOKIE['derniere_annonce_lue'];
}
else $derniere_annonce_lue=0;

//menu
$semaines=$this->dao->liste_des_semaines();
$this->set('semaines',$semaines);

//organisateurs connectés
$connectes=$this->dao->nombre_de_connectes();
$this->set('connectes',$connectes);

// ouverture automatique des semaines
$res=$this->dao->ouverture_semaines_automatique();

//liste des annonces
$annonces=$this->dao->liste_des_annonces();
$annonces_non_lues=$this->dao->nombre_annonces_non_lues($derniere_annonce_lue);
$this->set('annonces',$annonces); 
$this->set('annonces_non_lues',$annonces_non_lues['nombre']);
