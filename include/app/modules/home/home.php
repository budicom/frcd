<?php
require_once(FR_BASE_PATH."/modules/frcd_auth_no.php");

class home extends FRCD_Auth_No
{
	function home()
	{
		parent::FRCD_Auth_No();
	}

	function execute()
	{
		$this->redirect("index.php?module=semaine");
	}

	function destruct()
	{
		FR_Auth_Admin::destruct();
	}
}

?>
