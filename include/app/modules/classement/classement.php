<?php
require_once(FR_BASE_PATH."/modules/frcd_auth_no.php");
require_once(FR_LIB_PATH."/validate.php");

class classement extends FRCD_Auth_No
{
	function classement ()
	{
		parent::FRCD_Auth_No();
	}

	function execute()
	{
		$scores=array();

		// classement
		$totaux_temp = $this->dao->classement();
		if ($totaux_temp === false) $this->setErrorMsg($this->dao->errorMsg());
			$equipes=array();
			$rang=1;
			$n=1;
			$score_precedent=-1;
			foreach ($totaux_temp as $totaux_ligne)
			{
				if ($score_precedent==$totaux_ligne['points'])
				{
					$rang='-' ;
				} else {
					$score_precedent=$totaux_ligne['points'];
					//$rang=$n;
					$rang=$totaux_ligne['rang'];
				}
				//$equipes[$totaux_ligne['equipe_id']]=array('nom'=>$totaux_ligne['equipe_nom'], 'points'=>$totaux_ligne['points'],'rang'=>$rang ,'progression'=>$totaux_ligne['progression'],'bonus'=>$totaux_ligne['bonus']);
				$equipes[$totaux_ligne['equipe_id']]=array('nom'=>$totaux_ligne['equipe_nom'], 'points'=>$totaux_ligne['points'],'rang'=>$rang ,'progression'=>"",'bonus'=>$totaux_ligne['bonus']);
				$n+=1;
			}

			if (empty($equipes)) $this->setErrorMsg("Le classement n'est pas encore disponible");

			// scores detailles
			$scores_temp = $this->dao->classement_detail();

			if ($scores_temp === false) $this->setErrorMsg($this->dao->errorMsg());
				$scores=array();
				foreach ($scores_temp as $score_line)
				{
					$scores[$score_line['equipe_id']][$score_line['semaine_id']]['points']=$score_line['points'];
				}

				$this->set('equipes',$equipes);
				$this->set('scores',$scores);

	}

	function destruct()
	{
		FR_Auth_Admin::destruct();
	}
}
