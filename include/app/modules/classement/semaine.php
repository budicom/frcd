<?php
require_once(FR_BASE_PATH."/modules/frcd_auth_no.php");
require_once(FR_LIB_PATH."/validate.php");

class semaine extends FRCD_Auth_No
{
	function semaine ()
	{
		parent::FRCD_Auth_No();
	}

	function execute()
	{
		$scores=array();

		//scores
		if (isset($_GET['semaine_id']))
		{
			// v�rifie si on doit afficher les scores
			$semaine_id=Validate::num($_GET['semaine_id']);
			if ($semaine_id)
			{
				$infos_semaine=$this->dao->infos_semaine($semaine_id);
				if ($infos_semaine === false)
				{
					$this->setErrorMsg("La semaine n'existe pas.");
					return false;
				} else {
					$semaine_etat=$infos_semaine['etat'];
					if ($semaine_etat!='finie')
					{
						$this->setErrorMsg("La semaine n'est pas termin�e.");
						return false;
					}
				}
			} else {
				$this->setErrorMsg("Identificateur de semaine invalide.");
				return false;
			}

			// r�cup�re les questions de la semaine
			$questions=$this->dao->liste_des_questions($semaine_id);
			if ($questions === false) 
			{
				$this->setErrorMsg($this->dao->errorMsg());
				return false;
			}

			// r�cup�re le classement de la semaine
			$totaux_temp = $this->dao->classement_semaine($semaine_id);
			if ($totaux_temp === false) 
			{
				$this->setErrorMsg($this->dao->errorMsg());
				return false;
			}

			$equipes=array(); 
			$rang=1;
			$n=1;
			$score_precedent=-1;

			// r�arrange le classement dans le tableau 'equipes'
			foreach ($totaux_temp as $totaux_ligne)
			{
				if ($score_precedent==$totaux_ligne['points'])
				{
					$rang='-' ;
				} else {
					$score_precedent=$totaux_ligne['points'];
					$rang=$n;
				}
				$equipes[$totaux_ligne['equipe_id']]=array('nom'=>$totaux_ligne['equipe_nom'], 'points'=>$totaux_ligne['points'],'bonnes_reponses'=>$totaux_ligne['bonnes_reponses'],'rang'=>$rang);
				$n+=1;
			}

			if (empty($equipes)) $this->setErrorMsg("Classement non disponible");

			// r�cup�re les scores par questions
			$scores_temp = $this->dao->classement_semaine_detail($semaine_id);
			if ($scores_temp === false) 
			{
				$this->setErrorMsg($this->dao->errorMsg());
				return false;
			}
			// r�arrange les scores par questions dans le tableau 'scores'
			$scores=array();
			foreach ($scores_temp as $score_line)
			{
				$scores[$score_line['equipe_id']][$score_line['question_id']]['points']=$score_line['points'];
			}
				// envoie les donn�es � la vue
			$this->set('semaine',$infos_semaine);
			$this->set('equipes',$equipes);
			$this->set('scores',$scores);
			$this->set('questions',$questions);

        }

	}

	function destruct()
	{
		FR_Auth_Admin::destruct();
	}
}

?>
