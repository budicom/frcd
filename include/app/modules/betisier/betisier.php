<?php
require_once(FR_BASE_PATH."/modules/frcd_auth_no.php");
require_once(FR_LIB_PATH."/validate.php");

class betisier extends FRCD_Auth_No
{
	function betisier ()
	{
		parent::FRCD_Auth_No();
	}

	function execute()
	{
		$user=$this->session->get('user');

		if (count($_POST))
		{
			$betise_id=Validate::text($_POST['betise_id']);
			if ($betise_id && $user)
			{
				// un vote � enregistrer pour cette b�tise
				$res = $this->dao->voter_pour_betise($betise_id, $user['id']);
				if ($res === false)
				{
					$this->setErrorMsg($this->dao->errorMsg());
					return false;
				}
			}
	}
        
        if (isset($_GET['semaine_id'])) {
            $semaine_id=Validate::num($_GET['semaine_id']);
            if ($semaine_id) {
                  $infos_semaine=$this->dao->infos_semaine($semaine_id);
                  if (empty($infos_semaine)) {
                        $this->setErrorMsg("La semaine n'existe pas.");
                    return false;
                  } else {
                    $semaine_etat=$infos_semaine['etat'];
                    if ($semaine_etat != 'finie') {
                      $this->setErrorMsg("La semaine n'est pas encore termin�e.");
                      return false;
                    }
                  }
            } else {
              $this->setErrorMsg("Identificateur de semaine invalide.");
              return false;
            }
        } else {
          $this->setErrorMsg("Identificateur de semaine non trouv�.");
          return false;
        }
    
        // r�cup�re les questions
        $questions=$this->dao->liste_des_questions($semaine_id);
        if ($questions === false) {
            $this->setErrorMsg($this->dao->errorMsg());
            return false;
        }
                           
        // r�cup�re les r�ponses
        $reponses=$this->dao->liste_des_reponses($semaine_id);
        if ($reponses === false) {
            $this->setErrorMsg($this->dao->errorMsg());
            return false;
        }
                
        // pour les connect�s
        if ($user) {
            $user_id=$user['id'];
            $votes=$this->dao->liste_des_votes($user_id, $semaine_id);
            if ($votes === false) {
                $this->setErrorMsg($this->dao->errorMsg());
                return false;
            }
        }
        
        $elements=$this->dao->liste_des_elements_avec_betise($semaine_id);
        if ($elements === false) {
            $this->setErrorMsg($this->dao->errorMsg());
            return false;
        }
        
        $betises=$this->dao->liste_des_betises($semaine_id);
        if ($betises === false) {
            $this->setErrorMsg($this->dao->errorMsg());
            return false;
        }


        $assoc=array();
        foreach ($betises as $num=>$betise)
        {
            $assoc[$betise['id']]=$num;
        }
        
        if (isset($votes))
        foreach ($votes as $vote) {
            $betises[$assoc[$vote['betise_id']]]['vote']=true;
        }

        $assoc=array();
        foreach ($elements as $num=>$element)
        {
            $assoc[$element['id']]=$num;
        }

        foreach ($betises as $betise) {
            $temp=$betise;

            $temp['id']=$betise['id'];


            if ((($user) && ($user['id'] != $temp['equipe_id'])) && (count($votes) < FRCD_BETISIER_NB_VOTES) && (!isset($betise['vote']))) 
              $temp['peut_voter']=true;
            else $temp['peut_voter']=false;
              $elements[$assoc[$betise['element_id']]]['betises'][]=$temp;
        }        


        $assoc=array();
        foreach ($questions as $num=>$question)
        {
            $assoc[$question['id']]=$num;
        }

        foreach ($elements as $element) {
            $questions[$assoc[$element['question_id']]]['elements'][$element['ordre']]['miniature']= FRCD_FICHIERS_CHEMIN.$element['miniature'];
            $questions[$assoc[$element['question_id']]]['elements'][$element['ordre']]['fichier']= FRCD_FICHIERS_CHEMIN.$element['fichier'];
            $questions[$assoc[$element['question_id']]]['elements'][$element['ordre']]['indication']= $element['indication'];
            $questions[$assoc[$element['question_id']]]['elements'][$element['ordre']]['id']= $element['id'];
            $questions[$assoc[$element['question_id']]]['elements'][$element['ordre']]['betises']= $element['betises'];
        }
        


        // envoie les donn�es � la vue
        $this->set('semaine',$infos_semaine);
        $this->set('reponses',$reponses);
        $this->set('questions',$questions);
       // $this->set('betises',$betises);
        if ($user) {
            $this->set('user',$user);
            $this->set('votes',$votes);
        }
      }

	function destruct()
	{
		FR_Auth_Admin::destruct();
	}
}

?>
