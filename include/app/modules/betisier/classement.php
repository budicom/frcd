<?php
require_once(FR_BASE_PATH."/modules/frcd_auth_no.php");
require_once(FR_LIB_PATH."/validate.php");

class classement extends FRCD_Auth_No
{
	function classement ()
	{
		parent::FRCD_Auth_No();
	}

	function execute()
	{
		//$user=$this->session->get('user');

		$lignes_classement_temp = $this->dao->classement_du_betisier();
		if ($lignes_classement_temp === false)
		{
			$this->setErrorMsg($this->dao->errorMsg());
			return false;
		}

		$lignes_classement=array();
		$rang=1;
		$n=1;
		$score_precedent=-1;
		foreach ($lignes_classement_temp as $ligne)
		{
			if ($ligne['score']>0) 
			{
				if ($score_precedent==$ligne['score'])
				{
					$rang='-' ;
				} else {
					$score_precedent=$ligne['score'];
					$rang=$n;
				}

				$ligne['rang']=$rang;
				$lignes_classement[]=$ligne;
				$n+=1;
			}
		}

		if (empty($lignes_classement)) $this->setErrorMsg("Le classement n'est pas encore disponible.");
		$this->set('lignes_classement',$lignes_classement);

		$meilleures_betises = $this->dao->meilleures_betises(3);
		$this->set('meilleures_betises',$meilleures_betises);

	}

	function destruct()
	{
		FR_Auth_Admin::destruct();
	}
}

?>
