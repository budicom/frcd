<?php

require_once(FR_BASE_PATH."/modules/frcd_auth_user.php");

class logout extends FRCD_Auth_User
{
	function logout()
	{
		parent::FRCD_Auth_User();
	}

	function execute()
	{
		if (isset($_COOKIE['qp']))
		{
			$user = $this->session->get('user');
			// en conditions de jeu on n'autorise la d�co que pour les admins
			// (d�sactiv� en tests)
			//if ($user && $user['admin'] != 1) die("D�connexion interdite pour les joueurs !");

			// on vide la date de derni�re activit� de l'�quipe 
			// pour �viter qu'elle ne continue � appara�tre en ligne
			//if ($user && $user['admin'] == 1) $this->dao->reset_derniere_activite($user['id']);
			if ($user) $this->dao->reset_derniere_activite($user['id']);

			// on vide le cookie
			setcookie('qp', null, mktime(12,0,0,1, 1, 1990));
		}
		$this->session->destroy();

		if (isset($_GET['pg']))
		{
			$go = urldecode($_GET['pg']);
		} else {
			$go = '.';
		}

		$this->redirect($go);
		exit();
	}

	function destruct()
	{
		parent::destruct();
	}
}

?>
