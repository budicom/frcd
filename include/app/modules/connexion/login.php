<?php

require_once(FR_BASE_PATH."/modules/frcd_auth_no.php");
require_once(FR_LIB_PATH."/validate.php");

class login extends FRCD_Auth_No
{
	function login()
	{
		parent::FRCD_Auth_No();
	}

	function execute()
	{
		// on v�rifie qu'un cookie n'existe pas d�j� (on ne peut pas se reconnecter avec une autre �quipe !)
		if (isset($_COOKIE['qp'])) $this->setErrorMsg("Vous �tes d�j� connect�");
		else
		if (count($_POST)) {
			$code=Validate::text($_POST['code']);
			if ($code) {

				$user=$this->dao->get_user($code);

				if ($user)
				{
					if ($user['status'] == 'active')
					{
						if (isset($_GET['pg']))
						{
							$go = urldecode($_GET['pg']);

						} elseif (isset($_POST['pg'])) {
							$go = urldecode($_POST['pg']);
						} else {
							$go = '.';
						}

						$this->session->set('user',$user);

						// on logue une connexion (avec flag login �quipe)
						//$url = getenv('REQUEST_URI');
						$navigateur = getenv('HTTP_USER_AGENT');
						$ip_client = getenv('REMOTE_ADDR');
						$this->dao->insert_connexion($user['id'], $ip_client, $navigateur, 1);

						// on met le code crypt� dans le cookie
						// TODO : rajouter des infos bidon pour tromper l'ennemi ? :P
						setcookie('qp', sha1($user['code']), time()+ 60*60*24*365); // 1 an

						$this->redirect($go);
						exit();
					} else {
                           $this->setErrorMsg('Votre compte a &eacute;t&eacute; suspendu.');
					}
				} else {
					$this->setErrorMsg("Le code n'existe pas dans la base de donn&eacute;es.");
				}
			}
		}
	}

	function destruct()
	{
		FR_Auth_No::destruct();
	}
}

?>
