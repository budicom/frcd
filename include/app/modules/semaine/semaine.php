<?php
require_once(FR_BASE_PATH."/modules/frcd_auth_no.php");
require_once(FR_LIB_PATH."/validate.php");

class semaine extends FRCD_Auth_No
{
	function semaine ()
	{
		parent::FRCD_Auth_No();
	}

	function execute()
	{
		$scores=array();
		//questions

		//Choix de la semaine � afficher
		$semaine_etat='non_commencee';

		if (isset($_GET['semaine_id']))
		{
			$semaine_id=Validate::num($_GET['semaine_id']);
			if ($semaine_id)
			{
				$infos_semaine=$this->dao->infos_semaine($semaine_id);
				if (empty($infos_semaine))
				{
					$this->setErrorMsg("La semaine n'existe pas.");
					return false;
				} else {
					$semaine_etat=$infos_semaine['etat'];
					if ($semaine_etat=='non_commencee')
					{
						$this->setErrorMsg("La semaine n'a pas encore commenc�.");
						return false;
					}
				}
			} else {
				$this->setErrorMsg("Identificateur de semaine invalide.");
				return false;
			}
		} else {
		// sinon premi�re semaine encore ouverte
			$premiere_semaine_ouverte=$this->dao->premiere_semaine_ouverte();
			if ($premiere_semaine_ouverte === null)
			{
				$this->setErrorMsg($this->dao->errorMsg());
				return false;
			}
			if (!empty($premiere_semaine_ouverte)) 
			{
				$semaine_id=$premiere_semaine_ouverte['id'];
				$semaine_etat='en_cours';
			} else {
			// sinon derni�re semaine
				$derniere_semaine=$this->dao->derniere_semaine();
				if (!empty($derniere_semaine)) 
				{
					$semaine_id=$derniere_semaine['id'];
					$semaine_etat='finie';
				} else {
				// Le jeu n'a pas commenc�
					$this->setErrorMsg("Le jeu n'a pas encore commenc�.");
					return false;  
				}
			}
			$infos_semaine=$this->dao->infos_semaine($semaine_id);
		}

		// r�cup�re les questions
		$questions=$this->dao->liste_des_questions($semaine_id);
		if ($questions === null)
		{
			$this->setErrorMsg($this->dao->errorMsg());
			return false;
		}

		// r�cup�re les r�ponses
		if ($semaine_etat=='finie')
		{
			$reponses=$this->dao->liste_des_reponses($semaine_id);
			if ($reponses === null)
			{
				$this->setErrorMsg($this->dao->errorMsg());
				return false;
			}
		}
		else $reponses=array();

		$user=$this->session->get('user');
		// pour les connect�s
		if ($user && !$user['admin'])
		{
			$user_id=$user['id'];
			$scores = $this->dao->points_par_question($semaine_id,$user_id);
		} else {
		// pour les anonymes
			$user_id = NULL;
		}

		if ($semaine_etat=='finie') $elements=$this->dao->liste_des_elements($semaine_id);
		else $elements=$this->dao->liste_des_elements_visibles($semaine_id,$user_id);

		if ($elements === null)
		{
			$this->setErrorMsg($this->dao->errorMsg());
			return false;
		}

		//remplissage des elements
		$assoc=array();
		foreach ($questions as $num=>$q)
		{
			$assoc[$q['id']]=$num;
		}

		foreach ($elements as $element)
		{
			if (isset($element['question_id']))
				if (isset($assoc[$element['question_id']]))
					if (isset($questions[$assoc[$element['question_id']]]))
					{
						$questions[$assoc[$element['question_id']]]['elements'][$element['ordre']]['miniature']=FRCD_FICHIERS_CHEMIN.$element['miniature'];
						$questions[$assoc[$element['question_id']]]['elements'][$element['ordre']]['fichier']=FRCD_FICHIERS_CHEMIN.$element['fichier'];
						//$questions[$element['question_id']]['elements'][$element['ordre']]['id']=$element['id'];
						$questions[$assoc[$element['question_id']]]['elements'][$element['ordre']]['indication']=$element['indication'];
						$questions[$assoc[$element['question_id']]]['elements'][$element['ordre']]['points']=$element['points'];
					}
		}

		foreach ($scores as $score)
		{
			if (isset($score['question_id']))
				if (isset($assoc[$score['question_id']]))
					if (isset($questions[$assoc[$score['question_id']]]))
					{
					if ($infos_semaine['etat']=="en_cours" && $questions[$assoc[$score['question_id']]]["cacher_les_points"])
						$questions[$assoc[$score['question_id']]]['points']=null;
					else
						$questions[$assoc[$score['question_id']]]['points']=$score['points'];
					}
		}

		foreach ($reponses as $reponse)
		{
			if (isset($reponse['id']))
				if (isset($assoc[$reponse['id']]))
					if (isset($questions[$assoc[$reponse['id']]]))
					{
						$questions[$assoc[$reponse['id']]]['reponse']=$reponse['reponse'];
						$questions[$assoc[$reponse['id']]]['titre_original']=$reponse['titre_original'];
						$questions[$assoc[$reponse['id']]]['lien_imdb']=$reponse['lien_imdb'];
						$questions[$assoc[$reponse['id']]]['realisateur']=$reponse['realisateur'];
						$questions[$assoc[$reponse['id']]]['annee']=$reponse['annee'];
						$questions[$assoc[$reponse['id']]]['commentaire']=$reponse['commentaire'];
						$questions[$assoc[$reponse['id']]]['nom']=$reponse['nom'];
					}
		}

		// envoie les donn�es � la vue
		$this->set('semaine',$infos_semaine);
		$this->set('reponses',$reponses);
		$this->set('questions',$questions);

	}

	function destruct()
	{
		FR_Auth_Admin::destruct();
	}
}

?>
