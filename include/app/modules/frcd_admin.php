<?php

require_once(FR_BASE_PATH."/modules/frcd_auth_user.php");

class FRCD_Admin extends FRCD_Auth_User
{
	function FRCD_Admin()
	{
		parent::FRCD_Auth_User();

		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
		header("Cache-Control: no-store, no-cache, must-revalidate");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("pragma: no-cache"); 

		$user=$this->session->get('user');
		if ($user)
		{
			if (($user['admin'] == 1)&&($user['status'] == 'active'))
			{
				return true;
			}
		}

		die("Vous n'avez pas le droit d'acc&eacute;der &agrave; cette page");
	}

	function execute()
	{

	}

	function destruct()
	{
		parent::destruct();
	}
}

?>
