<?php

require_once(FR_LIB_PATH."/object/db.php");

class frcd_Dao extends FR_Object_DB
{
	function frcd_Dao()
	{
		FR_Object_DB::FR_Object_DB();
	}

	function echappe($data)
	{
		if (is_string($data))  return "'".addslashes($data)."'";
		elseif ($data===false) return "null";
		elseif ($data===null) return "null";
		elseif (is_array($data)) 
		{
			$res=array();
			foreach ($data as $key=> $value)
			{
				if (is_string($value)) $res[$key]="'".addslashes($value)."'";
				elseif ($value===false) $res[$key]="null";
				elseif ($value===null) $res[$key]="null";
				else $res[$key]=$value;
			}
			return $res;
		}
		else return $data;
	}

	function execute($sql)
	{
		$request = mysql_query($sql,$this->db);
		if (!$request)
		{
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message .= 'Whole query: ' . $sql;
			$this->setErrorMsg($message);
			return Null;
		}

		return True;
	}

	function getArray($sql)
	{
		$result = array();
		$request = mysql_query($sql,$this->db);
		if (!$request)
		{
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message .= 'Whole query: ' . $sql;
			$this->setErrorMsg($message);
			return Null;
		}
		while ($row = mysql_fetch_assoc($request))
		{
			$result[] = $row;
		}
		return $result;
	}

	function getRow($sql)
	{
		$request = mysql_query($sql,$this->db);
		if (!$request)
		{
			$message  = 'Invalid query: ' . mysql_error() . "\n";
			$message .= 'Whole query: ' . $sql;
			$this->setErrorMsg($message);
			return Null;
		}
		$row = mysql_fetch_assoc($request);
		return $row;
	}

	function parse_enum($str)
	{
		$reg = "^enum\('(.*)'\)$";
		$str = ereg_replace($reg,'\1',$str);
		return split("[']?,[']?",$str);
	}

/////////////////////////////Gestion des equipes////////////////////////////////

	function get_user($code)
	{
		$sql = "SELECT *
				FROM equipes
				WHERE code='".$code."'";

		$res = $this->getRow($sql);

		return $res;
	}

	function get_user_profile($id)
	{
		$sql = "SELECT *
				FROM equipes
				WHERE id='".$id."'";

		$res = $this->getRow($sql);

		return $res;
	}

	function get_user_crypte($code_crypte)
	{
		$sql = "SELECT *
				FROM equipes
				WHERE sha1(code)=\"$code_crypte\"";

		$res = $this->getRow($sql);

		return $res;
	}

	function nouvelle_equipe($nom,$code,$admin)
	{
		$sql = "INSERT INTO equipes ( `nom` , `code` , `admin` ) VALUES ('".$nom."','".$code."',".$admin.")";
		$res=$this->execute($sql);

		return $res;
	}

	function update_compte($equipe_id,$status)
	{
		$sql = "UPDATE equipes
				SET status = '$status'
				WHERE id = $equipe_id";
		$res=$this->execute($sql);

		return $res;
	}

	function update_description($equipe_id,$description)
	{
		$sql = "UPDATE equipes
				SET description = '$description'
				WHERE id = $equipe_id";
		$res=$this->execute($sql);

		return $res;
	}

	function update_bonus($equipe_id,$bonus)
	{
		$sql = "UPDATE equipes
				SET bonus = '$bonus'
				WHERE id = $equipe_id";
		$res=$this->execute($sql);

		return $res;
	}

	function liste_des_equipes($status='active')
	{
		$sql = "SELECT *
				FROM equipes
				WHERE admin=0
				AND status='$status'
				ORDER BY nom ASC";
		$res= $this->getArray($sql);

		return $res;
	}

	function liste_des_organisateurs()
	{
		$sql = "SELECT *
				FROM equipes
				WHERE admin=1";
		$res= $this->getArray($sql);
		return $res;
	}

	function update_derniere_activite($equipe_id)
	{
		$sql = "UPDATE equipes
				SET derniere_activite = now()
				WHERE id = $equipe_id";
		$res=$this->execute($sql);

		return $res;
	}

	function reset_derniere_activite($equipe_id)
	{
		$sql = "UPDATE equipes
				SET derniere_activite = null
				WHERE id = $equipe_id";
		$res=$this->execute($sql);

		return $res;
	}

/////////////////////////////Gestion des semaines///////////////////////////////

	function nouvelle_semaine($titre_semaine,$theme_semaine,$description_semaine,$image,$date_debut,$date_fin,$type)
	{
		$sql = "INSERT INTO semaines (`titre`, `theme`, `description`, `image`, `debut`, `fin`, `deroulement`)
				VALUES (\"$titre_semaine\",
						\"$theme_semaine\",
						\"$description_semaine\",
						\"$image\",
						'$date_debut',
						'$date_fin',
						'$type')";
		$res=$this->execute($sql);

		return $res;
	}

	function update_semaine($semaine_id,$titre_semaine,$theme_semaine,$description_semaine,$image,$date_debut,$date_fin,$type)
	{
		$sql = "UPDATE semaines
				SET `titre`=\"$titre_semaine\",
					`theme`=\"$theme_semaine\",
					`description`=\"$description_semaine\",
					`image`=\"$image\",
					`debut`='$date_debut',
					`fin`='$date_fin',
					`deroulement`='$type'
				WHERE id=$semaine_id";
		$res=$this->execute($sql);

		return $res;
	}

	function infos_semaine($semaine_id)
	{
		$sql = "SELECT *, date_format(debut,'%d/%m/%Y � %Hh%i') as debut, TIMESTAMP(debut) as debut_ts, date_format(fin,'%d/%m/%Y � %Hh%i') as fin, TIMESTAMP(fin) as fin_ts, (select count(S1.id)+1 from semaines S1, semaines S2 where S1.id!=$semaine_id and S1.debut<S2.debut and S2.id=$semaine_id) as numero 
				FROM semaines 
				WHERE id=$semaine_id";
		$res=$this->getRow($sql);

		return $res;
	}

	function supprimer_semaine($semaine_id)
	{
		//supprime les questions
		$sql = "delete from questions where semaine_id=$semaine_id";
		$res=$this->execute($sql);

		//supprime la semaine
		$sql = "delete from semaines where id=$semaine_id";
		$res=$this->execute($sql);

		return $res;
	}

	function set_etat_semaine($semaine_id,$etat)
	{
		$sql = "UPDATE semaines
				SET etat = '$etat'
				WHERE id = $semaine_id";
		$res=$this->execute($sql);

		return $res;
	}

	function set_ordre_semaine_synchrone($semaine_id,$ordre)
	{
		$sql = "UPDATE semaines
				SET ordre_actuel = '$ordre'
				WHERE id = $semaine_id";
		$res=$this->execute($sql);

		return $res;
	}

	function set_questions_semaine_progressive($semaine_id,$questions)
	{
		$sql = "UPDATE semaines
				SET questions_a_afficher = '$questions'
				WHERE id = $semaine_id";
		$res=$this->execute($sql);
		return $res;
	}

	function liste_des_semaines($etat=NULL)
	{
		if (isset($etat)) $sql_etat="WHERE etat='".$etat."'";
		else $sql_etat="";

		$sql="SET @rank=0";
		$this->execute($sql);
		$sql="SELECT *,@rank:=@rank+1 AS num FROM semaines $sql_etat ORDER BY debut ASC";
		$res=$this->getArray($sql);

		return $res;
	}

	function premiere_semaine_ouverte()
	{
		$sql= "select id from semaines where etat = 'en_cours' order by debut asc limit 1";
		$res=$this->getRow($sql);

		return $res;
	}

	function derniere_semaine()
	{
		$sql = "SELECT id
				FROM semaines
				WHERE etat = 'finie' order by debut desc limit 1";
		$res=$this->getRow($sql);

		return $res;
	}

	function ouverture_semaines_automatique()
	{
		$sql = "UPDATE semaines
				SET etat='en_cours'
				WHERE NOW()>=debut
					AND etat='non_commencee'";
		$res=$this->execute($sql);

		return $res;
	}

	function types_de_semaines()
	{
		$sql="DESCRIBE semaines 'deroulement'";
		$res=$this->getRow($sql);
		if (isset($res['Type']))
			return $this->parse_enum($res['Type']);
		else return array();
	}

////////////////////////////Gestion des questions///////////////////////////////

	function nouvelle_question($semaine_id,$libelle_id,$organisateur_id,$reponse)
	{
		$sql = "SELECT ordre from questions
				WHERE semaine_id = $semaine_id
				ORDER BY ordre desc limit 1";
		$res=$this->getRow($sql);
		if (isset($res['ordre'])) $nouvel_ordre=$res['ordre']+1;
		else $nouvel_ordre=1;
			$sql = "INSERT INTO questions (`semaine_id` ,`libelle_id` ,`organisateur_id`, `reponse`, `ordre`)
					VALUES (\"$semaine_id\",
							'$libelle_id',
							'$organisateur_id', '$reponse', '$nouvel_ordre')";
		$res=$this->execute($sql);

		return $res;
	}

	function supprimer_question($question_id)
	{
		$sql = "SELECT ordre,semaine_id
				FROM questions
				WHERE id = $question_id";
		$res=$this->getRow($sql);

		if ($res === null)
		{
			return $res;
		}

		$ordre=$res['ordre'];
		$semaine_id=$res['semaine_id'];
		$sql = "delete from questions where id = $question_id";
		$res=$this->execute($sql);

		if ($res === null)
		{
			return $res;
		}

		//r�organiser l'ordre des questions
		$sql = "UPDATE questions
				SET ordre = ordre -1
				WHERE semaine_id = $semaine_id
				AND ordre > $ordre";
		$res=$this->execute($sql);

		if ($res === null)  
		{
			return $res;
		}

		//supprimer les elements
		$sql = "delete from elements where question_id = $question_id";
		$res=$this->execute($sql);

		// TODO supprimer les fichiers et leur miniature
		if ($res === null)
		{
			return $res;
		}

		return true;         
	}

	function liste_des_libelles()
	{
		$sql = "SELECT *
				FROM libelles";
		$res= $this->getArray($sql);

		return $res;
	}

	// liste des entetes des questions
	function liste_des_questions($semaine_id)
	{
		$sql = "SELECT q.id, q.ordre, q.annulee, q.cacher_les_points, l.icone, l.texte as libelle
				FROM semaines S, questions q LEFT JOIN libelles l ON q.libelle_id=l.id
				WHERE q.semaine_id = S.id
					AND S.id = $semaine_id
					AND (S.etat != 'non_commencee')
				ORDER by q.ordre asc";

		$questions=$this->getArray($sql,false,true);
		return $questions;
	}

	// liste des questions avec r�ponses
	function liste_des_reponses($semaine_id)
	{
		$sql = "SELECT q.*, l.icone, l.texte as libelle, e.nom, e.id as auteur_id
				FROM questions q 
				LEFT JOIN equipes e ON q.organisateur_id = e.id
				LEFT JOIN libelles l ON q.libelle_id=l.id
				WHERE q.semaine_id = $semaine_id 
				ORDER BY q.ordre asc";

		$questions=$this->getArray($sql,false,true);
		return $questions;
	}

	function update_question($question_id,$libelle_id,$reponse,$commentaire,$titre_original,$realisateur,$annee,$lien_imdb,$cacher_les_points)
	{
		$sql = "UPDATE questions
				SET commentaire='$commentaire',
					libelle_id='$libelle_id',
					reponse='$reponse',
					titre_original='$titre_original',
					realisateur='$realisateur',
					annee='$annee',
					lien_imdb='$lien_imdb',
					cacher_les_points='$cacher_les_points'
				WHERE id=$question_id";
		$res=$this->execute($sql);

		return $res;
	}

	function melanger_les_questions($semaine_id)
	{
		$sql = "SELECT Q.id
				FROM questions Q
				WHERE Q.semaine_id = $semaine_id
				ORDER BY rand()";
		$questions = $this->getArray($sql);

		$numero_ordre = 0;
		foreach($questions as $question)
		{
			$numero_ordre = $numero_ordre + 1;
			$question_id = $question['id'];
			$sql = "UPDATE questions
					SET ordre = $numero_ordre
					WHERE id = $question_id";
			$res  = $this->execute($sql);
		}
		return true;
	}

	function question_quelle_semaine($question_id)
	{
		$sql = "SELECT semaine_id
				FROM questions
				WHERE id = $question_id";

		$res = $this->getRow($sql);
		return $res;
	}

	function annuler_question($question_id)
	{
		$sql = "UPDATE questions
				SET annulee=1
				WHERE id = $question_id";

		$res = $this->execute($sql);
		return $res;
	}

	function retablir_question($question_id)
	{
		$sql = "UPDATE questions
				SET annulee=0
				WHERE id = $question_id";

		$res = $this->execute($sql);
		return $res;
	}

////////////////////////////Gestion des �l�ments (indices)//////////////////////

	// renvoie l'element d'une question
	function get_element($question_id,$ordre)
	{
		$sql = "SELECT * 
				FROM elements
				WHERE question_id=$question_id
					AND ordre=$ordre";
		$res=$this->getRow($sql);

		return $res;
	}

	// enregistrer un element
	function enregistrer_element($question_id,$ordre,$points,$nouveau_fichier,$miniature,$fichier_existant="")
	{
		/*if ($fichier_existant!="")
		{
			$sql = "UPDATE elements
					SET fichier=\"$nouveau_fichier\", miniature=\"$miniature\", points=\"$points\"
					WHERE question_id = $question_id and ordre = $ordre";
		} else {*/
		$sql = "REPLACE into elements (`ordre`, `points`, `question_id`, `fichier`, `miniature`)
				VALUES ($ordre, $points, $question_id, '$nouveau_fichier', '$miniature') ";
		/*}*/
		$res=$this->execute($sql);

		return $res;
	}

	//modifier l'indication d'un element
	function enregistrer_indication($question_id,$ordre,$indication)
	{
		$sql = "UPDATE elements set indication=\"$indication\"
				WHERE question_id = $question_id
				AND ordre = $ordre";
		$res=$this->execute($sql);

		return $res;
	}

	//modifier les points d'un element
	function enregistrer_points($question_id,$ordre,$points)
	{
		$sql = "UPDATE elements set points=\"$points\"
				WHERE question_id = $question_id
				AND ordre = $ordre";
		$res=$this->execute($sql);

		return $res;
	}

	//renvoie tous les elements d'une semaine
	function liste_des_elements($semaine_id)
	{
		$sql = "SELECT E.*
				FROM elements E, questions Q
				WHERE E.question_id = Q.id
				AND Q.semaine_id = $semaine_id 
				ORDER by E.ordre asc";

		$res=$this->getArray($sql);
		return $res;
	}

	//renvoie tous les elements d'une semaine avec les b�tises
	function liste_des_elements_avec_betise($semaine_id)
	{
		$sql = "SELECT distinct E.*
				FROM elements E, questions Q, betises B
				WHERE E.question_id = Q.id and Q.semaine_id = $semaine_id 
					AND E.id = B.element_id
				ORDER by E.ordre asc";
		$res=$this->getArray($sql);
		return $res;
	}

	function type_semaine($semaine_id)
	{
		$sql = "SELECT `deroulement`
				FROM semaines
				WHERE id = $semaine_id";
		$res=$this->getArray($sql);
		if (isset($res[0]['deroulement']))
			return $res[0]['deroulement'];
		else return false;
	}

	function liste_des_elements_visibles($semaine_id, $equipe_id=NULL)
	{
		$type_semaine = $this->type_semaine($semaine_id);

		if ($type_semaine == "normale")
			return $this->liste_des_elements_visibles_normaux($semaine_id,$equipe_id);
		elseif  ($type_semaine == "synchronisee")
			return $this->liste_des_elements_visibles_synchrones($semaine_id,$equipe_id);
		elseif  ($type_semaine == "progressive")
			return $this->liste_des_elements_visibles_progressifs($semaine_id,$equipe_id);
	}

	//renvoie tous les elements d'une semaine visibles par une equipe ou un anonyme
	function liste_des_elements_visibles_normaux($semaine_id,$equipe_id=NULL)
	{
		if (isset($equipe_id))
		{
			$sql = "SELECT E.* FROM elements E, questions Q
					LEFT JOIN propositions P ON P.question_id = Q.id AND P.equipe_id = $equipe_id
					WHERE E.question_id = Q.id
						AND Q.semaine_id = $semaine_id
						AND (E.ordre = 1 OR 
							(P.correct = 1 AND E.ordre <= P.ordre) OR
							(P.correct = 0 AND E.ordre <= P.ordre + 1))
					ORDER BY Q.ordre ASC, E.ordre ASC";
		} else {
			//renvoie les �l�ments pour un anonyme
			$sql = "SELECT E.*
					FROM elements E, questions Q 
					WHERE E.question_id =Q.id
						AND Q.semaine_id=$semaine_id
						AND E.ordre = 1 
					ORDER BY Q.id asc,E.ordre asc";
		}
		$res=$this->getArray($sql);

		return $res;
	}

	//renvoie tous les elements visibles d'une semaine synchrone
	function liste_des_elements_visibles_synchrones($semaine_id,$equipe_id=NULL)
	{
		if (isset($equipe_id))
		{
			$sql = "SELECT E.*
					FROM elements E, semaines S, questions Q
					LEFT JOIN propositions P ON P.question_id = Q.id AND P.equipe_id = $equipe_id
					WHERE E.question_id = Q.id
					AND Q.semaine_id = $semaine_id
					AND S.id = $semaine_id
					AND ((E.ordre <= S.ordre_actuel AND (P.correct IS NULL OR P.correct = 0)) OR
						(P.correct = 1 AND E.ordre <= P.ordre))
					ORDER by Q.id asc,E.ordre asc";
		} else {
			$sql = "SELECT E.*
					FROM elements E, questions Q, semaines S
					WHERE E.question_id = Q.id
					AND Q.semaine_id = $semaine_id
					AND S.id = $semaine_id
					AND E.ordre <= S.ordre_actuel
					ORDER by Q.id asc,E.ordre asc";
		}
		$res=$this->getArray($sql);

		return $res;
	}

	//renvoie tous les elements visibles d'une semaine progressive
	function liste_des_elements_visibles_progressifs($semaine_id,$equipe_id=NULL)
	{
		if (isset($equipe_id))
		{
			//renvoie les �l�ments visibles par une �quipe
			$sql = "SELECT E.* FROM elements E, semaines S, questions Q
					LEFT JOIN propositions P ON P.question_id = Q.id AND P.equipe_id = $equipe_id
                    WHERE E.question_id = Q.id
                        AND Q.semaine_id = $semaine_id
                        AND S.id = Q.semaine_id
                        AND (E.ordre = 1 OR 
                            (P.correct = 1 AND E.ordre <= P.ordre) OR
                            (P.correct = 0 AND E.ordre <= P.ordre +1))    
                        AND (Q.ordre <= S.questions_a_afficher +
                            (SELECT COUNT(*) FROM questions, points WHERE points.question_etat = 'finie' AND points.question_id = questions.id AND points.equipe_id = $equipe_id AND questions.semaine_id = $semaine_id))
					ORDER BY Q.id ASC, E.ordre ASC";
            } else {
				//renvoie les �l�ments pour un anonyme
				$sql = "SELECT  E.*
						FROM elements E, questions Q, semaines S
						WHERE E.question_id =Q.id
							AND Q.semaine_id=$semaine_id
							AND S.id = Q.semaine_id
							AND E.ordre = 1
							AND Q.ordre <= S.questions_a_afficher
						ORDER BY Q.id asc,E.ordre asc";
			}
			$res=$this->getArray($sql);

			return $res;
	}

////////////////////////////Gestion des propositions////////////////////////////

	// enregistre une proposition
	function ajouter_proposition($question_id,$ordre,$equipe_id,$correct,$correcteur_id)
	{
		$sql = "REPLACE into propositions
				SET correct = '$correct', ordre = '$ordre', question_id = '$question_id', equipe_id = '$equipe_id', horodate=now(), correcteur_id = $correcteur_id";

		$res=$this->execute($sql);

		return $res;
	}

	// annule la derniere correction d'une question pour une equipe
	function annuler_proposition($question_id,$ordre,$equipe_id,$correct,$correcteur_id)
	{
		if ($ordre==1)
			$sql = "DELETE
					FROM propositions
					WHERE question_id = $question_id
						AND equipe_id = $equipe_id";
		else
			$sql = "UPDATE propositions set ordre = $ordre-1, correct = '0', horodate=now(), correcteur_id = $correcteur_id
					WHERE question_id = $question_id
						AND equipe_id = $equipe_id";
			$res=$this->execute($sql);

			return $res;
	}

	// renvoie l'�tat des propositions d'une �quipe pour une semaine donn�e
	function liste_des_propositions($equipe_id,$semaine_id)
	{
		$sql = "SELECT P.question_id, P.ordre, P.correct
				FROM propositions P, questions Q
				WHERE Q.semaine_id = $semaine_id
					AND Q.id = P.question_id
					AND P.equipe_id = $equipe_id";
		$res=$this->getArray($sql);

		return $res;
	}

	// renvoie la date et l'auteur de la derniere correction pour une equipe
	function derniere_correction($equipe_id, $semaine_id)
	{
		$sql = "Select date_format(C.horodate, '%d/%m/%Y � %Hh%i') date_correction, C.nom_correcteur
				From (
					Select P.horodate horodate, E.nom nom_correcteur
					From propositions P, questions  Q, equipes E
					Where   P.equipe_id = $equipe_id
						And E.id = P.correcteur_id
						And Q.id = P.question_id
						And Q.semaine_id = $semaine_id
				Order By P.horodate desc) C
				Limit 1";
		$res=$this->getRow($sql);

		return $res;
	}

	// renvoie les points d'une �quipe pour chaque question d'une semaine
	function points_par_question($semaine_id,$equipe_id)
	{
		$sql = "SELECT question_id,
				IF(points>0, points,
				IF(elements_restants=0, 0, NULL)) AS points
				FROM points 
				WHERE equipe_id=$equipe_id
					AND semaine_id=$semaine_id";

		$res=$this->getArray($sql);

		return $res;
	}

	//renvoie le bareme par defaut
	function bareme()
	{
		$sql = "SELECT ordre, points
				FROM bareme
				WHERE 1";
		$res=$this->getArray($sql);

		return $res;
	}

	// renvoie le classement final avec les scores totaux
	function classement()
	{
		$sql = "SELECT
					S1.equipe_id, 
					S1.equipe_nom, 
					S1.points, 
					S1.bonus,
					@rank:=(SELECT COUNT(S2.equipe_id)
							FROM scores as S2
							WHERE S2.points>S1.points)+1 as rang 
				FROM scores as S1
				ORDER BY S1.points DESC";

		$res=$this->getArray($sql);

		return $res;
	}

	function classement_provisoire()
	{
		$sql = "SELECT
						P.equipe_id AS equipe_id,
						P.equipe_nom AS equipe_nom,
						Eq.bonus AS bonus, (sum(P.points) + Eq.bonus) AS points
				FROM (points P join equipes Eq) 
				WHERE (P.equipe_id = Eq.id) 
				GROUP BY P.equipe_id
				ORDER BY points desc, P.equipe_nom asc";

		$res=$this->getArray($sql);

		return $res;
	}

	// renvoie le d�tail des scores du classement final
	function classement_detail()
	{
		$sql = "SELECT s_s.*
				FROM scores_semaines s_s, semaines s
				WHERE s.etat='finie' AND s_s.semaine_id = s.id";
		$res=$this->getArray($sql);

		return $res;
	}

	// renvoie le classement de la semaine avec les scores totaux
	function classement_semaine($semaine_id)
	{
		$sql = "SELECT *
				FROM scores_semaines
				WHERE semaine_id=$semaine_id ORDER BY points DESC, equipe_nom ASC";
		$res=$this->getArray($sql);

		return $res;
	}

	// renvoie le d�tail des scores du classement de la semaine
	function classement_semaine_detail($semaine_id)
	{
		$sql = "SELECT *
				FROM points
				WHERE semaine_id=$semaine_id
					AND points>0";
		$res=$this->getArray($sql);

		return $res;
	}

//////////////////////// gestion des betises ///////////////////////////////////

	function ajouter_betise($element_id,$equipe_id,$value)
	{
		$sql = "REPLACE into betises
				SET element_id = '$element_id', equipe_id = '$equipe_id', betise='$value'";
		$res=$this->execute($sql);

		return $res;
	}

	function get_betises_aleatoires($nombre_betises, $equipe_id=-1)
	{
		// s�lection al�atoire pour l'instant
		// TODO : s�lection statistique
		$sql = "SELECT	B.id,
						B.equipe_id,
						B.betise,
						El.type_id,
						El.fichier,
						El.miniature,
						Eq.nom
			FROM 	betises     B,
					elements    El,
					equipes     Eq
            WHERE B.element_id = El.id
				AND B.equipe_id <> $equipe_id
				AND B.equipe_id=Eq.id
            ORDER BY rand()
            LIMIT $nombre_betises";

		$res=$this->getArray($sql);

		return $res;
	}

	function liste_des_votes($equipe_id, $semaine_id)
	{
		$sql = "SELECT V.*
                FROM	votes     V,
						betises   B,
						elements  E,
						questions Q
                WHERE V.equipe_id = $equipe_id
					AND B.id = V.betise_id
					AND E.id = B.element_id
					AND Q.id = E.question_id
					AND Q.semaine_id = $semaine_id";
		$res=$this->getArray($sql);

		return $res;
	}

	function liste_des_betises($semaine_id)
	{
		$sql = "SELECT  B.id,B.*,
						B.id  id,
						Q.id question_id,
						E.id element_id,
						EQ.nom nom_equipe
				FROM	betises B,
						equipes EQ,
						elements E,
						questions Q
				WHERE EQ.id = B.equipe_id
					AND E.id = B.element_id
					AND Q.id = E.question_id
					AND Q.semaine_id = $semaine_id";
		$res=$this->getArray($sql);

		return $res;
	}

	function voter_pour_betise($betise_id, $equipe_id)
	{
		// 1. v�rifier que l'�quipe a encore des votes disponibles
		// pour �a on commence par r�cup�rer l'identifiant de la semaine
		$sql = "SELECT Q.semaine_id
				FROM betises B,
					elements E,
					questions Q
				WHERE B.id = $betise_id
					AND E.id = B.element_id
					AND Q.id = E.question_id";
		$res=$this->getRow($sql);
		if ($res === null)
		{
			return false;
		}
		$semaine_id = $res['semaine_id'];

		$sql = "SELECT  count(V.id) vote_count
				FROM	votes       V,
						betises     B,
						elements    E,
						questions   Q
				WHERE V.equipe_id = $equipe_id
					AND B.id = V.betise_id
					AND E.id = B.element_id
					AND Q.id = E.question_id
					AND Q.semaine_id = $semaine_id";
		$res = $this->getRow($sql);
		if ($res === null)
		{
			return false;
		}
		$vote_count = $res['vote_count'];
		if ($vote_count >= FRCD_BETISIER_NB_VOTES)
		{
			$this->setErrorMsg("Vous avez d�j� �puis� vos votes pour cette semaine !");
			return false;
		}

		// 2. v�rifier que l'�quipe n'a pas d�j� vot� pour cet �l�ment
		$sql = "SELECT count(V.id) vote_count
				FROM votes V
				WHERE V.equipe_id = $equipe_id
					AND V.betise_id = $betise_id";
		$res = $this->getRow($sql);
		if ($res === null)
		{
			return false;
		}
		$vote_count = $res['vote_count'];
		if ($vote_count > 0)
		{
			//die($sql." || ".$vote_count);
			$this->setErrorMsg("Vous avez d�j� vot� pour cette b�tise !");
			return false;
		}

		// 3. c'est bon (ouf) on enregistre le vote
		$sql = "INSERT INTO votes
                SET equipe_id = $equipe_id, betise_id = $betise_id";
		$res=$this->execute($sql);

		return $res;
	}

	function classement_du_betisier()
	{
		// TODO possiblit� de le faire sans select inline si trop lent
		$sql = "SELECT E.id equipe_id, E.nom nom, (
						SELECT count(V.id)
						FROM votes V, betises B
						WHERE B.id = V.betise_id
							AND B.equipe_id = E.id) score
				FROM equipes E
				WHERE E.admin = 0
				ORDER BY score desc";
		$res = $this->getArray($sql);

		return $res;
	}

	function meilleures_betises($nombre)
	{
		$range = $nombre * 3;
		$sql = "SELECT *
				FROM (
						SELECT count(*) as votes, count(*)/nb_votes.votes as score, b.betise, e.*, eq.nom
						FROM	betises b,
								votes v,
								elements e,
								questions q,
								semaines s,
								equipes eq,
								(
									SELECT s2.id as semaine_id, count(*) as votes
									FROM betises b2, votes v2, elements e2, questions q2, semaines s2 
									WHERE v2.betise_id = b2.id
										AND b2.element_id = e2.id
										AND e2.question_id = q2.id
										AND q2.semaine_id = s2.id group by s2.id) nb_votes
						WHERE eq.id = b.equipe_id
							AND v.betise_id = b.id
							AND e.id = b.element_id
							AND e.question_id = q.id
							AND q.semaine_id = s.id
							AND nb_votes.semaine_id = s.id
				GROUP BY b.id
				ORDER BY score desc
				LIMIT 0, $range) meilleures
				ORDER BY rand()
				LIMIT 0, $nombre";
		$res = $this->getArray($sql);
		return $res;
	}

//////////////////////// gestion des annonces //////////////////////////////////

	function ajouter_annonce($user_id,$titre,$texte)
	{
		$sql = "INSERT INTO annonces
				SET user_id = '$user_id', titre = '$titre', texte='$texte', date=NOW()";
		$res=$this->execute($sql);

		return $res;
	}

	function liste_des_annonces()
	{
		$sql = "SELECT a.id, a.titre, date_format(a.date,'%d/%m/%Y � %Hh%i') as date, u.nom from annonces a, equipes u where u.id=a.user_id
				ORDER BY a.date desc";
		$res=$this->getArray($sql);

		return $res;
	}

	function nombre_annonces_non_lues($last_id)
	{
		$sql = "SELECT count(*) as nombre
				FROM annonces
				WHERE id > $last_id";
		$res=$this->getRow($sql);

		return $res;
	}

	function get_annonces()
	{
		$sql = "SELECT a.id, a.titre, a.texte, date_format(a.date,'%d/%m/%Y � %Hh%i') as date, u.nom
				FROM annonces a, equipes u
				WHERE u.id=a.user_id
				ORDER BY a.date desc";
		$res=$this->getArray($sql);

		return $res;
	}

	function get_annonce($id=null)
	{
		if ($id) $sql = "SELECT a.id, a.titre, a.texte, date_format(a.date,'%d/%m/%Y � %Hh%i') as date, u.nom
						FROM annonces a, equipes u
						WHERE u.id=a.user_id
							AND a.id=$id";
		else
			$sql = "SELECT a.id, a.titre, a.texte, date_format(a.date,'%d/%m/%Y � %Hh%i') as date, u.nom
					FROM annonces a, equipes u
					WHERE u.id=a.user_id
					ORDER BY a.date desc limit 1";    
		$res=$this->getRow($sql);

		return $res;
	}

	function supprimer_annonce($id)
	{
		$sql = "DELETE FROM annonces WHERE id=$id";
		$res=$this->execute($sql);

		return $res;
	}

	function update_annonce($id,$titre,$texte)
	{
		$sql = "UPDATE annonces
				SET titre = '$titre', texte='$texte'
				WHERE id=$id";
		$res=$this->execute($sql);

	return $res;
	}

/////////////////////// gestion des sessions////////////////////////////////////

	function nombre_de_connectes($minutes=FRCD_TEMPS_CONNEXION)
	{
		$sql = "SELECT count(id) as nombre
				FROM equipes
				WHERE admin=1
					AND TIMESTAMPDIFF(MINUTE,derniere_activite,NOW()) <= $minutes";
		$res=$this->getRow($sql);

		return $res;
	}

	function liste_equipes_connectees($minutes=FRCD_TEMPS_CONNEXION)
	{
		$sql = "SELECT *
				FROM equipes
				WHERE TIMESTAMPDIFF(MINUTE,derniere_activite,NOW()) <= $minutes
				ORDER BY nom ASC, admin DESC";
		$res=$this->getArray($sql);

		return $res;
	}

	function insert_connexion($equipe_id, $adresse_ip, $navigateur, $si_login)
	{
		$sql = "INSERT INTO connexions (`horodate`, `equipe_id`, `session_id`, `adresse_ip`, `navigateur`, `si_login`)
				VALUES (now(), $equipe_id, Null, \"$adresse_ip\", \"$navigateur\", $si_login)";
		$res=$this->execute($sql);

		return $res;
	}

	function liste_des_connexions_problematiques()
	{
		// liste des couples d'�quipes s'�tant connect�es avec la m�me IP
		$sql = "SELECT	distinct 
						E1.nom			as nom1,
						E2.nom			as nom2,
						C1.adresse_ip	as adresse_ip
				FROM connexions C1, connexions C2, equipes E1, equipes E2
				WHERE C1.equipe_id = E1.id
					AND C2.equipe_id = E2.id
					AND E1.id < E2.id
					AND C1.adresse_ip = C2.adresse_ip";
		$res=$this->getArray($sql);

		return $res;
	}

	function liste_nombre_ip_par_equipe()
	{
		$sql = "SELECT E.nom,
						count(distinct C.adresse_ip) as nombre_ip
				FROM equipes E, connexions  C
				WHERE E.id = C.equipe_id
				GROUP BY E.id
				ORDER BY nombre_ip desc";
		$res=$this->getArray($sql);

		return $res;
	}

	function liste_nombre_connexions_par_equipe()
	{
		$sql =" SELECT E.nom,
						count(*) as nombre_conn,
						date_format(max(horodate), '%d/%m/%Y � %Hh%i') derniere_conn
				FROM equipes E, connexions C
				WHERE E.id = C.equipe_id
				GROUP BY E.id
				ORDER BY nombre_conn desc";
		$res=$this->getArray($sql);

		return $res;
	}

	function liste_des_connexions_par_ip($adresse_ip)
	{
		$sql =" SELECT E.nom,
                          date_format(C.horodate,'%d/%m/%Y � %Hh%i') horodate,
                          C.* from equipes E,
                          connexions C
				WHERE C.equipe_id = E.id
					AND C.adresse_ip = \"$adresse_ip\"
				ORDER BY C.horodate asc
				LIMIT 0, 200";
		$res=$this->getArray($sql);

		return $res;
	}

//////////////////// Gestion des �ditions de propositions///////////////////////

	function infos_verrou($semaine_id,$equipe_id)
	{
		$sql = "SELECT Eq.id, Eq.nom, Ed.date
				FROM editions Ed, equipes Eq
				WHERE Ed.semaine_id=$semaine_id
					AND Ed.equipe_id=$equipe_id
					AND Ed.active=1
					AND Eq.id=Ed.organisateur_id";
		$res=$this->getRow($sql);

		return $res;
	}

	function nouvelle_edition($organisateur_id,$semaine_id,$equipe_id)
	{
		$sql = "INSERT into editions (`organisateur_id`,`semaine_id`,`equipe_id`,`date`,`active`)
				VALUES ($organisateur_id,$semaine_id,$equipe_id,NOW(),1)";
		$res=$this->execute($sql);

		return $res;
	}

	function deverrouille_edition($organisateur_id,$semaine_id,$equipe_id)
	{
		$sql = "UPDATE editions set active=0
				WHERE organisateur_id=$organisateur_id
					AND semaine_id=$semaine_id
					AND equipe_id=$equipe_id";
		$res=$this->execute($sql);

		return $res;
	}

	function nettoyer_verrous($minutes=FRCD_TEMPS_CORRECTION)
	{
		$sql = "DELETE
				FROM editions
				WHERE active=1
					AND TIMESTAMPDIFF(minute,date,NOW()) >= $minutes";
		$res=$this->execute($sql);

		return $res;
	}

//////////////////////////////////Divers////////////////////////////////////////

	function generate_password($length)
	{
		$use_mix='Yes';
		$use_num='Yes';
		$use_let='Yes';

		$allowable_characters = "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjklmnpqrstuvwxyz0123456789";

		if (($use_mix == "Yes") && ($use_num == "Yes") && ($use_let == "Yes"))
		{
			$ps_st = 0;
			$ps_len = strlen($allowable_characters);
		}

		if (($use_mix == "No") && ($use_num == "Yes") && ($use_let == "Yes"))
		{
			$ps_st = 24;
			$ps_len = strlen($allowable_characters);
		}

		if (($use_mix == "Yes") && ($use_num == "No") && ($use_let == "Yes"))
		{
			$ps_st = 0;
			$ps_len = 47;
		}

		if (($use_mix == "Yes") && ($use_num == "Yes") && ($use_let == "No"))
		{
			$ps_st = 48;
			$ps_len = strlen($allowable_characters);
		}

		if (($use_mix == "No") && ($use_num == "No") && ($use_let == "Yes"))
		{
			$ps_st = 24;
			$ps_len = 47;
		}

		if (($use_mix == "No") && ($use_num == "Yes") && ($use_let == "No"))
		{
			$ps_st = 48;
			$ps_len = strlen($allowable_characters);
		}

		if (($use_mix == "Yes") && ($use_num == "No") && ($use_let == "No"))
		{
			$ps_st = 0;
			$ps_len = 1;
		}

		if (($use_mix == "No") && ($use_num == "No") && ($use_let == "No"))
		{
			$ps_st = 0;
			$ps_len = 1;
		}

		mt_srand((double)microtime()*1000000);

		$pass = "";

		for($i = 0; $i < $length; $i++)
		{
			$pass .= $allowable_characters[mt_rand($ps_st, $ps_len - 1)];
		}

		return $pass;
	}

	function icone_type($type)
	{
		$sql = "SELECT *
				FROM types
				WHERE nom='$type'";
		$res=$this->getRow($sql);

		return $res;
	}

	function creer_miniature($src, $dest, $largeur)
	{
		ini_set("memory_limit","64M");

		$im_src=imagecreatefromstring(file_get_contents($src));
		if (!$im_src)
		{
			$this->setErrorMsg("Impossible d'ex�cuter la fonction imagecreatefromjpeg()");
			return false;
		}
		$src_x=imagesx($im_src);
		$src_y=imagesy($im_src);
		$verticale="0";
		$horizontale="0";
		if ($im_src=="") return; 
			//on determine un carr� avec la plus petite largeur
			if($src_x >= $src_y){$dim=$src_y; $verticale="1";}
			elseif($src_x <= $src_y){$dim=$src_x; $horizontale="1";}
			else{$dim=$src_x;}

		//on determine le point de depart x,y
		if($verticale == "1")
		{
			$point_x_ref=($src_x/2)-($dim/2);
			$point_y_ref="0";
		}
		if($horizontale == "1")
		{
			$point_x_ref="0";
			$point_y_ref=($src_y/2)-($dim/2);
		}
		//$im_dest=@imagecreate($tailleX, $tailleY);
		$im_dest=imagecreatetruecolor($largeur, $largeur);

		imagecopyresampled($im_dest, $im_src, 0, 0, $point_x_ref, $point_y_ref, $largeur, $largeur, $dim, $dim);
		imagedestroy($im_src); 
		if (!imagejpeg($im_dest, $dest, 90))
		{
			$this->setErrorMsg("Le fichier de la miniature n'a pu �tre cr��. V�rifier que le r�pertoire de destination existe.");
			return false;
		}
		imagedestroy($im_dest);
		return true;
	}

	function upload ($fichier_nom,$fichier_data)
	{
		$nouveau_fichier="";
		if ($fichier_nom)
		{
			$extension = end(explode(".", $fichier_nom));
			do
				$nouveau_fichier = $this->generate_password(FRCD_FICHIERS_CODAGE).".$extension";
			while (file_exists (FRCD_FICHIERS_CHEMIN."$nouveau_fichier"));
			// Copie du fichier sous son nouveau nom dans fichiers

			if (is_writable(FRCD_FICHIERS_CHEMIN))
			{
				if (!move_uploaded_file($fichier_data, FRCD_FICHIERS_CHEMIN."$nouveau_fichier") )
				{
					$this->setErrorMsg("Un probl&egrave;me s'est produit pendant la copie du fichier");
					return false;
				}
			} else {
				$this->setErrorMsg("Le repertoire de destination n'est pas accessible en ecriture.");
				return false;
			}
		}
		return $nouveau_fichier;
	}

	function generer_miniature ($fichier)
	{
		$miniature="";
		if ($fichier)
		{
			$extension = strtolower(end(explode(".", $fichier)));
			$types = array ( 
				"bmp"	=> "image",
				"gif"	=> "image",
				"png"	=> "image",
				"jpg"	=> "image",
				"jpeg"	=> "image",
				"wav"	=> "son",
				"mp3"	=> "son",
				"mp4"	=> "video",
				"mpg"	=> "video",
				"mpeg"	=> "video",
				"avi"	=> "video",
				"txt"	=> "texte"
				);

			if (isset($types [ $extension ]))
			{
				$nom_type= $types [ $extension ];
				$type=$this->icone_type($nom_type);
				$miniature=$type['icone'];

				// Fabrication de la miniature
				if ("$miniature" == "")
				{
					$miniature = "small_$fichier";
					if ($this->creer_miniature(FRCD_FICHIERS_CHEMIN."$fichier", FRCD_FICHIERS_CHEMIN."$miniature",FRCD_MINIATURES_LARGEUR));
					else $this->setErrorMsg($this->dao->errorMsg());
				}
			} else  $this->setErrorMsg("Extension de fichier inconnue.");
		}

		return $miniature;
	}

	function destruct()
	{
		FR_Object_DB::destruct();
	}

}
