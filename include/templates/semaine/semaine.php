<div id="entete">
    <?php if ($semaine['image'])
	echo "<img src=\"".FRCD_FICHIERS_CHEMIN.$semaine['image']."\" alt=\"Semaine".$semaine['numero']."\"/>";
	else echo "Semaine ".$semaine['numero']?>
</div>

<div id="semaine">

  <div id="theme">
      <?php if ($semaine['theme']) :?>
      Th&egrave;me : <?php echo $semaine['theme']?>
      <?php endif ?>
  </div>

  <div id="description"><?php echo $semaine['description']?></div>

  <div id="deroulement">Il s'agit d'une semaine <?php echo $semaine['deroulement']?></div>

  <div id="etat">
      <?php if ($semaine['etat']=='finie'):?>
        La semaine est termin&eacute;e<br/>
        <br/>
        <br/>
        <a href="index.php?module=classement&amp;action=semaine&amp;semaine_id=<?php echo $semaine['id'];?>"><img src="images/frcd_bouton_classement.jpg" title="Voir les classement et scores d&eacute;taill&eacute;s de la semaine" alt="score"/></a>
        <a href="index.php?module=betisier&amp;semaine_id=<?php echo $semaine['id'];?>"><img src="images/frcd_bouton_betisier.jpg" title="Voir le b&ecirc;tisier de la semaine" alt="b&ecirc;tisier"/></a>
      <?php elseif ($semaine['etat']=='en_cours'):?>
        Fin de la semaine le <?php echo $semaine['fin'];?><br/><br/>
        <a href="mailto:<?php echo FRCD_EMAIL;?>"><img src="images/frcd_bouton_reponses.jpg" title="Pour valider vos r&eacute;ponses ou demander des indices, cliquez ici" alt="email"/></a>
      <?php elseif ($semaine['etat']=='non_commencee'):?>
        D&eacute;but de la manche le <?php echo $semaine['debut'];?>
      <?php endif;?>
  </div>
</div>

<div id="questions">
<table>
<tr><td>
    <!-- bloc d'affichage de toutes les questions -->
    <?php foreach ($questions as $question_id => $question) :?>
    <div class="clear">
    <?php if (isset($question['elements']) && count($question['elements'])) :?>    
        <!-- bloc d'affichage d'une question -->
        <div class="question">
                <!-- numero de la question -->
                <div class="question_entete">
                  <div class="ordre"><?php echo $question['ordre'];?></div>
                  <div class="libelle"><?php echo $question['libelle'];?></div>
                </div>

                <!-- indices de la question -->
                <?php  if (isset($question['elements'])) foreach ($question['elements'] as $ordre => $element):?>

                  <div class="miniature"><a href="<?php echo $element['fichier'];?>"><img src="<?php echo $element['miniature'];?>" title="Cet &eacute;l&eacute;ment rapporte <?php echo $element['points']?>pt." alt="<?php echo $element['points']?> pt."/></a>

                    <?php  if ($element['indication']!=""):?>

                      <div class="indication"><?php echo str_repeat("*", $ordre);?></div>

                    <?php endif;?></div>

                <?php endforeach;?>
      
                <?php  if (isset($questions[$question_id]['points'])):?>

                    <div class="score">
                      <img src="images/frcd_<?php echo $questions[$question_id]['points'];?>pt.jpg" alt="<?php echo $questions[$question_id]['points'];?> points"/>
                    </div>

                <?php endif;?>
        </div>
        <!-- fin du bloc d'affichage d'une question -->
        <div class="clear"></div>    
        <!-- bloc d'affichage des indications -->
        <?php if (isset($question['elements']) && count($question['elements'])):?>
            <table class="indications" cellpadding="0" cellspacing="0" >
            <tr><td>
            <?php  foreach ($question['elements'] as $ordre => $element):?>
              <?php  if ($element['indication']!=""):?>

                <b><?php echo str_repeat("*", $ordre);?></b> <?php echo $element['indication'];?><br/>

              <?php endif;?>
            <?php endforeach;?>
            </td></tr>
            </table>
        <?php endif;?>
        <!-- fin du bloc d'affichage des indications -->

        <!-- bloc d'affichage de l'annulation -->
        <?php if ($question['annulee']):?>
            <table class="indications" cellpadding="0" cellspacing="0" >
            <tr><td>
               <h3>Question annul&eacute;e ! <h3>
            </td></tr>
            </table>
        <?php endif;?>
        <!-- fin du bloc d'affichage des indications -->


        <!-- bloc d'affichage des reponses -->
        <?php if (isset($questions[$question_id]['reponse']) && ($questions[$question_id]['reponse'])):?>
        <div class="bloc_reponse">
              <div class="reponse">
                <?php echo $questions[$question_id]['reponse'];?>
              <?php if ($questions[$question_id]['titre_original']):?>
                (<?php echo $questions[$question_id]['titre_original'];?>)
              <?php endif;?>
              <?php if ($questions[$question_id]['lien_imdb']):?>
                <a href="<?php echo $questions[$question_id]['lien_imdb'];?>" target="_blank"><img src="images/imdb.gif" style="vertical-align:text-top" alt="imdb"/></a>
              <?php endif;?>
              </div>
              <div class="infos">
              <?php if ($questions[$question_id]['realisateur']):?>
                de <?php echo $questions[$question_id]['realisateur'];?>
              <?php endif;?>
              <?php if ($questions[$question_id]['annee']):?>
                (<?php echo $questions[$question_id]['annee'];?>)
              <?php endif;?>
              </div>
              <div class="commentaire"><p><?php echo nl2br($questions[$question_id]['commentaire']);?></p></div>
              <div class="auteur"><?php echo $questions[$question_id]['nom'];?></div>
        </div>
        <?php endif;?>
        <!-- fin du bloc d'affichage des reponses -->

    <?php endif ?>
</div>
    <?php endforeach;?>
</td></tr></table>
</div>
