<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">

<head>
<meta http-equiv="Content-Type" content="text/html;"/>
<link rel="SHORTCUT ICON" href="favicon.ico"/>
<link href="frcd.css" rel="stylesheet" type="text/css"/>
  <title><?php echo FRCD_TITRE?></title>
<meta http-equiv="Content-type" content="text/html;charset=iso-8859-1"/>

</head>

<body>
<div style="width:100%;background-image:url(images/frcd_fond.png);background-position:top center; background-repeat:repeat-y;">
	<table width="650" cellspacing="0" cellpadding="0" border="0" style="margin:auto">
		<tr>
			<td rowspan="2" valign="bottom"><a href="."><img src="images/frcd_titre_gauche.jpg" alt="titre gauche"/></a></td>
			<td style="background-color:black"><a href="."><img src="images/frcd_titre_droit.jpg" alt="titre droit"/></a></td>
		</tr>
		<tr>
			<td style="background-color:black;color:white;height:24px">
				<div id="heure"><?php echo date("d/m/y H:i")?></div>
				<div id="connectes">
					<?php  $nombre=$connectes['nombre']; ?>
					<?php if ($nombre > 0):?>
					<?php echo $nombre;?>  
					<?php else :?>
					Aucun
					<?php endif;?> 
					organisateur<?php if ($nombre > 1):?>s<?php endif;?> en ligne
				</div>
			</td>
		</tr>
		<tr>
			<td rowspan="2" style="background-color:#7f0301;background-image:url(images/frcd_menu_fond.jpg);background-repeat:no-repeat;background-align:top" valign="top"><div id="menu"><?php include($tplPath."/menu.php");?></div></td>
			<td valign="top" class="contenu">
				<div>
					<?php if (is_array($errors) && count($errors)) :?>
					<div class="erreurs">
						<?php foreach ($errors as $error):?>
						<?php echo $error;?><br>
						<?php endforeach; ?>
					</div>
					<?php else :?>	 
					<?php include($modulePath.'/'.$tplFile);?>
					<?php endif;?>
				</div>
			</td>
		</tr>
		<tr>
			<td class="contenu">
				<div style="text-align:right;margin:10px 20px 0px 10px;">&agrave; Kronos...</div></td>
		</tr>
	</table>
</div>
<div id="pied-de-page"></div>
</body>
</html>
