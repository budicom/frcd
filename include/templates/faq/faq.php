<div id="entete">FAQ</div>
<div class="faq">
<ul>
  <li> <a href="#q1">C'est quoi le FRCD ?</a></li>
  <li> <a href="#q2">Pourquoi ce nom barbare ?</a></li>
  <li> <a href="#q3">A quoi &ccedil;a sert ?</a></li>

  <li> <a href="#q4">Quand &ccedil;a commence ?</a></li>
  <li> <a href="#q5">Qu'est ce qu'on gagne?</a></li>
  <li> <a href="#q6">Et si je veux jouer, comment je fais ?</a></li>
  <li> <a href="#q7">Quelles sont les r&eacute;gles ?</a></li>

  <li> <a href="#q7bis">Y&#8217;a t-il des choses qu&#8217;il faut absolument &eacute;viter 
    de faire ?</a></li>
	<li> <a href="#q8bis">Et si j&#8217;ai l&#8217;impression que les organisateurs (qui sont beaux, 
    forts et intelligents et quoiqu&#8217;il en soit, mieux en tout que moi) se 
    sont tromp&eacute;s ?</a></li>
  <li> <a href="#q8">C&#8217;est quoi, le b&ecirc;tisier ?</a></li>

  <li> <a href="#q9">T'as des conseils pour un d&eacute;butant comme moi ?</a></li>
  <li> <a href="#q10">J'ai besoin de quoi ?</a></li>
  <li> <a href="#q11">Et pourquoi c'est vous qui organisez ?</a></li>
  <li> <a href="#q12">Mais au fait, qui est-ce qui a eu cette id&eacute;e ?</a></li>

  <li> <a href="#q14">Dis donc, votre site, il est vachement chouette&#8230; Comment &ccedil;a se fait ?</a></li>
  <li> <a href="#q15">D&#8217;o&ugrave; &ccedil;a sort, ces th&eacute;matiques ?</a></li>
</ul>

<div class="separation"></div>
<ul>
  <li>

  <a name="q1">
 <span class="faq_questions">C'est quoi le FRCD ?</span></a>
  <p> Option 1 (pour profane) : c&#8217;est un jeu de connaissance autour du cin&eacute;ma, organis&eacute; en sessions (celle-ci est la meilleure, et accessoirement la 26&egrave;me), lesquelles se d&eacute;composent en semaines de dur&eacute;es variables, g&eacute;n&eacute;ralement une semaine (&ccedil;a va, vous suivez toujours ?), et dont le but est de trouver ce qu&#8217;on cherche (en g&eacute;n&eacute;ral, un film &agrave; partir d&#8217;une image ou d&#8217;un indice sonore). Attention, ce jeu a un fort potentiel d&#8217;addiction.<br/><br/>
  Option 2 (pour professionnel) : c&#8217;est un cauchemar de recherche sur la toile, organis&eacute; en s&eacute;ances de tortures, lesquelles ont des dur&eacute;es variables mais toujours trop, dont le but est d&#8217;occuper les nuits des participants. Attention&#8230; en fait non, d&eacute;sol&eacute;, pour toi, c&#8217;est d&eacute;j&agrave; trop tard.
  </p></li>
</ul>
  <div class="separation"></div>
<ul>
  <li>
  <a name="q2">
 <span class="faq_questions">Pourquoi ce nom barbare ?</span></a>
  <p> Parce que le jeu est n&eacute;, invent&eacute; par Kronos, sur un forum de discussion (fr.rec.cin&eacute;ma.discussion) dont les initiales forment cet acronyme barbare. </p>
</li> 
</ul>
<div class="separation"></div>
<ul>
  <li>
  <a name="q3">
 <span class="faq_questions">A quoi &ccedil;a sert ?</span></a>

  <p> Et les oiseaux ? Et les &eacute;toiles ? Et l&#8217;amour ? Je te pr&eacute;viens, si tu commences &agrave; poser des questions &agrave; la con, je vais pas la finir, ta FAQ&#8230;
  </p></li>
</ul>

  <div class="separation"></div>
<ul>
  <li>
  <a name="q4">
 <span class="faq_questions">Quand &ccedil;a commence ?</span></a>
  <p> Parce que nous savons d&#8217;o&ugrave; nous venons et qu&#8217;il fallait marquer le coup, 
    le jeu d&eacute;butera le 7 Ao&ucirc;t 2007, dix ans jour pour jour apr&egrave;s le d&eacute;but de la 
    premi&egrave;re session organis&eacute;e par Kronos. Une mani&egrave;re de lui rendre hommage, 
    ainsi qu&#8217;&agrave; tous ceux qui ont fait la l&eacute;gende de ce jeu. </p>

</li>
</ul>
 <div class="separation"></div>
<ul>
<li> <a name="q5"> 
  <span class="faq_questions">Qu'est ce qu'on gagne ?</span>
  </a> 
  <p> La fiert&eacute; (regardez nous !), le prestige (regardez nous !!), la gloire 
    &eacute;ternelle (regardez nous !!!) et l&#8217;honneur d&#8217;organiser 
    la session suivante, pour pouvoir tortu&#8230; pardon, faire d&eacute;couvrir 
    les films que l&#8217;on aime aux autres.</p>

</li>
</ul>
 <div class="separation"></div>
<ul>
<li> <a name="q6"> 
  <span class="faq_questions">Et si je veux jouer, comment je fais ?</span>
  </a> 
  <p> D&#8217;abord, tu apprends les r&egrave;gles (voir question suivante), parce 
    que si t&#8217;essayes de tricher, on te d&eacute;gage. Ensuite, tu trouves 
    un nom amusant/repr&eacute;sentatif/&agrave; la con, et enfin tu t&#8217;inscris 
    aupr&egrave;s des gentils organisateurs &agrave; cette adresse : glanches26efrcd@googlegroups.com

<br/>
    S&#8217;ils t&#8217;acceptent, ils t&#8217;enverront en &eacute;change de 
    la composition int&eacute;grale de ton &eacute;quipe (ainsi que les adresses 
    e-mail de chaque membre) un code d&#8217;acc&egrave;s personnel, qui te permettra 
    de g&eacute;rer tes indices comme tu le souhaites, en toute confidentialit&eacute;. 
  </p>
</li>
</ul>
 <div class="separation"></div>
<ul> 
<li> <a name="q7"> 
  <span class="faq_questions">Quelles sont les r&eacute;gles ?</span>
  </a> 
  <p> Le jeu se compose de plusieurs semaines (qui font pas forc&eacute;ment une 
    semaine, mais tu commences &agrave; comprendre le truc), selon un calendrier 
    bien d&eacute;fini, que voici :</p><br/>

    <ul>
  <li>7 aout au 4 septembre : Semaine 1 </li>
  <li>4 septembre au 11 septembre : Semaine 2</li>
  <li>11 septembre au 18 septembre : Semaine 3</li>
  <li>18 septembre au 25 septembre : Semaine 4</li>
  <li>25 septembre au 2 octobre : Semaine 5</li>

  <li>2 octobre au 9 octobre : Semaine 6</li>
  <li>9 octobre au 16 octobre : Semaine 7</li>
  <li>16 octobre au 23 octobre : Semaine 8</li>
  <li>23 octobre au 6 novembre (semaine double, because vacances de la Toussaint) 
    : Semaine 9</li>
  <li>6 novembre au 13 novembre : Semaine 10</li>
  <li>13 novembre au 27 novembre (semaine double pour finir) : Semaine 11</li>

</ul>
<p>
<br/>

    De toute fa&ccedil;on, quand une semaine commence, il est clairement indiqu&eacute; 
    quand elle se termine. <br/>
    Toujours est-il qu&#8217;une semaine commence toujours un mardi, et se finit 
    toujours un mardi.</p>
  <p><strong>Envoi des r&eacute;ponses</strong><br/>
    L&#8217;envoi des r&eacute;ponses se fait par mail, &agrave; cette adresse 
    (et &agrave; cette adresse seulement) :glanches26efrcd@googlegroups.com, en 
    pr&eacute;cisant dans le titre du message le nom de ton &eacute;quipe. N'oublie 
    pas de mettre en copie tous les membres de votre &eacute;quipe, les organisateurs 
    r&eacute;pondent en utilisant la fonction &quot;R&eacute;pondre &agrave; tous&quot; 
    de la messagerie, si tu les oublies, ils n'auront pas copie du mail.<br/>

    Pour soumettre des propositions, tu donnes le num&eacute;ro de la question, 
    puis la r&eacute;ponse, en pr&eacute;cisant (s&#8217;il s&#8217;agit d&#8217;un 
    film) le nom du r&eacute;alisateur et l'ann&eacute;e de r&eacute;alisation 
    du film, selon un mod&egrave;le ressemblant &agrave; &ccedil;a :<br/>
    Q1 : Mais o&ugrave; est donc pass&eacute;e la septi&egrave;me compagnie? ( 
    Robert Lamoureux - 1973 )<br/>

    Q2 : Det Sjunde inseglet (Le septi&egrave;me sceau) ( Ingmar Bergman - 1957 
    )<br/>
    &#8230;<br/>
    Bien s&ucirc;r, il n'est pas obligatoire de fournir toutes les r&eacute;ponses 
    en m&ecirc;me temps. Il n&#8217;est m&ecirc;me pas obligatoire de donner des 
    r&eacute;ponses. Il n'est m&ecirc;me pas obligatoire de jouer.<br/>

    Aucune limite n'est fix&eacute;e sur le nombre d'envois par jour. Cependant, 
    on tiendra compte de la capacit&eacute; de r&eacute;ponse des organisateurs 
    (notamment autour de l'heure de l'ap&eacute;ro, car c'est sacr&eacute;, voire 
    de celle de la cl&ocirc;ture d'une semaine, car les messages affluent), de 
    leurs &eacute;ventuelles absences (certains bossent et/ou ont une vie de famille), 
    et l'on ne demandera pas l'impossible (une r&eacute;ponse en moins de 10 minutes 
    si on envoie des propositions &agrave; 2h du matin, par exemple).</p>
  <blockquote> 
    <p><strong>Points et fonctionnement</strong><br/>

      Pour chaque question, 3 r&eacute;ponses au total sont accept&eacute;es. 
      <br/>
      A chaque r&eacute;ponse erron&eacute;e, un indice est mis &agrave; disposition 
      sur ta page du jeu. Donc pas de r&eacute;ponse impulsive (genre, &ccedil;a 
      c&#8217;est Fist of Legend, j&#8217;ai reconnu&#8230;) parce qu&#8217;une 
      fois le message envoy&eacute; aux organisateurs, y&#8217;a plus de marche 
      arri&egrave;re.<br/>

      En cas d'absence de proposition pour une question, on consid&egrave;re que 
      tu continues de chercher. Si tu veux un indice sans faire de proposition, 
      il faut le demander *explicitement* par mail.<br/>
      Si tu constates qu'un indice a &eacute;t&eacute; oubli&eacute;, il est toujours 
      possible de contacter les organisateurs par mail, l'erreur sera r&eacute;par&eacute;e 
      au plus vite. </p>
    <p><strong>Attribution des points :</strong><br/>
      Les points sont attribu&eacute;s pour chaque question en fonction du nombre 
      d'indices dont l'&eacute;quipe aura eu besoin pour trouver la bonne r&eacute;ponse. 
      <br/>

      Bonne r&eacute;ponse du 1er coup (sans indice) : 3 points<br/>
      R&eacute;ponse trouv&eacute;e gr&acirc;ce au 1er indice : 2 points<br/>
      R&eacute;ponse trouv&eacute;e gr&acirc;ce au dernier indice : 1 point<br/>
      Et si tu trouves pas, z&eacute;ro.</p>

  </blockquote>
</li>
</ul>
 <div class="separation"></div>
<ul>
<li> <a name="q7bis"> 
  <span class="faq_questions">Y&#8217;a t-il des choses qu&#8217;il faut absolument &eacute;viter de faire 
    ?</span>
  </a> 
  <p> Il est formellement interdit de communiquer le code d'acc&egrave;s fourni 
    au moment de l&#8217;inscription &agrave; un membre non reconnnu de votre 
    &eacute;quipe (a fortiori &agrave; celui d'une autre &eacute;quipe). Il est 
    &eacute;galement interdit de poster des r&eacute;ponses ou de donner des indices 
    sur le fr.rec.cinema.discussion, ou sur tout autre forum internet, ceci sous 
    peine d'exclusion du jeu. <br/>

    Il est pr&eacute;f&eacute;rable d'&eacute;viter les fusions et/ou scissions 
    entre &eacute;quipes une fois le jeu commenc&eacute;. C'est absolument ing&eacute;rable 
    dans le calcul des points. Dans le doute, on mettra z&eacute;ro &agrave; tout 
    le monde.</p>
</li>
</ul>
 <div class="separation"></div>
<ul> 
<li> <a name="q8bis"> 
  <span class="faq_questions">Et si j&#8217;ai l&#8217;impression que les organisateurs (qui sont beaux, 
    forts et intelligents et quoiqu&#8217;il en soit, mieux en tout que moi) se 
    sont tromp&eacute;s ?</span>
  </a> 
  <p> Soit tu r&eacute;alises que c&#8217;est impossible, et tout va pour le mieux, 
    soit vraiment tu persistes, et dans ce cas, un mail courtois, joliment tourn&eacute;, 
    plein de r&eacute;v&eacute;rence et de flagornerie, pourra &eacute;ventuellement 
    &ecirc;tre pris en compte.</p>
</li>
</ul>
  <div class="separation"> </div>
<ul>
<li> <a name="q8"> 
  <span class="faq_questions">C&#8217;est quoi, le b&ecirc;tisier ?</span>
  </a>
<p> Il s'agit d'un panach&eacute; de propositions insolites, d&eacute;cal&eacute;es 
    ou ridicules qui auront eu comme m&eacute;rite de nous affoler les zygomatiques.<br/>
    D&#8217;une part, &ccedil;a d&eacute;tend les organisateurs stress&eacute;s 
    par les hyst&eacute;riques, les r&acirc;leurs et les proc&eacute;duriers. 
    D&#8217;autre part, &ccedil;a offre l&#8217;opportunit&eacute; aux &eacute;quipes 
    de d&eacute;montrer leur finesse d&#8217;esprit et leur humour redoutable 
    (y&#8217;en aurait m&ecirc;me qui n&#8217;auraient que &ccedil;a).</p>

</li>
</ul><div class="separation"></div>
<ul> 
<li> <a name="q9"> 
  <span class="faq_questions">T'as des conseils pour un d&eacute;butant comme moi ?</span>
  </a> 
  <p>D&#8217;une, tu &eacute;vites de me tutoyer, on n&#8217;a pas &eacute;chang&eacute; 
    nos slips quand on &eacute;tait gamins.<br/>

    De deux, tu dois &ecirc;tre pr&ecirc;t &agrave; entrer dans une fracture spatio-temporelle 
    o&ugrave; les relations humaines et les activit&eacute;s autres (boulot, loisirs, 
    promenade du chien ou moments privil&eacute;gi&eacute;s en famille) sont exclues. 
    Apr&egrave;s, il faut &ecirc;tre patient, et utiliser les ressources de l&#8217;Internet, 
    au premier rang desquelles des sites comme l&#8217;Imdb (utilisation de mots 
    cl&eacute;s, moteur de recherche interne, casting, lieux de tournages etc&#8230;), 
    Monsieur Cin&eacute;ma ou ses fiches, ou &eacute;videmment l&#8217;incontournable 
    Google.<br/>

    De trois, parce que tu verras tr&egrave;s vite que &ccedil;a aura son importance, 
    ce serait bien de jeter un coup d&#8217;&#339;il aux archives du jeu, qui 
    recensent l&#8217;int&eacute;gralit&eacute; des films d&eacute;j&agrave; propos&eacute;s.<br/>
    De quatre, et parce que le jeu se veut quand m&ecirc;me aussi au d&eacute;part 
    un test sur l&#8217;ampleur de tes connaissances cin&eacute;matographiques, 
    une bonne m&eacute;moire et une grosse ressource de films &agrave; domicile 
    (tous membres de ton &eacute;quipe confondus) sont indispensables, histoire 
    de v&eacute;rifier tes intuitions. Mais attention, car les v&eacute;rifications 
    &agrave; vitesse rapide sont trompeuses (y&#8217;a des plans tr&egrave;s courts 
    hin hin hin) et tu auras parfois l&#8217;impression qu&#8217;une piste n&#8217;est 
    pas valable sous pr&eacute;texte que tu as vu le film la semaine derni&egrave;re, 
    alors qu&#8217;en fait, c&#8217;est &ccedil;a (entre nous, on appelle cela 
    le Walk the line effect). </p>

</li>
</ul>
 <div class="separation"></div>
<ul>
<li> <a name="q10"> 
  <span class="faq_questions">J'ai besoin de quoi ?</span>
  </a>
  <p> Si tu ne les as pas (ceux-l&agrave; ou d&#8217;autres hein, pas de pros&eacute;lytisme), 
    on peut te proposer quelques utilitaires t&eacute;l&eacute;chargeables : par 
    exemple, Winamp, iTunes, Musicmatch Jukebox ou Realplayer pour les utilisateurs 
    de Windows ou iTunes pour les utilisateurs de MacOS (ceux sous Linux, eh ben, 
    d&eacute;brouillez-vous, vous l&#8217;avez choisi)<br/>

    A NOTER : nous faisons de notre mieux pour vous proposer des images qui respectent 
    le format d'origine du film (lorsqu'elles sont extraites par nos soins depuis 
    des DVD). Cependant, pour ce qui est des autres sources, nous ne pouvons garantir 
    de respecter ce crit&egrave;re. Si, malgr&eacute; les efforts des organisateurs, 
    vous trouvez les photos propos&eacute;es trop petites, trop grandes, trop 
    sombres ou trop claires, vous pouvez utiliser Irfan-view, logiciel gratuit 
    et tr&egrave;s simple &agrave; l'usage. </p>
</li>
</ul>
<div class="separation"></div>
<ul>
<li> <a name="q11"> 
  <span class="faq_questions">Et pourquoi c'est vous qui organisez ?</span>

  </a>
  <p> Toi, t&#8217;as pas suivi&#8230; <br/>
    Parce qu&#8217;on a &eacute;cras&eacute; tout le monde sur la session pr&eacute;c&eacute;dente. 
    Et si tu gagnes, tu organiseras la suivante. Ainsi va le cycle du grand jeu&#8230;</p>

</li>
</ul>
<div class="separation"></div>
<ul>
<li> <a name="q12"> 
  <span class="faq_questions">Mais au fait qui est-ce qui a eu cette id&eacute;e ?</span>
  </a> 
  <p> C'est une cr&eacute;ation de Kronos, un des premiers contributeurs r&eacute;guliers 
    de FRCD. Cin&eacute;phile &agrave; la m&eacute;moire &eacute;l&eacute;phantesque, 
    il adorait Roger Corman et Steve Martin (entre autres). Au d&eacute;part, 
    il avait cr&eacute;&eacute; le jeu pour pousser les gens &agrave; utiliser 
    l'Internet Movie Database et puis &ccedil;a a pris un peu d'ampleur. Aujourd'hui 
    il est all&eacute; rejoindre Lon Chaney et Ed Wood sur de nouveaux rivages 
    mais il avait l'air de vouloir que le jeu continue sans lui. On pense encore 
    &agrave; lui. </p>

</li>
</ul>
<div class="separation"></div>
<ul>
<li> <a name="q14"> 
  <span class="faq_questions">Dis donc, votre site il est vachement chouette, comment &ccedil;a se fait ?</span>
  </a> 
  <p> C&#8217;est parce que sous nos allures de kakous, en fait, on est des fortiches 
    en tout (chacun ayant sa sp&eacute;cialit&eacute;, mais nous t&#8217;invitions 
    &agrave; aller nous d&eacute;couvrir dans la section ad&eacute;quate) et qu&#8217;en 
    plus, on a bien profit&eacute; du boulot des organisateurs pr&eacute;c&eacute;dents, 
    qui ont eu le talent et la gentillesse de faire des trucs qui tournent bien 
    et de nous les refiler. D&#8217;ailleurs, si tu gagnes, on te laissera tout 
    reprendre &agrave; z&eacute;ro.</p>

</li>
</ul>
<div class="separation"></div>
<ul>
 
<li> <a name="q15"> 
  <span class="faq_questions">D'o&ugrave; &ccedil;a sort, ces th&eacute;matiques ?</span>
  </a> 
  <p> Histoire d&#8217;enjoliver un peu les semaines, qui pouvaient parfois avoir 
    l&#8217;air d&eacute;cousues de toutes pi&egrave;ces (oui oui) mais aussi 
    de donner des orientations de recherche, on trouvait &ccedil;a plus sympa 
    de proposer des questions ayant un point commun (on vous laissera les d&eacute;couvrir 
    au fur et &agrave; mesure). En plus, il s&#8217;agit d&#8217;un retour d&eacute;lib&eacute;r&eacute; 
    &agrave; une &eacute;poque pas si lointaine du jeu, parce que, quelque part, 
    nous sommes des vieux cons qui pensons que c&#8217;&eacute;tait mieux avant.<br/>

    Il faut toutefois souligner que ces th&eacute;matiques, parfois capillotract&eacute;es, 
    ne concernent en g&eacute;n&eacute;ral (&agrave; une exception pr&egrave;s) 
    que la premi&egrave;re image (celle de la question), ne seront pas forc&eacute;ment 
    illustr&eacute;es par les indices suivants, et ne concernent pas directement 
    la th&eacute;matique g&eacute;n&eacute;rale du film (ou son titre&#8230;).</p>

</li>
</ul>
</div>
