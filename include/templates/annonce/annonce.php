<div id="entete">Annonces</div>
<?php if (count($liste_des_annonces)):?>
<?php foreach($liste_des_annonces as $annonce):?>
<div class="annonce">
	<div class="annonce_titre">  
		<?php echo $annonce['titre'];?>
	</div>
	<div style="padding: 0px 20px 20px 20px; text-align:justify;">
		<?php echo $annonce['texte'];?>
	</div>
	<div class="annonce_signature"> 
		Post� le <?php echo $annonce['date'];?> par <?php echo $annonce['nom'];?>
	</div>
</div>
<?php endforeach;?>
<?php endif;?>

