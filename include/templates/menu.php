	<?php if ($user['admin']==1) :?>
		<ul>
			<li><a href="index.php?module=admin">Page des organisateurs</a></li>
		</ul>
	<?php endif;?>

<ul>
	<li><?php if ($user['id']>0):?><a href="index.php?module=connexion&amp;action=logout" >Se d&eacute;connecter</a>
		<?php else:?>
		<a href="index.php?module=connexion&action=login">Se connecter</a>
		<?php endif;?>
	</li>
</ul>

<ul>
	<li>
		<?php if (count($annonces)):?><a href="index.php?module=annonce" title="Derni&egrave;re annonce le <?php echo $annonces['0']['date'];?>">Annonces <?php if ($annonces_non_lues>0) echo "(".$annonces_non_lues.")"?></a><?php else:?>Annonces<?php endif?>
	</li>
</ul>

<ul>
	<?php if (isset($semaines) && count($semaines)):?>
	<?php foreach ($semaines as $s) :?>
	<li>
		<?php if ($s['etat']=='en_cours'):?>
			<a class="en_cours" href="index.php?module=semaine&amp;semaine_id=<?php echo $s['id'];?>" title="Semaine <?php echo $s['num'];?>">Semaine <?php echo $s['num'];?></a>
		<?php elseif ($s['etat']=='finie'):?>
			<a class="finie" href="index.php?module=semaine&amp;semaine_id=<?php echo $s['id'];?>" title="Semaine <?php echo $s['num'];?>">Semaine <?php echo $s['num'];?></a>
		<?php else :?>
			Semaine <?php echo $s['num'];?>
		<?php endif?>
	</li>
	<?php endforeach;?>
	<?php endif;?>
</ul>

<ul>
	<li><a href="index.php?module=classement">Classement</a></li>
</ul>

<ul>
	<li><a href="index.php?module=betisier&amp;action=classement">B&ecirc;tisier</a></li>
</ul>

<ul>
	<li><a href="index.php?module=presentation">Qui sommes-nous ?</a></li>
</ul>
<ul>
	<li><a href="<?php echo FRCD_ARCHIVES?>" target="_blank">Archives</a></li>
</ul>
<ul>
	<li><a href="index.php?module=faq">FAQ</a></li>
</ul>

<div class="hidden">
	<p><a href="http://validator.w3.org/check?uri=referer">
	<img src="http://www.w3.org/Icons/valid-xhtml10-blue" alt="Valid XHTML 1.0 Transitional" height="31" width="88" /></a></p>
</div>
