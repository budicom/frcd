<center>
  <table class="adminboxline" cellspacing="0">
    <tr>
	<td class="adminboxtitle">La page de l'administrateur du jeu</td>
	</tr>
	<tr>
	  <td class="adminboxbody" >
         Salut &agrave; toi, � organisateur tout-puissant. Te voil&agrave; en position
         d'abuser de
         tes pouvoirs et de commettre les bourdes suivantes :
         <ul>
           <li>Comptes
             <ul>
                    <li><a href="index.php?module=admin&amp;action=creer_compte&amp;event=creer_admin">Cr&eacute;er un compte organisateur</a></li>
                    <li><a href="index.php?module=admin&amp;action=creer_compte&amp;event=creer_equipe">Cr&eacute;er un compte &eacute;quipe</a></li>
                    <?php if (is_array($equipes_actives) && count($equipes_actives)):?>
                        <li><form action="index.php?module=admin&amp;action=validation&amp;event=suspendre_compte" method="post">Suspendre le compte : 
                          <select name="equipe_id"><?php foreach ($equipes_actives as $n):?><option value="<?php echo $n['id'];?>"><?php echo $n['nom'];?></option><?php endforeach;?></select>
                              <input type="submit" value="Suspendre" onclick="if(!confirm('Veux-tu vraiment suspendre ce compte ?')) return false;"/>
                    </form></li><?php endif;?>
                    <?php if (is_array($equipes_suspendues) && count($equipes_suspendues)):?>
                    <li><form action="index.php?module=admin&amp;action=validation&amp;event=reactiver_compte" method="post">R&eacute;activer le compte : 
                    
                              <select name="equipe_id"><?php foreach ($equipes_suspendues as $n):?><option value="<?php echo $n['id'];?>"><?php echo $n['nom'];?></option><?php endforeach;?></select>
                              <input type="submit" value="R&eacute;activer"/>
                    </form></li>          
                    <?php endif;?>

            </ul>             
          </li>

                      
           <?php if (is_array($semaines) && count($semaines)):?>

           <li>Les points
             <ul>      
                <?php if (is_array($semaines_ouvertes) && count($semaines_ouvertes)
                && is_array($equipes_actives) && count($equipes_actives)):?>

                <li><form action="index.php"
                              method="get">
<input type="hidden" name="module" value="admin"/>
<input type="hidden" name="action" value="ajouter_propositions"/> Modifier les propositions de
                                              la semaine <select name="semaine_id"><?php foreach ($semaines_ouvertes as $n):?><option value="<?php echo $n['id']?>">Semaine <?php echo $n['num']?></option><?php endforeach;?></select> 
                                               <br/>pour l'&eacute;quipe
              <select name="equipe_id"><?php foreach ($equipes_actives as $n):?><option value="<?php echo $n['id']?>"><?php echo $n['nom']?></option><?php endforeach;?></select> 
                                               
                                              <input type="submit" value="Corriger"/>
                      </form></li>
                <?php endif;?>

                <?php if (is_array($equipes_actives) && count($equipes_actives)):?>
                <li>Bonus/malus
                <form action="index.php" method="get"> 
                  <input type="hidden" name="module" value="admin"/>
                  <input type="hidden" name="action" value="editer_bonus"/>
                  Modifier le bonus/malus de l'�quipe :
                  <select name="equipe_id"><?php foreach ($equipes_actives as $n):?><option value="<?php echo $n['id']?>"><?php echo $n['nom']?></option><?php endforeach;?></select> <input type="submit" value="Ok"/>
                  </form>


                  </li>
                <li><a href="index.php?module=admin&amp;action=classement_provisoire">Classement provisoire</a></li>

                <?php endif;?>
                </ul></li>
           <?php endif;?>

           <li>Semaines
             <ul><li><a href="index.php?module=admin&amp;action=creer_semaine">Cr&eacute;er une semaine</a> </li>


                <?php if (is_array($semaines) && count($semaines)):?>
                <li> &Eacute;diter les semaines et leurs questions :
		        <ul>		
		        <?php foreach ($semaines as $n):?>

			        <li>
			        <b><a href="index.php?module=admin&amp;action=editer_semaine&amp;semaine_id=<?php echo $n['id']?>">Semaine <?php echo $n['num']?></a></b>
                                <?php if ($n['etat']=="en_cours"):?>
                                (<a href="index.php?module=admin&amp;action=validation&amp;event=clore_semaine&amp;semaine_id=<?php echo $n['id']?>"  onclick="if(!confirm('Veux-tu vraiment clore la semaine ?')) return false;">clore la semaine</a>)
                                <?php endif?>
<br/><i><?php echo $n['theme']?></i>
                                </li>
		        <?php endforeach;?>

		        </ul>
                </li>

                <?php endif;?>

            </ul>

        </li>





        <li>Annonces
        <ul>
        <li><a href="index.php?module=admin&amp;action=editer_annonce">Ajouter une annonce</a></li>
        <?php if (is_array($annonces) && count($annonces)):?>
                                <li><form action="index.php?module=admin&amp;action=editer_annonce" method="post">Editer une annonce :<br/> 
                                  <select name="annonce_id"><?php foreach ($annonces as $n):?><option value="<?php echo $n['id'];?>"><?php echo $n['titre'];?></option><?php endforeach;?></select>
                                      <input type="submit" value="Editer"/>
                            </form></li><?php endif;?>
        <?php if (is_array($annonces) && count($annonces)):?>
                                <li><form action="index.php?module=admin&amp;action=validation&amp;event=supprimer_annonce" method="post">Supprimer une annonce :<br/> 
                                  <select name="annonce_id"><?php foreach ($annonces as $n):?><option value="<?php echo $n['id'];?>"><?php echo $n['titre'];?></option><?php endforeach;?></select>
                                      <input type="submit" value="Supprimer" onclick="if(!confirm('Veux-tu vraiment supprimer cette annonce ?')) return false;"/>
                            </form></li><?php endif;?>
        </ul>
        </li>

           <li>Connexions
              <ul>
                <li>
                     <a href="index.php?module=admin&amp;action=analyser_connexions">Analyses des connexions</a>
                </li>
              </ul>
           </li>
        </ul>
                             	  </td>
	</tr>
	<tr>
	  <td class="adminboxbottom" align="center">
        <a href="index.php">retour � la page d'accueil</a>
	  </td>
	</tr>
  </table>
</center>
