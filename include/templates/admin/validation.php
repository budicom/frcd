<center>
	<table width="300" class="adminboxline" cellspacing="0">
		<tr>
			<td class="adminboxtitle"><?php echo $titre;?></td>
		</tr>
		<tr>
			<td class="adminboxbody" align="center">
				<?php echo $message;?>
			</td>
		</tr>
		<tr>
			<td class="adminboxbottom" align="center">
				<a href="<?php if (isset($retour)) echo $retour; else echo "index.php?module=admin"; ?>">Retour</a>
			</td>
		</tr>
	</table>
</center>
