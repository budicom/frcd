  <table  class="adminboxline" cellspacing="0">
    <tr>
	<td class="adminboxtitle">Cr&eacute;ation d'une semaine</td>
	</tr>
	<tr>
	  <td class="adminboxbody">

<form action="index.php?module=admin&amp;action=validation&amp;event=nouvelle_semaine"
          method="post" name="nouvelle_semaine" enctype="multipart/form-data">
	Image : <input name="image" value="" type="file"/><br/><br/>
        Th&egrave;me de la semaine : <input type="text"
                                     name="theme_semaine"/><br/><br/>
        Description de la semaine : (html autoris&eacute;)<br/><textarea
                                     name="description_semaine" cols="60" rows="3"></textarea><br/><br/>
        Date de d&eacute;but : 
        <select name="jour_debut" style="width:auto"><?php foreach ($jours as $n):?><option value="<?php echo $n;?>"><?php echo $n;?></option><?php endforeach;?>	
	</select>
<select name="mois_debut" style="width:auto"><?php foreach ($mois as $n):?><option value="<?php echo $n;?>"><?php echo strftime("%B", mktime(0, 0, 0, $n, 1, 2005));?></option><?php endforeach;?>	
	</select>
<select name="annee_debut" style="width:auto"><?php foreach ($annees as $n):?><option value="<?php echo $n;?>"><?php echo $n;?></option><?php endforeach;?>	
	</select>
	&agrave;
<select name="heure_debut" style="width:auto"><?php foreach ($heures as $n):?><option value="<?php echo $n;?>" <?php if ($n==FRCD_HEURE_DEFAUT) echo 'selected="selected"'?>><?php echo $n;?>h</option><?php endforeach;?>	
	</select>
<br/>
        Date de fin : 
        <select name="jour_fin" style="width:auto"><?php foreach ($jours as $n):?><option value="<?php echo $n;?>"><?php echo $n;?></option><?php endforeach;?>	
	</select>
<select name="mois_fin" style="width:auto"><?php foreach ($mois as $n):?><option value="<?php echo $n;?>"><?php echo strftime("%B", mktime(0, 0, 0, $n, 1, 2005));?></option><?php endforeach;?>	
	</select>
<select name="annee_fin" style="width:auto"><?php foreach ($annees as $n):?><option value="<?php echo $n;?>"><?php echo $n;?></option><?php endforeach;?>	
	</select>
	&agrave;
<select name="heure_fin" style="width:auto"><?php foreach ($heures as $n):?><option value="<?php echo $n;?>" <?php if ($n==FRCD_HEURE_DEFAUT) echo 'selected="selected"'?>><?php echo $n;?>h</option><?php endforeach;?>	
	</select>
 <br/><br/>
         Type de la semaine : 
        <select name="deroulement"><?php foreach ($types as $n):?><option value="<?php echo $n;?>"><?php echo $n;?></option><?php endforeach;?>	
	</select>
 <br/><br/>

 
        <div style="text-align:center"><input type="submit" value="Cr&eacute;er"/></div>
        </form>              
	  </td>
	</tr>
  <tr>
	  <td class="adminboxbottom" align="center">
        <a href="index.php?module=admin">Retour</a>
	  </td>
	</tr>
  </table>             
