<table  class="adminboxline" width="500" cellspacing="0">
  <tr><td class="adminboxtitle">G&eacute;n&eacute;ral</td></tr>
  <tr>
    <td class="adminboxbody">

      <form name="semaine" action="index.php?module=admin&amp;action=validation&amp;event=update_semaine" method="post"  enctype="multipart/form-data">
	      <input type="hidden" name="semaine_id" value="<?php echo $semaine['id']?>"/>


      <fieldset>
           <legend>Param&egrave;tres</legend>
                   <ul>
                        <li><b>Image :</b> <?php if ($semaine['image']=="") echo "Aucune image"?> <a href="<?php echo FRCD_FICHIERS_CHEMIN.$semaine['image'];?>"><?php echo $semaine['image'];?></a><br/>
              <input type="hidden" name="semaine_image" value="<?php echo $semaine['image']?>"/>
	              <input name="modifier_image" type="radio" class="radio" value="1"/>Nouvelle l'image <input name="nouvelle_image" value="" type="file" onchange = "document.forms.semaine.modifier_image[0].checked=true"/><br/>
                      <input name="modifier_image" type="radio" class="radio" value="0" onclick = "document.forms.semaine.nouvelle_image.value=''"/>Aucune l'image<br/></li>
                        <li><b>Th&egrave;me :</b> <input type="text" name="theme_semaine" value="<?php echo $semaine['theme']?>"/></li>
                        <li><b>Description :</b>  (html autoris&eacute;) <textarea name="description_semaine" cols="60" rows="2"><?php echo $semaine['description']?></textarea></li>


                        <li><b>Ouverture :</b>
                      <select name="jour_debut" style="width:auto"><?php foreach ($jours as $n):?><option value="<?php echo $n;?>" <?php if($n==$jour_debut) echo 'selected="selected"'?>><?php echo $n;?></option><?php endforeach;?>	
	              </select>
              <select name="mois_debut" style="width:auto"><?php foreach ($mois as $n):?><option value="<?php echo $n;?>" <?php if($n==$mois_debut) echo 'selected="selected"'?>><?php echo strftime("%B", mktime(0, 0, 0, $n, 1, 2005));?></option><?php endforeach;?>	
	              </select>
              <select name="annee_debut" style="width:auto"><?php foreach ($annees as $n):?><option value="<?php echo $n;?>" <?php if($n==$annee_debut) echo 'selected="selected"'?>><?php echo $n;?></option><?php endforeach;?>	
	              </select>

	              &agrave;
              <select name="heure_debut" style="width:auto"><?php foreach ($heures as $n):?><option value="<?php echo $n;?>" <?php if($n==$heure_debut) echo 'selected="selected"'?>><?php echo $n;?>h</option><?php endforeach;?>	
	              </select>

              </li>
                        <li><b>Fermeture :</b>
                      <select name="jour_fin" style="width:auto"><?php foreach ($jours as $n):?><option value="<?php echo $n;?>" <?php if($n==$jour_fin) echo 'selected="selected"'?>><?php echo $n;?></option><?php endforeach;?>	
	              </select>
              <select name="mois_fin" style="width:auto"><?php foreach ($mois as $n):?><option value="<?php echo $n;?>" <?php if($n==$mois_fin) echo 'selected="selected"'?>><?php echo strftime("%B", mktime(0, 0, 0, $n, 1, 2005));?></option><?php endforeach;?>	
	              </select>
              <select name="annee_fin" style="width:auto"><?php foreach ($annees as $n):?><option value="<?php echo $n;?>" <?php if($n==$annee_fin) echo 'selected="selected"'?>><?php echo $n;?></option><?php endforeach;?>	
	              </select>

	              &agrave;
              <select name="heure_fin" style="width:auto"><?php foreach ($heures as $n):?><option value="<?php echo $n;?>" <?php if($n==$heure_fin) echo 'selected="selected"'?>><?php echo $n;?>h</option><?php endforeach;?>	
	              </select>

              </li>

                       <li>Type de la semaine : 
                      <select name="deroulement"><?php foreach ($types_de_semaines as $n):?><option value="<?php echo $n;?>" <?php if($n==$semaine['deroulement']) echo 'selected="selected"'?>><?php echo $n;?></option><?php endforeach;?>	
	              </select>
              </li>

              </ul>
              <br/>
              <div style="text-align:center"><input type="submit" value="Modifier"/></div>
              </fieldset>
      </form>

      <fieldset>
           <legend>Infos</legend>
           <ul>

                <li><b>&Eacute;tat : </b><?php echo $semaine['etat']?></li>
                <li><b>Type : </b><?php echo $semaine['deroulement']?></li>
                <li><b>Nombre de questions : </b> <?php echo count($questions)?></li>
           </ul>
      </fieldset>
      <fieldset>
           <legend>Op&eacute;rations</legend>
           <ul>
	      <?php if ($semaine['deroulement']=='synchronisee'):?>
              <li><form action="index.php"
                                            method="get">
              <input type="hidden" name="module" value="admin"/>
              <input type="hidden" name="action" value="validation"/> 
              <input type="hidden" name="event" value="ordre_semaine_synchronisee"/> 
              <input type="hidden" name="semaine_id" value="<?php echo $semaine['id']?>"/>
              Affichage des indices : <select name="ordre">
                <option value="1" <?php if ($semaine['ordre_actuel']==1) echo 'selected="selected"'?>>Questions</option>
                <option value="2" <?php if ($semaine['ordre_actuel']==2) echo 'selected="selected"'?>>Indices 1</option> 
                <option value="3" <?php if ($semaine['ordre_actuel']==3) echo 'selected="selected"'?>>Indices 2</option></select>
                                                     
                                                    <input type="submit" value="Afficher"/>
                            </form></li>
        <?php endif ?>

	      <?php if ($semaine['deroulement']=='progressive'):?>
              <li><form action="index.php"
                                            method="get">
              <input type="hidden" name="module" value="admin"/>
              <input type="hidden" name="action" value="validation"/> 
              <input type="hidden" name="event" value="questions_semaine_progressive"/> 
              <input type="hidden" name="semaine_id" value="<?php echo $semaine['id']?>"/>
              Nombre de questions &agrave; afficher : <input type="text" name="questions_a_afficher" value="<?php echo $semaine['questions_a_afficher']?>"/>
                                                     
                                                    <input type="submit" value="Modifier"/>
                            </form></li>
        <?php endif ?>

	      <?php if ($semaine['etat']!='en_cours'):?>
              	<li> <a href="index.php?module=admin&amp;action=ajouter_question&amp;semaine_id=<?php echo $semaine_id?>">Ajouter une question</a></li>
              	<?php if (count($questions)):?><li> <a href="index.php?module=admin&amp;action=validation&amp;event=melanger_semaine&amp;semaine_id=<?php echo $semaine_id?>">M&eacute;langer les questions</a></li><?php endif?>
	      <?php else:?>
		      <li>Ajouter une question (la semaine ne doit pas &ecirc;tre en cours)</li>
		      <li>M&eacute;langer les questions (la semaine ne doit pas &ecirc;tre en cours)</li>
	      <?php endif?>
	      <li>			<?php if ($semaine['etat']=='en_cours'):?>
				      <a href="index.php?module=admin&amp;action=validation&amp;event=clore_semaine&amp;semaine_id=<?php echo $semaine['id']?>"  onclick="if(!confirm('Veux-tu vraiment clore la semaine ?')) return false;">Clore la semaine</a>
				      <?php elseif ($semaine['etat']=='non_commencee'):?>
				      <a href="index.php?module=admin&amp;action=validation&amp;event=ouvrir_semaine&amp;semaine_id=<?php echo $semaine['id']?>"  onclick="if(!confirm('Veux-tu vraiment ouvrir la semaine ?')) return false;">Ouvrir la semaine</a>
				      <?php elseif ($semaine['etat']=='finie'):?>
				      <a href="index.php?module=admin&amp;action=validation&amp;event=rouvrir_semaine&amp;semaine_id=<?php echo $semaine['id']?>"  onclick="if(!confirm('Veux-tu vraiment rouvrir la semaine ?')) return false;">Rouvrir la semaine</a>
				      <?php endif?></li>
      <li><a href="index.php?module=admin&amp;action=validation&amp;event=supprimer_semaine&amp;semaine_id=<?php echo $semaine['id']?>" onclick="if(!confirm('Veux-tu vraiment supprimer cette semaine ?')) return false;"> <span style="color:red"> supprimer</span></a></li>




            
                 <?php if (is_array($equipes_actives) && count($equipes_actives)):?>

                      <li><form action="index.php"
                                    method="get">
      <input type="hidden" name="module" value="admin"/>
      <input type="hidden" name="action" value="ajouter_propositions"/> 
      <input type="hidden" name="semaine_id" value="<?php echo $semaine['id']?>"/>
      Modifier les propositions de l'&eacute;quipe <select name="equipe_id"><?php foreach ($equipes_actives as $n):?><option value="<?php echo $n['id']?>"><?php echo $n['nom']?></option><?php endforeach;?></select> 
                                                     
                                                    <input type="submit" value="Corriger"/>
                            </form></li>
                      <?php endif;?>
           </ul>
      </fieldset>
    </td>
  </tr>
  </table><br/>



<?php foreach ($questions as  $question):?>
<a name="<?php echo $question['id']?>"></a>

  <table  class="adminboxline" width="500" cellspacing="0" <?php if ($question['auteur_id']==$user['id']):?>style="background-color:#e5d5d0"<?php endif?>>
    <tr> <td class="adminboxtitle">Question <?php echo $question['ordre']?> (<?php echo $question['nom']?>)</td></tr>
    <tr>
    <td class="adminboxbody" <?php if ($question['auteur_id']==$user['id']):?>style="background-color:transparent"<?php endif?>>
    <b> <?php if ($question['annulee']):?>Annul&eacute;e<?php endif?></b>
    <br/>
    <fieldset>
    <legend>Indices</legend>
    <table cellspacing="0" width="100%">
      <tr>
        <?php foreach ($question['elements'] as $ordre => $element):?>
        <td  valign="top" align="center" width="33%">
            
            <?php if (isset($element['miniature']) && ($element['miniature']!="")):?>
            <a href="index.php?module=admin&amp;action=modifier_element&amp;question_id=<?php echo $question['id'];?>&amp;ordre=<?php echo $ordre;?>">
              <img src="<?php echo $element['miniature'];?>" alt="miniature"/></a>
              <br/>
              <table cellspacing="0" cellpadding="0">
                <tr><td>
                  Indication :
                  <a href="index.php?module=admin&amp;action=editer_indication&amp;question_id=<?php echo $question['id'];?>&amp;ordre=<?php echo $ordre;?>">        
                  <?php if (isset($element['indication']) && ($element['indication']!="")):?>
                  <?php echo $element['indication'];?>
                  <?php else:?>vide
                  <?php endif;?>
                  </a></td></tr>
                <tr><td>Points :
                  <a href="index.php?module=admin&amp;action=modifier_element_points&amp;question_id=<?php echo $question['id'];?>&amp;ordre=<?php echo $ordre;?>"><?php echo $element['points'];?></a>
                </td></tr>
              </table>
            <?php else :?>
              <a href="index.php?module=admin&amp;action=modifier_element&amp;question_id=<?php echo $question['id'];?>&amp;ordre=<?php echo $ordre;?>">Editer</a>
            <?php endif;?>
        </td>
        <?php endforeach;?>
      </tr>
    </table>

</fieldset> 
 </td>
  </tr>
  <tr><td class="adminboxbottom" align="center" <?php if ($question['auteur_id']==$user['id']):?>style="background-color:transparent"<?php endif?>>
<br/>
            <form action="index.php?module=admin&amp;action=validation&amp;event=update_question" method="post">
<fieldset> 
          <legend></legend>  
             Quelle question ?
          <select name="libelle_id">            <?php foreach ($libelles as $libelle):?>
            <option value="<?php echo $libelle['id'];?>" <?php if ($libelle['id']==$question['libelle_id']):?>selected="selected"<?php endif;?>>            <?php echo $libelle['texte'];?>
            </option>            <?php endforeach;?>
          </select>
             <br/>
             R&eacute;ponse : <input type="text" name="reponse" size="50" maxlength="100" value="<?php echo $question['reponse'];?>"/>
             
             <br/>Titre original :
             <input type="text" name="titre_original" size="50" maxlength="100" value="<?php echo $question['titre_original'];?>"/>
             <br/>R&eacute;alisateur(s) :
             <input type="text" name="realisateur" size="50" maxlength="200" value="<?php echo $question['realisateur'];?>"/>
             <br/>Ann&eacute;e :
             <input type="text" name="annee" size="8" maxlength="4" value="<?php echo $question['annee'];?>"/>
             &nbsp;&nbsp;&nbsp;&nbsp;Lien IMDB :
             <input type="text" name="lien_imdb" size="30" maxlength="100" value="<?php echo $question['lien_imdb'];?>"/>
             
             
        <br/>Organisateur : <?php echo $question['nom'];?>
  <br/><br/>
              <input type="hidden" name="question_id"
                     value="<?php echo $question['id'];?>"/>
                     Commentaire : (html autoris&eacute;)<br/>
              <textarea name="commentaire" cols="72" rows="5"><?php echo $question['commentaire']?></textarea>
<ul>
<li>Cacher les points aux joueurs <input type="checkbox" name="cacher_les_points" <?php if ($question['cacher_les_points']) echo 'checked="checked"'?>/></li>
</ul>
</fieldset>
  <br/><input type="submit" value="Valider les changements"/>
            </form><form action="index.php?module=admin&amp;action=validation&amp;event=supprimer_question" method="post">
            
            <input type="hidden" name="question_id" value="<?php echo $question['id'];?>"/>
            <input type="submit" value="Supprimer la question" onclick="if(!confirm('Veux-tu vraiment supprimer cette question ?')) return false;"/>
            </form>
          <?php if ($question['annulee']):?>
          <form action="index.php?module=admin&amp;action=validation&amp;event=retablir_question" method="post">
            
            <input type="hidden" name="question_id" value="<?php echo $question['id'];?>"/>
            <input type="submit" value="R&eacute;tablir la question" onclick="if(!confirm('Veux-tu vraiment r&eacute;tablir cette question ?')) return false;"/>
            </form>
          <?php else:?>
          <form action="index.php?module=admin&amp;action=validation&amp;event=annuler_question" method="post">
            
            <input type="hidden" name="question_id" value="<?php echo $question['id'];?>"/>
            <input type="submit" value="Annuler la question" onclick="if(!confirm('Veux-tu vraiment annuler cette question ?')) return false;"/>
            </form>
          <?php endif?>
 
  </td>
  </tr>
  
  </table><br/>



<?php endforeach;?>
<div style="text-align:center">
<a href="index.php?module=admin">Retour</a>           </div>
