<center>
  <table width="500" class="adminboxline" cellspacing="0">
    <tr>
	  <td class="adminboxbody" colspan="4">Connexions d'�quipes ayant la m�me adresse IP</td>
    </tr>
    <tr>
	<td class="adminboxtitle">Equipe 1</td>
	<td class="adminboxtitle">Equipe 2</td>
	<td class="adminboxtitle">IP</td>
	<td class="adminboxtitle"></td>
	</tr>
	<?php foreach ($ip_communes as $ip_commune):?>
    <tr>
	  <td class="adminboxbody">
	    <?php echo $ip_commune['nom1'];?>
	  </td>
	  <td class="adminboxbody">
	    <?php echo $ip_commune['nom2'];?>
	  </td>
	  <td class="adminboxbody">
	    <?php echo $ip_commune['adresse_ip'];?>
	  </td>
	  <td class="adminboxbody">
	    <form action="index.php?module=admin&action=detail_ip" method="POST">
	       <input type="hidden" name="adresse_ip" value="<?php echo $ip_commune['adresse_ip'];?>">
	       <input type="submit" value="D�tail">
	    </form>
	  </td>
	</tr>
	<?php endforeach;?>
  </table>
  <br>
  <table width="500" class="adminboxline" cellspacing="0">
    <tr>
	  <td class="adminboxbody" colspan="3">Informations sur les connexions</td>
    </tr>
    <tr>
	<td class="adminboxtitle">Equipe</td>
	<td class="adminboxtitle">Nombre de connexions</td>
	<td class="adminboxtitle">Derni�re connexion</td>
	</tr>
	<?php foreach ($nombre_conn_equipes as $nombre_conn_equipe):?>
    <tr>
	  <td class="adminboxbody">
	    <?php echo $nombre_conn_equipe['nom'];?>
	  </td>
	  <td class="adminboxbody">
	    <?php echo $nombre_conn_equipe['nombre_conn'];?>
	  </td>
	  <td class="adminboxbody">
	    <?php echo $nombre_conn_equipe['derniere_conn'];?>
	  </td>
	</tr>
	<?php endforeach;?>
  </table>

  <table width="500" class="adminboxline" cellspacing="0">
    <tr>
	<td class="adminboxtitle">Equipe</td>
	<td class="adminboxtitle">Nombre d'IP distinctes</td>
	</tr>
	<?php foreach ($nombre_ip_equipes as $nombre_ip_equipe):?>
    <tr>
	  <td class="adminboxbody">
	    <?php echo $nombre_ip_equipe['nom'];?>
	  </td>
	  <td class="adminboxbody">
	    <?php echo $nombre_ip_equipe['nombre_ip'];?>
	  </td>
	</tr>
	<?php endforeach;?>
  </table>

  <br>
  <table width="500" class="adminboxline" cellspacing="0">
  <tr>
  <td class="adminboxtitle">Equipes actuellement connect&eacute;es :</td>
  </tr>
  <tr><td class="adminboxbody">
	<?php foreach ($equipes_connectees as $equipe):?>
    <span <?php if ($equipe['admin']):?>style="font-style:italic"<?php endif?>><?php echo $equipe['nom']?></span><br>
	<?php endforeach;?>
  </td></tr>
  </table>

  <a href="index.php?module=admin">Retour</a>
</center>
