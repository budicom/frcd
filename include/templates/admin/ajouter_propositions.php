<script type="text/javascript">
<!--
function show(id)
{
		document.getElementById(id).style.display='block'; 
}

function hide(id)
{
		document.getElementById(id).style.display='none';
}

//-->
</script>

<center>
  <table width="500" class="adminboxline" cellspacing="0">
    <tr>
	<td class="adminboxtitle">Edition d'une semaine</td>
	</tr>
	<tr>
	  <td class="adminboxbody">
      <?php if(isset($derniere_correction)) {?>
          Derni�re correction de l'�quipe cette semaine : <?php echo $derniere_correction['date_correction'];?>
          par <?php echo $derniere_correction['nom_correcteur'];?><br/><br/>
      <?php } else {?>
          L'�quipe n'a pas encore �t� corrig�e cette semaine. <br/><br/>
      <?php }?>

<form action="index.php?module=admin&amp;action=validation&amp;event=nouvelle_proposition" method="post">
<input type="hidden" name="equipe_id" value="<?php echo $equipe_id;?>"/>
<input type="hidden" name="semaine_id" value="<?php echo $semaine_id;?>"/>
<?php foreach ($questions as $question):?>

  <table  class="adminboxline" cellspacing="0" width="100%"><tr>
  <td class="adminboxbottom">Question <?php echo $question['ordre'];?><br/>
  R&eacute;ponse : <b><?php echo $question['reponse'];?></b>
  <?php if ($question['titre_original']):?>
        <br/>Titre original : <?php echo $question['titre_original'];?>
  <?php endif;?>
  <?php if ($question['realisateur']):?>
        <br/>R&eacute;alisateur : <?php echo $question['realisateur'];?>
  <?php endif;?>
  <?php if ($question['annee']):?>
        <br/>Ann&eacute;e : <?php echo $question['annee'];?>
  <?php endif;?>
  <br/>
  <br/>

    <?php if (isset($question['elements'])) :?>
	<?php foreach ($question['elements'] as $ordre => $element):?>
<a href="<?php echo $element['fichier']?>"><img src="<?php echo $element['miniature']?>" alt="miniature"/></a>
	<?php endforeach?>
	<?php if (isset($question['points'])&&$question['points']!=NULL):?>
    <b><?php echo $question['points'];?>pt</b>
	<?php endif?>

      <?php $dernier_ordre=false;?>
  <ul>
      <?php foreach ($question['elements'] as $ordre => $element):?>

      <li>�l�ment <?php echo $ordre;?> : 
        <?php if ($element['correct']=="1") :?>Juste <?php $dernier_ordre=$ordre; $dernier_correct=1;?>
        <?php elseif ($element['correct']=="0") :?>Fausse <?php $dernier_ordre=$ordre; $dernier_correct=0;?>
        <?php else:?>
        <div>
              <input type="radio" class="radio" checked="checked" name="juste[<?php echo $question['id'];?>][<?php echo $ordre;?>]"  value="" onclick="hide('betise_<?php echo $element['id'];?>');"/>En attente<br/>
              <input type="radio" class="radio" name="juste[<?php echo $question['id'];?>][<?php echo $ordre;?>]" value="1" onclick="show('betise_<?php echo $element['id'];?>');"/>Juste<br/>
              <input type="radio" class="radio" name="juste[<?php echo $question['id'];?>][<?php echo $ordre;?>]" value="0" onclick="show('betise_<?php echo $element['id'];?>');"/>Fausse
              <div id="betise_<?php echo $element['id'];?>" class="hidden" style="margin:10px">B�tise : <input type="text" name="betise[<?php echo $element['id'];?>]" size="50"/></div>
        </div>

      <?php endif;?>
    
      </li>
      <?php endforeach;?>
  </ul>
    <?php endif;?>
<?php if ($dernier_ordre!==false) : ?>
<div style="text-align:center;">
    <a href="index.php?module=admin&amp;action=validation&amp;event=annuler_proposition&amp;eq_id=<?php echo $equipe_id;?>&amp;s_id=<?php echo $semaine_id;?>&amp;q_id=<?php echo $question['id'];?>&amp;o=<?php echo $dernier_ordre;?>&amp;c=<?php echo $dernier_correct;?>" onclick="if(!confirm('Veux-tu vraiment annuler la derni�re correction ?')) return false;">Annuler la derni�re correction
    </a>    
</div>
<?php endif;?>

  </td>
  </tr>
  
  </table><br/>
<?php endforeach;?>
<center><input type="submit" value="Valider"/></center>
</form>

	  </td>
	</tr>
  <tr>
	  <td class="adminboxbottom" align="center">
        <a href="index.php?module=admin">Retour</a>
	  </td>
	</tr>
  </table>
</center>              
