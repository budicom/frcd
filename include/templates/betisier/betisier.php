<div id="entete">
    <?php if ($semaine['image'])
	echo "<img src=\"".FRCD_FICHIERS_CHEMIN.$semaine['image']."\" alt=\"Semaine".$semaine['numero']."\"/>";
	else echo "Semaine ".$semaine['numero']?>
</div>

<div style="text-align:center;margin:20px">
<?php if (isset($user) && isset($votes)) {?>
    Chaque semaine, vous pouvez voter pour vos <?php echo FRCD_BETISIER_NB_VOTES?> �neries pr�f�r�es.<br/>
    <?php $nb_votes_restant = FRCD_BETISIER_NB_VOTES - count($votes);
    if ($nb_votes_restant > 0) {?>
        Il reste <?php echo $nb_votes_restant;?> vote(s) � votre �quipe pour cette semaine !
    <?php } else {?>
        Vous avez utilis� tous vos votes pour cette semaine.
    <?php }?>
<?php }?>
</div>

<!-- bloc d'affichage de toutes les questions -->
<table class="questions">
        <tr>
          <td><img src="images/spacer.gif" width="590" height="1" alt=""/></td>
        </tr>
          <?php foreach ($questions as $question_id => $question) :?>
        <tr>
                <td>
                  <?php  if (isset($question['elements'])) foreach ($question['elements'] as $ordre => $element):?>
                  <!-- bloc d'affichage d'une question -->
                  <table cellpadding=0 cellspacing=0 border=0>

                  <tr>
                          <td><img src="images/spacer.gif" width="30" height="140" alt=""/></td>
                          <td valign="top">
                              <img src="images/spacer.gif" width="560" height="1" alt=""/><br/>
                                  <table border="0" class="question" cellpadding="0" cellspacing="0">
                                  <tr>
                                          <td valign="top">
                                          <div class="miniature"><a href="<?php echo $element['fichier'];?>"><img src="<?php echo $element['miniature'];?>" alt="miniature"/></a></div>
                      
                                          </td>
                                          <td>
                                          <img src="images/spacer.gif" height="1" width="15" alt=""/>
                                          </td>
                  
                  
                                          <td>
                                                  <table>
                                                        <?php foreach ($element['betises'] as $betise) :?>
                                                           <tr><td>
                                                            <form action="index.php?module=betisier&amp;semaine_id=<?php echo $semaine['id'];?>" method="POST">
                                                           <table><tr>
                                                                <td>
                                                                <?php if (isset($betise['vote'])&& $betise['vote']) : ?>
	                                                                   <img src="images/frcd_vote_checked.jpg" align="left" alt="checked"/>
	                                                                <?php  else :?>
	                                                                
                                                                            
                                                                   <img src="images/frcd_vote_unchecked.jpg" align="left" alt="unchecked"/><?php endif;?>
                                                                   
                                                                </td>
                                                                <td width="300">
                                                                    <p><?php echo $betise['betise'];?></p>
                                                                   <?php if ($user['id'] && !$betise['peut_voter']) :?>
                                                                        <div style="text-align:right;font-weight:bold;font-style: italic;width:100%">...<?php echo $betise['nom_equipe'];?></div>
                                                                   <?php endif;?>
                                                                </td>
                                                                <td width="100" align="center">
                                                                    <?php if ($betise['peut_voter']) :?>
                                                    	               <input type="hidden" name="betise_id" value="<?php echo $betise['id'];?>">
	                                                                    <input type="image" src="images/frcd_bouton_voter.jpg" style="border:0; alt="voter"">
	                                                                    <?php endif;?>
                                    	                            
                                    	                        </td>
                    	                       
                                    	                   </tr></table>
                                             	           </form>
                                                           </td></tr>

                                                        <?php endforeach;?>
                                                </table>
                                        </td>
                                   </tr>
                                   </table>
                        </td>
                  </tr>        
                  </table>
  <!-- bloc de separation -->
  <div class="separation">
  </div>       <?php endforeach;?>  
  
        </td>
        </tr>
  <?php endforeach;?>
</table>
