<div id="entete">Classement du b&ecirc;tisier</div>

<div id="meilleures_betises">
<table>
  <?php foreach ($meilleures_betises as $betise):?>
  <tr>
    <td>
      <div class="miniature">
        <a href="<?php echo FRCD_FICHIERS_CHEMIN.$betise['fichier']?>"><img src="<?php echo FRCD_FICHIERS_CHEMIN.$betise['miniature']?>" alt="miniature"/></a>
      </div>
    </td>
    <td>
    <div>
      <div>B&ecirc;tise : <?php echo $betise['betise']?></div>
      <div>&Eacute;quipe : <?php echo $betise['nom']?></div>
      <div><?php echo $betise['votes']?> vote(s)</div>
    </div>
    </td>
  </tr>
  <?php endforeach?>
</table>
</div>



<table id="classement" cellspacing="0" cellpadding="3">

<tr>
<th></th>
<th>Equipe</th>
<th>Score</th>
</tr>
<?php  $lignepaire=false ?>

<?php  foreach ($lignes_classement as $ligne_classement):?>
<tr 
<?php  if (isset($user) && $user['id']==$ligne_classement['equipe_id']) :?> class="classement_ligne_mon_equipe"
<?php  elseif ($lignepaire) :?> 
class="classement_ligne_paire"
onmouseover="this.className='classement_ligne_surlignee';"
onmouseout="this.className='classement_ligne_paire';"
<?php else:?>
class="classement_ligne_impaire"
onmouseover="this.className='classement_ligne_surlignee';"
onmouseout="this.className='classement_ligne_impaire';"
<?php endif;?>
>
<td><?php echo $ligne_classement['rang'];?></td>
<td nowrap="nowrap"><?php echo $ligne_classement['nom'];?></td>

<td><?php echo $ligne_classement['score'];?></td>
</tr>
<?php $lignepaire=!$lignepaire; ?>
<?php endforeach;?>
</table>
