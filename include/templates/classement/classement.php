<div id="entete">Classement g&eacute;n&eacute;ral</div>
<table id="classement" cellspacing="0" cellpadding="3">
<tr>
        <th></th>
        <th>Equipe</th>

        <?php  $counter=1;?>
        <?php foreach ($semaines as $semaine):?>
                <th><a href="index.php?module=classement&amp;action=semaine&amp;semaine_id=<?php echo $semaine['id']?>">S<?php echo $counter;?></a></th>
                <?php $counter+=1;?>
        <?php endforeach;?>
        <th>Bonus</th>
        <th>Total</th>
</tr>
<?php  $lignepaire=false ?>
<?php  foreach ($equipes as $equipe_id => $equipe):?>
<tr 
        <?php  if ($user['id']==$equipe_id) :?> class="classement_ligne_mon_equipe"
        <?php  elseif ($lignepaire) :?> 
        class="classement_ligne_paire"
        onmouseover="this.className='classement_ligne_surlignee';"
        onmouseout="this.className='classement_ligne_paire';"
        <?php else:?>
        class="classement_ligne_impaire"
        onmouseover="this.className='classement_ligne_surlignee';"
        onmouseout="this.className='classement_ligne_impaire';"
        <?php endif;?>
        >
        <td><?php echo $equipe['rang'];?></td>
        <td nowrap="nowrap"><?php echo $equipe['nom'];?></td>
        <?php foreach ($semaines as $semaine):?>
                <td>
                <?php  if (isset($scores[$equipe_id][$semaine['id']]['points'])) echo $scores[$equipe_id][$semaine['id']]['points'];?>
                </td>
        <?php endforeach;?>
        <td><?php if ($equipe['bonus']>0) echo "+"; if ($equipe['bonus']!=0) echo $equipe['bonus'];?>
        </td>
        <td><?php echo $equipe['points'];?>
        </td>
</tr>
<?php $lignepaire=!$lignepaire; ?>
<?php endforeach;?>
</table>
