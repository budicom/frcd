<div id="entete">
    <?php if ($semaine['image']) 
	echo "<img src=\"".FRCD_FICHIERS_CHEMIN.$semaine['image']."\"/>"; 
	else echo "Semaine ".$semaine['numero']?>
</div>

<table id="classement" cellspacing="0" cellpadding="3" width="100%">
<tr>
<th>Rang</th>
<th>Equipe</th>
<?php foreach ($questions as $question_id=>$question):?>
  <th><?php echo $question['ordre'];?></th>
<?php endforeach;?>
<th>Total</th>
</tr>
<?php  $lignepaire=false ?>
<?php  foreach ($equipes as $equipe_id => $equipe):?>
  <tr 
  <?php  if ($user['id']==$equipe_id) :?> class="classement_ligne_mon_equipe"
  <?php  elseif ($lignepaire) :?> 
  class="classement_ligne_paire"
  onmouseover="this.className='classement_ligne_surlignee';"
  onmouseout="this.className='classement_ligne_paire';"
  <?php else:?>
  class="classement_ligne_impaire"
  onmouseover="this.className='classement_ligne_surlignee';"
  onmouseout="this.className='classement_ligne_impaire';"
  <?php endif;?>
  >
  <td><?php echo $equipe['rang'];?></td>
  <td nowrap="nowrap"><?php echo $equipe['nom'];?></td>
  <?php foreach ($questions as $question):?>
  <td>
  <?php  if (isset($scores[$equipe_id][$question['id']]['points'])) echo $scores[$equipe_id][$question['id']]['points'];?>
  </td>
  <?php endforeach;?>
  <td <?php if ($equipe['bonnes_reponses'] == count($questions)):?>class="classement_gc"<?php endif;?>><?php echo $equipe['points'];?>

  </td>
  </tr>
  <?php $lignepaire=!$lignepaire; ?>

<?php endforeach;?>
</table>
