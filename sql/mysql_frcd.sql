-- phpMyAdmin SQL Dump
-- version 3.1.5
-- http://www.phpmyadmin.net
--
-- Serveur: localhost:3306
-- Généré le : Mar 16 Juin 2009 à 16:55
-- Version du serveur: 5.1.35
-- Version de PHP: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Base de données: `frcd`
--

-- --------------------------------------------------------

--
-- Structure de la table `annonces`
--

CREATE TABLE IF NOT EXISTS `annonces` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `user_id` tinyint(3) unsigned NOT NULL,
  `titre` char(255) COLLATE latin1_general_ci NOT NULL,
  `texte` text COLLATE latin1_general_ci NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `annonces_ibfk_1` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Contenu de la table `annonces`
--


-- --------------------------------------------------------

--
-- Structure de la table `bareme`
--

CREATE TABLE IF NOT EXISTS `bareme` (
  `ordre` tinyint(4) NOT NULL,
  `points` tinyint(4) NOT NULL,
  PRIMARY KEY (`ordre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Contenu de la table `bareme`
--

INSERT INTO `bareme` (`ordre`, `points`) VALUES
(1, 3),
(2, 2),
(3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `betises`
--

CREATE TABLE IF NOT EXISTS `betises` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `element_id` int(10) unsigned NOT NULL,
  `equipe_id` tinyint(3) unsigned NOT NULL,
  `betise` text COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `element_id` (`element_id`,`equipe_id`),
  KEY `betises_ibfk_1` (`equipe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Contenu de la table `betises`
--


-- --------------------------------------------------------

--
-- Structure de la table `connexions`
--

CREATE TABLE IF NOT EXISTS `connexions` (
  `horodate` datetime NOT NULL,
  `equipe_id` tinyint(3) unsigned DEFAULT NULL,
  `session_id` char(32) COLLATE latin1_general_ci DEFAULT NULL,
  `adresse_ip` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `navigateur` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `si_login` tinyint(1) NOT NULL,
  KEY `connexions_ibfk_1` (`equipe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Contenu de la table `connexions`
--


-- --------------------------------------------------------

--
-- Structure de la table `editions`
--

CREATE TABLE IF NOT EXISTS `editions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `semaine_id` int(10) unsigned NOT NULL DEFAULT '0',
  `equipe_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `organisateur_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `editions_ibfk_1` (`equipe_id`),
  KEY `editions_ibfk_2` (`organisateur_id`),
  KEY `editions_ibfk_3` (`semaine_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Contenu de la table `editions`
--


-- --------------------------------------------------------

--
-- Structure de la table `elements`
--

CREATE TABLE IF NOT EXISTS `elements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordre` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `question_id` int(10) unsigned NOT NULL DEFAULT '0',
  `type_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `fichier` text COLLATE latin1_general_ci NOT NULL,
  `miniature` text COLLATE latin1_general_ci NOT NULL,
  `indication` text COLLATE latin1_general_ci NOT NULL,
  `points` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `question_id` (`question_id`,`ordre`),
  KEY `elements_ibfk_2` (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Contenu de la table `elements`
--


-- --------------------------------------------------------

--
-- Structure de la table `equipes`
--

CREATE TABLE IF NOT EXISTS `equipes` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `nom` char(120) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `code` char(32) COLLATE latin1_general_ci NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `status` enum('active','disabled') COLLATE latin1_general_ci NOT NULL DEFAULT 'active',
  `derniere_activite` datetime DEFAULT NULL,
  `avatar` char(64) COLLATE latin1_general_ci DEFAULT NULL,
  `description` text COLLATE latin1_general_ci,
  `bonus` int(11) NOT NULL DEFAULT '0',
  `effectif` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `nom` (`nom`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=2 ;

--
-- Contenu de la table `equipes`
--

INSERT INTO `equipes` (`id`, `nom`, `code`, `admin`, `status`, `derniere_activite`, `avatar`, `description`, `bonus`, `effectif`) VALUES
(1, 'admin', 'admin', 1, 'active', NULL, NULL, NULL, 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `libelles`
--

CREATE TABLE IF NOT EXISTS `libelles` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `texte` text COLLATE latin1_general_ci NOT NULL,
  `icone` text COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Contenu de la table `libelles`
--

INSERT INTO `libelles` (`id`, `texte`, `icone`) VALUES
(1, 'Quel film ?', 'frcd_quelfilm.gif'),
(2, 'Quel court-métrage ?', 'frcd_quelcourt.gif'),
(3, 'Qui est-ce ?', 'frcd_quiestce.gif');

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `points`
--
CREATE TABLE IF NOT EXISTS `points` (
`equipe_id` tinyint(3) unsigned
,`equipe_nom` char(120)
,`question_id` int(10) unsigned
,`semaine_id` int(10) unsigned
,`points` int(7)
,`elements_restants` bigint(22)
,`question_etat` varchar(13)
);
-- --------------------------------------------------------

--
-- Structure de la table `propositions`
--

CREATE TABLE IF NOT EXISTS `propositions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordre` int(11) NOT NULL DEFAULT '0',
  `correct` tinyint(1) NOT NULL DEFAULT '0',
  `question_id` int(10) unsigned NOT NULL DEFAULT '0',
  `equipe_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `horodate` datetime NOT NULL,
  `correcteur_id` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `equipe_id_2` (`equipe_id`,`question_id`),
  KEY `propositions_ibfk_2` (`correcteur_id`),
  KEY `propositions_ibfk_3` (`question_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Contenu de la table `propositions`
--


-- --------------------------------------------------------

--
-- Structure de la table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `semaine_id` int(10) unsigned NOT NULL DEFAULT '0',
  `libelle_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `organisateur_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ordre` int(2) unsigned NOT NULL DEFAULT '0',
  `reponse` text COLLATE latin1_general_ci NOT NULL,
  `commentaire` text COLLATE latin1_general_ci NOT NULL,
  `annulee` tinyint(1) NOT NULL DEFAULT '0',
  `titre_original` text COLLATE latin1_general_ci,
  `realisateur` text COLLATE latin1_general_ci,
  `lien_imdb` text COLLATE latin1_general_ci,
  `annee` varchar(4) COLLATE latin1_general_ci DEFAULT NULL,
  `cacher_les_points` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `libelle_id` (`libelle_id`),
  KEY `semaine_id` (`semaine_id`),
  KEY `organisateur_id` (`organisateur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Contenu de la table `questions`
--


-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `scores`
--
CREATE TABLE IF NOT EXISTS `scores` (
`equipe_id` tinyint(3) unsigned
,`equipe_nom` char(120)
,`points` decimal(26,0)
,`bonus` int(11)
);
-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `scores_semaines`
--
CREATE TABLE IF NOT EXISTS `scores_semaines` (
`equipe_id` tinyint(3) unsigned
,`equipe_nom` char(120)
,`semaine_id` int(10) unsigned
,`points` decimal(32,0)
,`bonnes_reponses` bigint(21)
);
-- --------------------------------------------------------

--
-- Structure de la table `semaines`
--

CREATE TABLE IF NOT EXISTS `semaines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(128) COLLATE latin1_general_ci NOT NULL,
  `theme` text COLLATE latin1_general_ci NOT NULL,
  `description` text COLLATE latin1_general_ci NOT NULL,
  `image` varchar(128) COLLATE latin1_general_ci DEFAULT NULL,
  `debut` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fin` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `etat` enum('non_commencee','en_cours','finie') COLLATE latin1_general_ci NOT NULL DEFAULT 'non_commencee',
  `deroulement` enum('normale','synchronisee','progressive') COLLATE latin1_general_ci DEFAULT NULL,
  `questions_a_afficher` tinyint(4) NOT NULL DEFAULT '5',
  `ordre_actuel` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `etat` (`etat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Contenu de la table `semaines`
--


-- --------------------------------------------------------

--
-- Structure de la table `types`
--

CREATE TABLE IF NOT EXISTS `types` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `nom` text COLLATE latin1_general_ci NOT NULL,
  `icone` text COLLATE latin1_general_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `types`
--

INSERT INTO `types` (`id`, `nom`, `icone`) VALUES
(1, 'image', ''),
(2, 'son', 'son.jpg'),
(3, 'texte', 'texte.jpg'),
(4, 'video', 'video.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `votes`
--

CREATE TABLE IF NOT EXISTS `votes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `equipe_id` tinyint(3) unsigned NOT NULL,
  `betise_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `equipe_id` (`equipe_id`),
  KEY `betise_id` (`betise_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Contenu de la table `votes`
--


-- --------------------------------------------------------

--
-- Structure de la vue `points`
--
DROP TABLE IF EXISTS `points`;

CREATE VIEW `points` AS select `Eq`.`id` AS `equipe_id`,`Eq`.`nom` AS `equipe_nom`,`Q`.`id` AS `question_id`,`Q`.`semaine_id` AS `semaine_id`,(`El`.`points` * `P`.`correct`) AS `points`,((select count(0) AS `COUNT(*)` from (`elements` join `questions`) where ((`elements`.`question_id` = `questions`.`id`) and (`elements`.`question_id` = `Q`.`id`)) group by `questions`.`id`) - `P`.`ordre`) AS `elements_restants`,if(((`P`.`correct` = 1) or ((`P`.`correct` = 0) and (((select count(0) AS `count(0)` from (`elements` join `questions`) where ((`elements`.`question_id` = `questions`.`id`) and (`elements`.`question_id` = `Q`.`id`)) group by `questions`.`id`) - `P`.`ordre`) = 0))),_latin1'finie',`S`.`etat`) AS `question_etat` from ((((`equipes` `Eq` join `propositions` `P`) join `questions` `Q`) join `elements` `El`) join `semaines` `S`) where ((`Q`.`id` = `P`.`question_id`) and (`S`.`id` = `Q`.`semaine_id`) and (`P`.`equipe_id` = `Eq`.`id`) and (`El`.`ordre` = `P`.`ordre`) and (`El`.`question_id` = `Q`.`id`) and (`El`.`points` > 0) and (`Q`.`annulee` = 0));

-- --------------------------------------------------------

--
-- Structure de la vue `scores`
--
DROP TABLE IF EXISTS `scores`;

CREATE VIEW `scores` AS select `Eq`.`id` AS `equipe_id`,`Eq`.`nom` AS `equipe_nom`,(sum(`El`.`points`) + `Eq`.`bonus`) AS `points`,`Eq`.`bonus` AS `bonus` from ((((`equipes` `Eq` join `propositions` `P`) join `questions` `Q`) join `elements` `El`) join `semaines` `S`) where ((`Q`.`semaine_id` = `S`.`id`) and (`Q`.`id` = `P`.`question_id`) and (`P`.`equipe_id` = `Eq`.`id`) and (`El`.`ordre` = `P`.`ordre`) and (`El`.`question_id` = `Q`.`id`) and (`P`.`correct` = 1) and (`El`.`points` > 0) and (`S`.`etat` = _latin1'finie') and (`Q`.`annulee` = 0)) group by `P`.`equipe_id` order by (sum(`El`.`points`) + `Eq`.`bonus`) desc,`Eq`.`nom`;

-- --------------------------------------------------------

--
-- Structure de la vue `scores_semaines`
--
DROP TABLE IF EXISTS `scores_semaines`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `scores_semaines` AS select `points`.`equipe_id` AS `equipe_id`,`points`.`equipe_nom` AS `equipe_nom`,`points`.`semaine_id` AS `semaine_id`,sum(`points`.`points`) AS `points`,count(`points`.`points`) AS `bonnes_reponses` from `points` where ((`points`.`points` > 0) and (`points`.`question_etat` = _latin1'finie')) group by `points`.`equipe_id`,`points`.`semaine_id`;

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `annonces`
--
ALTER TABLE `annonces`
  ADD CONSTRAINT `annonces_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `equipes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `betises`
--
ALTER TABLE `betises`
  ADD CONSTRAINT `betises_ibfk_1` FOREIGN KEY (`equipe_id`) REFERENCES `equipes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `betises_ibfk_2` FOREIGN KEY (`element_id`) REFERENCES `elements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `connexions`
--
ALTER TABLE `connexions`
  ADD CONSTRAINT `connexions_ibfk_1` FOREIGN KEY (`equipe_id`) REFERENCES `equipes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `editions`
--
ALTER TABLE `editions`
  ADD CONSTRAINT `editions_ibfk_1` FOREIGN KEY (`equipe_id`) REFERENCES `equipes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `editions_ibfk_2` FOREIGN KEY (`organisateur_id`) REFERENCES `equipes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `editions_ibfk_3` FOREIGN KEY (`semaine_id`) REFERENCES `semaines` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `elements`
--
ALTER TABLE `elements`
  ADD CONSTRAINT `elements_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `elements_ibfk_2` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `propositions`
--
ALTER TABLE `propositions`
  ADD CONSTRAINT `propositions_ibfk_1` FOREIGN KEY (`equipe_id`) REFERENCES `equipes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `propositions_ibfk_2` FOREIGN KEY (`correcteur_id`) REFERENCES `equipes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `propositions_ibfk_3` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `questions`
--
ALTER TABLE `questions`
  ADD CONSTRAINT `questions_ibfk_1` FOREIGN KEY (`libelle_id`) REFERENCES `libelles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `questions_ibfk_2` FOREIGN KEY (`semaine_id`) REFERENCES `semaines` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `questions_ibfk_3` FOREIGN KEY (`organisateur_id`) REFERENCES `equipes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `votes`
--
ALTER TABLE `votes`
  ADD CONSTRAINT `votes_ibfk_1` FOREIGN KEY (`equipe_id`) REFERENCES `equipes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `votes_ibfk_2` FOREIGN KEY (`betise_id`) REFERENCES `betises` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

